package com.weifly.weistock.web.interceptor;

import com.weifly.weistock.web.WeistockBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 视图配置拦截器
 *
 * @author weifly
 * @since 2019/9/27
 */
public class ViewConfigInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private WeistockBuilder weistockBuilder;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        request.setAttribute(WeistockBuilder.ATTR_CONFIG_CDN, String.valueOf(weistockBuilder.isConfigCdn()));
        return true;
    }
}
