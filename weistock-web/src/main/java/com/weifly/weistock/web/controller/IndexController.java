package com.weifly.weistock.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 首页面
 *
 * @author weifly
 * @since 2019/1/30
 */
@Controller
public class IndexController {

    @RequestMapping(value = "/")
    public String welcome(HttpServletRequest request, HttpServletResponse response) {
        return this.index(request, response);
    }

    @RequestMapping(value = "/index")
    public String index(HttpServletRequest request, HttpServletResponse response) {
        return "index";
    }

    /**
     * option首页
     */
    @RequestMapping(value = "/option/index")
    public String optionIndex(HttpServletRequest request, HttpServletResponse response) {
        return "option/index";
    }
}
