package com.weifly.weistock.web.controller.record;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 记录管理
 *
 * @author weifly
 * @since 2019/9/19
 */
@Controller
@RequestMapping(value = "/record")
public class RecordController {

}
