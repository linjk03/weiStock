package com.weifly.weistock.web.util;

import com.weifly.weistock.core.common.ErrorCode;
import com.weifly.weistock.core.common.StockException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * 工具类
 *
 * @author weifly
 * @since 2021/01/04
 */
public class WebUtils {

    /**
     * 获得上传文件
     */
    public static MultipartFile getFile(MultipartHttpServletRequest request, String param) {
        MultipartFile orderFile = request.getFile(param);
        if (orderFile == null || orderFile.isEmpty() || orderFile.getSize() <= 0) {
            throw new StockException(ErrorCode.RESULT_PARAM_ERROR, "上传文件为空, param=" + param);
        }
        return orderFile;
    }
}
