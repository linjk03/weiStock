package com.weifly.weistock.web.controller.record;

import com.weifly.weistock.core.common.Result;
import com.weifly.weistock.record.data.DataHistoryService;
import com.weifly.weistock.record.data.DataMarketService;
import com.weifly.weistock.record.data.domain.StockDataDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 股票历史数据
 *
 * @author weifly
 * @since 2019/9/29
 */
@Controller
@RequestMapping(value = "/record/data")
public class DataHistoryController {

    @Autowired
    private DataMarketService dataMarketService;

    @Autowired
    private DataHistoryService dataHistoryService;

    /**
     * 加载股票数据
     */
    @ResponseBody
    @RequestMapping("/loadStockData")
    public Result loadStockData(HttpServletRequest request){
        String stockCode = request.getParameter("stockCode");
        if(StringUtils.isBlank(stockCode)){
            return Result.createErrorResult("缺少stockCode参数");
        }
        StockDataDto stockDto = this.dataMarketService.loadStockData(stockCode);
        if(stockDto==null){
            return Result.createErrorResult("无法加载数据 " + stockCode);
        }

        this.dataHistoryService.updateStockData(stockDto);
        return Result.createSuccessResult(null);
    }
}
