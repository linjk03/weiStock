package com.weifly.weistock.web.controller.record;

import com.weifly.weistock.core.common.Result;
import com.weifly.weistock.core.util.ConvertUtils;
import com.weifly.weistock.core.util.WeistockUtils;
import com.weifly.weistock.record.bill.BillConverter;
import com.weifly.weistock.record.bill.bo.StockHoldSummaryBO;
import com.weifly.weistock.record.bo.MergeRecordResult;
import com.weifly.weistock.record.bill.BillMonitorService;
import com.weifly.weistock.record.bill.BillParseService;
import com.weifly.weistock.record.bill.domain.StockRecordDto;
import com.weifly.weistock.record.bill.domain.StockSummaryDto;
import com.weifly.weistock.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.weifly.weistock.core.constant.WeistockConstants.PAGE_LIMIT;

/**
 * 股票记录
 *
 * @author weifly
 * @since 2020/01/17
 */
@Controller
@RequestMapping(value = "/record/bill")
public class BillMonitorController {

    private Logger log = LoggerFactory.getLogger(BillMonitorController.class);

    @Autowired
    private BillParseService billParseService;

    @Autowired
    private BillMonitorService billMonitorService;

    /**
     * 记录首页
     */
    @RequestMapping(value = "/index")
    public String index(HttpServletRequest request, HttpServletResponse response) {
        return "record/bill/index";
    }

    /**
     * 导入对账单
     */
    @RequestMapping(value = "/import")
    public String importOrder(HttpServletRequest request, HttpServletResponse response) {
        return "record/bill/import";
    }

    /**
     * 上传交割单
     */
    @ResponseBody
    @RequestMapping("/uploadStockOrder")
    public Result uploadStockOrder(MultipartHttpServletRequest request) throws IOException {
        MultipartFile orderFile = WebUtils.getFile(request, "stockOrderFile");
        List<StockRecordDto> recordList = this.billParseService.parseRecord(orderFile.getInputStream());
        MergeRecordResult mergeResult = this.billMonitorService.mergeRecordList(recordList);
        return Result.createSuccessResult(mergeResult);
    }

    /**
     * 股票列表
     */
    @RequestMapping(value = "/stockList")
    public String stockList(HttpServletRequest request, HttpServletResponse response) {
        return "record/bill/stockList";
    }

    /**
     * 加载股票列表
     */
    @ResponseBody
    @RequestMapping("/getStockList")
    public Result getStockList(HttpServletRequest request) {
        // 获取股票列表 并排序
        List<StockSummaryDto> stockList = BillConverter.sortStockList(this.billMonitorService.getStockList());
        // 转换数据
        List<Map<String, Object>> infoList = ConvertUtils.convert(stockList, BillConverter::convertStockSummary);
        return Result.createSuccessResult(infoList);
    }

    /**
     * 交易记录列表
     */
    @RequestMapping(value = "/recordList")
    public String recordList(HttpServletRequest request, HttpServletResponse response) {
        String stockCode = request.getParameter("stockCode"); // 股票代码
        StockSummaryDto summaryDto = Optional.of(this.billMonitorService.getStockSummary(stockCode)).orElse(new StockSummaryDto());
        request.setAttribute("stockSummary", BillConverter.convertStockSummary(summaryDto));
        return "record/bill/recordList";
    }

    /**
     * 加载交易记录列表
     */
    @ResponseBody
    @RequestMapping("/getRecordList")
    public Result getRecordList(HttpServletRequest request) {
        List<StockRecordDto> recordList = this.billMonitorService.getRecordList(BillConverter.convertToGetStockRecordRequest(request, PAGE_LIMIT));

        boolean haveMore = false;
        if (recordList.size() > PAGE_LIMIT) {
            haveMore = true;
            recordList = recordList.subList(0, PAGE_LIMIT); // 只返回limit条记录
        }
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("haveMore", haveMore);
        dataMap.put("recordList", ConvertUtils.convert(recordList, BillConverter::convertStockRecord));
        return Result.createSuccessResult(dataMap);
    }

    /**
     * 某股票 净持仓统计
     */
    @RequestMapping(value = "/stockHoldSummary")
    public String stockHoldSummary(HttpServletRequest request) {
        String stockCode = request.getParameter("stockCode"); // 股票代码
        StockHoldSummaryBO stockHoldSummary = Optional.of(this.billMonitorService.getStockHold(stockCode)).orElse(new StockHoldSummaryBO());
        request.setAttribute("stockHoldSummary", WeistockUtils.toJsonString(stockHoldSummary));
        return "record/bill/stockHoldSummary";
    }
}
