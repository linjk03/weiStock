package com.weifly.weistock.web.controller;

import com.weifly.weistock.core.chart.ChartConverter;
import com.weifly.weistock.core.chart.bo.ChartConfigBO;
import com.weifly.weistock.core.chart.bo.ChartDataSetBO;
import com.weifly.weistock.core.common.Result;

import com.weifly.weistock.core.util.ConvertUtils;
import com.weifly.weistock.core.util.DateUtils;
import com.weifly.weistock.optionvix.OptionVixConverter;
import com.weifly.weistock.optionvix.OptionVixMonitorService;
import com.weifly.weistock.optionvix.bo.VixDayBO;
import com.weifly.weistock.optionvix.bo.VixDayExternalBO;
import com.weifly.weistock.optionvix.dto.GetDayListRequest;
import com.weifly.weistock.optionvix.dto.VixSummaryDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.weifly.weistock.core.constant.WeistockConstants.PAGE_LIMIT;

/**
 * 期权VIX
 *
 * @author weifly
 * @since 2021/1/22
 */
@Controller
@RequestMapping(value = "/option/vix")
public class OptionVixController {

    private Logger log = LoggerFactory.getLogger(OptionVixController.class);

    @Autowired
    private OptionVixMonitorService optionVixMonitorService;

    /**
     * 添加编辑VIX
     */
    @RequestMapping(value = "/editDay")
    public String editDay() {
        return "option/vix/editDay";
    }

    /**
     * 更新VIX
     */
    @ResponseBody
    @RequestMapping("/updateDay")
    public Result updateDay(HttpServletRequest request) {
        VixDayBO vixDay = new VixDayBO();
        Result result = OptionVixConverter.convertToVixDay(request, vixDay);
        if (result != null) {
            return result;
        }
        this.optionVixMonitorService.updateVix(vixDay);
        return Result.createSuccessResult(null);
    }

    /**
     * VIX记录列表
     */
    @RequestMapping(value = "/list")
    public String list() {
        return "option/vix/list";
    }

    /**
     * 加载VIX记录列表
     */
    @ResponseBody
    @RequestMapping("/getDayList")
    public Result getDayList(HttpServletRequest request) {
        GetDayListRequest queryRequest = OptionVixConverter.convertToGetDayListRequest(request, PAGE_LIMIT + 1);
        // 降序排列的，多取一条数据，以便判断haveMore
        List<VixDayBO> dayList = this.optionVixMonitorService.getVixDayList(queryRequest);

        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("haveMore", dayList.size() > PAGE_LIMIT);
        dayList = dayList.size() > PAGE_LIMIT ? dayList.subList(0, PAGE_LIMIT) : dayList;
        List<Map<String, Object>> dayInfoList = ConvertUtils.convert(dayList, (vixDay) -> {
            VixDayExternalBO vixDayExternal = this.optionVixMonitorService.getVixDayExternal(vixDay.getDay());
            return OptionVixConverter.convertToVixDayInfo(vixDay, vixDayExternal);
        });
        dataMap.put("dayList", dayInfoList);
        return Result.createSuccessResult(dataMap);
    }

    /**
     * VIX统计
     */
    @RequestMapping(value = "/summary")
    public String summary() {
        return "option/vix/summary";
    }

    /**
     * 加载VIX统计
     */
    @ResponseBody
    @RequestMapping("/loadSummaryInfo")
    public Result loadSummaryInfo() {
        VixSummaryDTO vixSummary = this.optionVixMonitorService.calcMonitorInfo();
        return Result.createSuccessResult(vixSummary);
    }

    /**
     * VIX走势图
     */
    @RequestMapping(value = "/chart")
    public String chart(HttpServletRequest request) {
        Calendar lowDayCalc = Calendar.getInstance();
        lowDayCalc.add(Calendar.MONTH, -12);
        request.setAttribute("lowDay", new SimpleDateFormat(DateUtils.FORMATTER_DATE).format(lowDayCalc.getTime()));
        return "option/vix/chart";
    }

    /**
     * 加载VIX chart数据
     */
    @ResponseBody
    @RequestMapping("/loadChartData")
    public Result loadChartData(HttpServletRequest request) {
        ChartConfigBO chartConfig = ChartConverter.getChartConfig(request);
        ChartDataSetBO dataSet = new ChartDataSetBO();

        // 加载VIX列表
        List<VixDayBO> vixDayList = this.optionVixMonitorService.loadChartDayList(chartConfig.getDay());
        OptionVixConverter.fillVixDataSet(dataSet, vixDayList);

        // 加载百分比数据
        OptionVixConverter.fillPercentDataSet(dataSet, this.optionVixMonitorService);

        return Result.createSuccessResult(dataSet);
    }
}
