<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>股票</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <style>
        .option-table td{
            white-space: nowrap;
            padding: 2px 2px 2px 2px;
        }
        .option-table .text-right{
            text-align: right;
        }
        .option-table .tit{
            font-weight: 600;
            text-align: center;
        }
        .total-table td{
            text-align: center;
        }
    </style>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";

        $(function () {
            refreshStockStatus();
        });

        function refreshStockStatus() {
            $.ajax({
                url: CONTEXT_PATH + "/stock/loadStockStatus",
                cache: false,
                data: {},
                type: "get",
                dataType: 'json',
                success: function(result){
                    if(result.code=="200"){
                        renderStockList(result.data);
                        setTimeout(refreshStockStatus, 5000);
                    }else{
                        alert(result.returnMsg);
                    }
                }
            });
        }

        function renderStockList(monitorInfo){
            $("#statusField").html(makeStatusString(monitorInfo.status));
            $("#dateField").html(monitorInfo.date);
            $("#timeField").html(monitorInfo.time);

            var stockArray = [];
            if(monitorInfo.stockList){
                for(var i=0;i<monitorInfo.stockList.length;i++){
                    var oneInfo = monitorInfo.stockList[i];

                    var editUrl = CONTEXT_PATH + "/stock/editStockConfig?code=" + oneInfo.stockCode;
                    var newestPrice = oneInfo.newestPrice ? oneInfo.newestPrice : "";
                    var buyInfo = "";
                    if(oneInfo.buyInfo){
                        buyInfo = oneInfo.buyInfo.entrustPrice + "<br>" + oneInfo.buyInfo.orderTime + ", " + oneInfo.buyInfo.orderNumber;
                    }
                    var sellInfo = "";
                    if(oneInfo.sellInfo){
                        sellInfo = oneInfo.sellInfo.entrustPrice + "<br>" + oneInfo.sellInfo.orderTime + ", " + oneInfo.sellInfo.orderNumber;
                    }

                    var oneStr = "<tr><td><a href=\""+editUrl+"\" target=\"_self\">" + oneInfo.stockName + "<br>" + oneInfo.stockCode + "</a></td>"
                            +"<td class=\"text-right\">" + newestPrice + "</td>"
                            +"<td class=\"text-right\">" + oneInfo.basePrice + "<br>最后操作:" + makeLastOperString(oneInfo.lastOperation) + "</td>"
                            +"<td class=\"text-right\">" + buyInfo + "</td>"
                            +"<td class=\"text-right\">" + sellInfo + "</td>"
                            +"</tr>";
                    stockArray.push(oneStr);
                }
            }
            $("#stockListCt").html(stockArray.join(""));

            var msgArray = [];
            if(monitorInfo.messageList){
                var msgSize = monitorInfo.messageList.length;
                for(var i=(msgSize-1);i>=0;i--){
                    var oneInfo = monitorInfo.messageList[i];
                    var oneStr = "<tr><td>"+oneInfo.time+"</td><td>"+oneInfo.message+"</td></tr>";
                    msgArray.push(oneStr);
                }
            }
            $("#messageListCt").html(msgArray.join(""));
        }

        function makeStatusString(status){
            if(status==1){
                return "初始化";
            }else if(status==2){
                return "休眠&nbsp;非交易日";
            }else if(status==3){
                return "休眠&nbsp;非交易时间";
            }else if(status==4){
                return "判断";
            }else if(status==5){
                return "运行";
            }else{
                return "未知";
            }
        }

        function makeLastOperString(lastOper) {
            if(lastOper=="buy"){
                return "买入";
            }else if(lastOper=="sell"){
                return "卖出";
            }else{
                return "未知";
            }
        }

        function addStockConfig() {
            alert("addStockConfig");
        }

        function editAccount(){
            alert("editAccount");
        }
    </script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12" style="padding:0px;">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/stock/index">网格交易</a></li>
                    <li class="breadcrumb-item active">股票监控</li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12" style="overflow-x: auto;">
                <table class="table table-bordered option-table total-table">
                    <tr>
                        <td class="tit">日期</td>
                        <td class="tit">时间</td>
                        <td class="tit" colspan="2">状态</td>
                    </tr>
                    <tr>
                        <td id="dateField"></td>
                        <td id="timeField"></td>
                        <td id="statusField" colspan="2"></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12" style="width: 100%; overflow-x: auto;">
                <table class="table table-bordered option-table">
                    <thead>
                        <tr>
                            <td class="tit">股票</td>
                            <td class="tit">最新价</td>
                            <td class="tit">基准价</td>
                            <td class="tit">买入</td>
                            <td class="tit">卖出</td>
                        </tr>
                    </thead>
                    <tbody id="stockListCt">
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12" style="width: 100%; overflow-x: auto;">
                <table class="table table-bordered option-table">
                    <colgroup>
                        <col style="width: 80px;">
                    </colgroup>
                    <tbody id="messageListCt">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
