<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>stock-web</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <style>
        .option-table td{
            white-space: nowrap;
            padding: 2px 2px 2px 2px;
        }
        .option-table .text-right{
            text-align: right;
        }
        .option-table .tit{
            font-weight: 600;
            text-align: center;
        }
        .total-table td{
            text-align: center;
        }
    </style>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";
        var uploadFileButtonObj = null;

        $(function () {
            uploadFileButtonObj = $("#uploadFileButton");
            initUploadButtonClick();
        });

        function initUploadButtonClick(){
            uploadFileButtonObj.on("click", function(){
                var fileVal = $("#stockOrderFile").val();
                if(fileVal.length<=0){
                    alert("请选择文件");
                    return;
                }

                $("#uploadFileForm").ajaxSubmit({
                    type: "post",
                    dataType: "json",
                    url: CONTEXT_PATH + "/record/bill/uploadStockOrder",
                    beforeSubmit: function(arr, $form, options){
                        uploadFileButtonObj.attr("disabled", "disabled").html("上传中...");
                    },
                    success: function(result){
                        uploadFileButtonObj.removeAttr("disabled").html("上传");
                        if(result.code=="200"){
                            var mergeInfo = result.data;
                            var str = "总记录：" + mergeInfo.total + "，插入：" + mergeInfo.insert + "，更新：" + mergeInfo.update;
                            $("#importResult").html(str);
                        }else{
                            alert(result.message);
                        }
                    }
                });
            });
        }
    </script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12" style="padding:0px;">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/record/bill/index">股票记录</a></li>
                    <li class="breadcrumb-item active">导入对账单</li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <form id="uploadFileForm" method="post">
                    <input id="stockOrderFile" name="stockOrderFile" accept="" type="file">
                    <button id="uploadFileButton" type="button">上传</button>
                </form>
                <div id="importResult"></div>
            </div>
        </div>
    </div>
</body>
</html>
