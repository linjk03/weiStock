<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>stock-web</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12" style="padding:0px;">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                    <li class="breadcrumb-item active">资产记录</li>
                </ul>
            </div>
        </div>

        <div class="row" style="margin-bottom: 4px;">
            <div class="col-sm-12" style="text-align: center;">
                <a class="btn btn-info" href="<%=request.getContextPath()%>/record/asset/chart">资产走势图</a>
            </div>
        </div>
        <div class="row" style="margin-bottom: 4px;">
            <div class="col-sm-12" style="text-align: center;">
                <a class="btn btn-info" href="<%=request.getContextPath()%>/record/asset/editDay">添加资产记录</a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12" style="text-align: center;">
                <a class="btn btn-info" href="<%=request.getContextPath()%>/record/asset/list">资产记录列表</a>
            </div>
        </div>
    </div>
</body>
</html>
