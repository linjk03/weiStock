<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>stock-web</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <style>
        .option-table td{
            white-space: nowrap;
            padding: 2px 2px 2px 2px;
        }
        .option-table .text-right{
            text-align: right;
        }
        .option-table .tit{
            font-weight: 600;
            text-align: center;
        }
        .total-table td{
            text-align: center;
        }
    </style>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";
        var loadBtnObj;
        var lastRecordDay;

        $(function () {
            initLoadButton();
            loadDayList();
        });

        function initLoadButton(){
            loadBtnObj = $("#loadBtn");
            loadBtnObj.on("click", function(){
                loadDayList();
            });
        }

        function loadDayList(){
            loadBtnObj.attr("disabled", "disabled");
            $.ajax({
                url: CONTEXT_PATH + "/record/asset/getDayList",
                cache: false,
                data: {
                    day: lastRecordDay
                },
                type: "get",
                dataType: 'json',
                success: function(result){
                    loadBtnObj.removeAttr("disabled");
                    if(result.code=="200"){
                        renderDayList(result.data);
                    }else{
                        alert(result.returnMsg);
                    }
                }
            });
        }

        function renderDayList(dataMap){
            if(dataMap.haveMore){
                loadBtnObj.removeAttr("disabled");
            }else{
                loadBtnObj.attr("disabled", "disabled");
            }

            var dayList = dataMap.dayList;
            if(dayList.length>0){
                var dayListCtObj = $("#dayListCt");
                for(var i=0;i<dayList.length;i++){
                    var dayInfo = dayList[i];
                    var diff = (dayInfo.diff===undefined ? "" : dayInfo.diff);
                    var rate = (dayInfo.rate===undefined ? "" : dayInfo.rate);
                    var line = "<tr><td>" + dayInfo.day + "</td><td>" + dayInfo.asset + "</td><td>" + diff + "</td><td>" + rate + "</td></tr>";
                    dayListCtObj.append(line);
                }
                lastRecordDay = dayList[dayList.length-1].day;
            }
        }
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12" style="padding:0px;">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/record/asset/index">资产记录</a></li>
                <li class="breadcrumb-item active">资产记录列表</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="overflow-x: auto;">
            <table class="table table-bordered option-table total-table">
                <colgroup>
                    <col style="width: 80px;">
                </colgroup>
                <thead>
                    <tr>
                        <td class="tit">日期</td>
                        <td class="tit">资产</td>
                        <td class="tit">差额</td>
                        <td class="tit">涨幅%</td>
                    </tr>
                </thead>
                <tbody id="dayListCt">
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <button id="loadBtn" class="btn btn-info">加载更多</button>
        </div>
    </div>
</div>
</body>
</html>
