<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>轮动监控</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <style>
        .option-table td{
            white-space: nowrap;
            padding: 2px 2px 2px 2px;
        }
        .option-table .text-right{
            text-align: right;
        }
        .option-table .tit{
            font-weight: 600;
            text-align: center;
        }
        .total-table td{
            text-align: center;
        }
    </style>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";

        $(function () {
            refreshRotationStatus();
        });

        function refreshRotationStatus() {
            $.ajax({
                url: CONTEXT_PATH + "/rotation/loadRotationStatus",
                cache: false,
                data: {},
                type: "get",
                dataType: 'json',
                success: function(result){
                    if(result.code=="200"){
                        renderMonitorInfo(result.data);
                        setTimeout(refreshRotationStatus, 5000);
                    }else{
                        alert(result.returnMsg);
                    }
                }
            });
        }

        function renderMonitorInfo(monitorInfo){
            renderHeadInfo(monitorInfo);
            renderPointList(monitorInfo);
            renderMessageList(monitorInfo);
        }

        function renderHeadInfo(monitorInfo){
            $("#statusField").html(makeStatusString(monitorInfo.status));
            $("#dateField").html(monitorInfo.date);
            $("#timeField").html(monitorInfo.time);
            if(monitorInfo.baseStockCode){
                $("#baseStockField").html(monitorInfo.baseStockCode + " - " + monitorInfo.baseStockName);
            }
            if(monitorInfo.compareStockCode){
                $("#compareStockField").html(monitorInfo.compareStockCode + " - " + monitorInfo.compareStockName);
            }
            if(monitorInfo.nowDiffTime){
                $("#nowDiffField").html(monitorInfo.nowDiff + " (" + monitorInfo.nowDiffTime + ")");
            }
            if(monitorInfo.minDiffTime){
                $("#minDiffField").html(monitorInfo.minDiff + " (" + monitorInfo.minDiffTime + ")");
            }
            if(monitorInfo.maxDiffTime){
                $("#maxDiffField").html(monitorInfo.maxDiff + " (" + monitorInfo.maxDiffTime + ")");
            }
        }

        function makeStatusString(status){
            if(status==1){
                return "初始化";
            }else if(status==2){
                return "休眠&nbsp;非交易日";
            }else if(status==3){
                return "休眠&nbsp;非交易时间";
            }else if(status==4){
                return "判断";
            }else if(status==5){
                return "运行";
            }else{
                return "未知";
            }
        }

        function renderPointList(monitorInfo){
            var pointArray = [];
            if(monitorInfo.pointList){
                for(var i=0;i<monitorInfo.pointList.length;i++){
                    var oneInfo = monitorInfo.pointList[i];
                    var oneStr = "<tr><td>" + oneInfo.time  + "</td>"
                        +"<td class=\"text-right\">" + oneInfo.basePrice + "</td>"
                        +"<td class=\"text-right\">" + oneInfo.baseNormal + "</td>"
                        +"<td class=\"text-right\">" + oneInfo.comparePrice + "</td>"
                        +"<td class=\"text-right\">" + oneInfo.compareNormal + "</td>"
                        +"<td class=\"text-right\">" + oneInfo.diff + "</td>"
                        +"</tr>";
                    pointArray.push(oneStr);
                }
            }
            $("#pointListCt").html(pointArray.join(""));
        }

        function renderMessageList(monitorInfo){
            var msgArray = [];
            if(monitorInfo.messageList){
                var msgSize = monitorInfo.messageList.length;
                for(var i=(msgSize-1);i>=0;i--){
                    var oneInfo = monitorInfo.messageList[i];
                    var oneStr = "<tr><td>"+oneInfo.time+"</td><td>"+oneInfo.message+"</td></tr>";
                    msgArray.push(oneStr);
                }
            }
            $("#messageListCt").html(msgArray.join(""));
        }

        function makeLastOperString(lastOper) {
            if(lastOper=="buy"){
                return "买入";
            }else if(lastOper=="sell"){
                return "卖出";
            }else{
                return "未知";
            }
        }

        function addStockConfig() {
            alert("addStockConfig");
        }

        function editAccount(){
            alert("editAccount");
        }
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12" style="padding:0px;">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/rotation/index">轮动首页</a></li>
                <li class="breadcrumb-item active">轮动监控</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="overflow-x: auto;">
            <table class="table table-bordered option-table total-table">
                <tr>
                    <td class="tit">日期</td>
                    <td class="tit">时间</td>
                    <td class="tit">状态</td>
                </tr>
                <tr>
                    <td id="dateField"></td>
                    <td id="timeField"></td>
                    <td id="statusField"></td>
                </tr>
                <tr>
                    <td class="tit">基准标的</td>
                    <td class="tit">比较标的</td>
                    <td class="tit">&nbsp;</td>
                </tr>
                <tr>
                    <td id="baseStockField"></td>
                    <td id="compareStockField"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="tit">最新差值</td>
                    <td class="tit">最小差值</td>
                    <td class="tit">最大差值</td>
                </tr>
                <tr>
                    <td id="nowDiffField"></td>
                    <td id="minDiffField"></td>
                    <td id="maxDiffField"></td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="width: 100%; overflow-x: auto;">
            <table class="table table-bordered option-table">
                <thead>
                    <tr>
                        <td class="tit">时间</td>
                        <td class="tit">基准价格</td>
                        <td class="tit">基准归一</td>
                        <td class="tit">比较价格</td>
                        <td class="tit">比较归一</td>
                        <td class="tit">差值</td>
                    </tr>
                </thead>
                <tbody id="pointListCt">
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="width: 100%; overflow-x: auto;">
            <table class="table table-bordered option-table">
                <colgroup>
                    <col style="width: 80px;">
                </colgroup>
                <tbody id="messageListCt">
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
