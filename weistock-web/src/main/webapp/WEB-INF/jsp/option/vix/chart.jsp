<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>vix走势图</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <script src="<%=request.getContextPath()%>/static/js/echarts/echarts.js"></script>
    <style>
        .option-table td{
            white-space: nowrap;
            padding: 2px 2px 2px 2px;
        }
        .option-table .text-right{
            text-align: right;
        }
        .option-table .tit{
            font-weight: 600;
            text-align: center;
        }
        .option-table select{
            line-height: 24px;
        }
        .total-table td{
            text-align: center;
        }
        .table-c-l {
            border: none;
            width: 100%;
        }
        .table-c-l td {
            border: none;
            padding: 0px;
        }
    </style>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";
        var searchBtnObj = null;
        var vixChartObj = null;
        var percentChartObj = null;

        $(function () {
            vixChartObj = echarts.init(document.getElementById("vixChart"));
            percentChartObj = echarts.init(document.getElementById("percentChart"));
            initSearchButton();
            searchChart();
        });

        function initSearchButton(){
            searchBtnObj = $("#searchBtn");
            searchBtnObj.on("click", function(){
                searchChart();
            });
        }

        function searchChart(){
            var param = {};
            param.day = $("#dayField").val();
            searchBtnObj.attr("disabled", "disabled");
            $.ajax({
                url: CONTEXT_PATH + "/option/vix/loadChartData",
                cache: false,
                traditional: true,
                data: param,
                type: "get",
                dataType: 'json',
                success: function(result){
                    searchBtnObj.removeAttr("disabled");
                    if(result.code=="200"){
                        renderVixChart(result.data);
                        renderPercentChart(result.data);
                    }else{
                        alert(result.returnMsg);
                    }
                }
            });
        }

        function renderVixChart(data){
            var option = {
                title: { text: "VIX走势图" },
                tooltip: {},
                legend: { data: [] },
                xAxis: {
                    data: []
                },
                yAxis: {
                    min: "dataMin"
                },
                series: []
            };

            option.xAxis.data = data.pointList;
            var serieInfo = data.serieList[0];
            option.legend.data.push(serieInfo.name);
            option.series.push({
                "name": serieInfo.name,
                "type": serieInfo.type,
                "data": serieInfo.dataList,
                "showAllSymbol": true
            });
            vixChartObj.setOption(option);
        }

        function renderPercentChart(data) {
            var option = {
                title: { text: "百分比图" },
                tooltip: {},
                legend: { data: [] },
                xAxis: {
                    data: []
                },
                yAxis: {
                    min: "dataMin"
                },
                series: []
            };

            option.xAxis.data = data.pointList;
            for(var i=1;i<data.serieList.length;i++){
                var serieInfo = data.serieList[i];
                option.legend.data.push(serieInfo.name);
                option.series.push({
                    "name": serieInfo.name,
                    "type": serieInfo.type,
                    "data": serieInfo.dataList,
                    "showAllSymbol": true
                });
            }
            percentChartObj.setOption(option);
        }
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12" style="padding:0px;">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/option/index">期权模块</a></li>
                <li class="breadcrumb-item active">VIX走势图</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="overflow-x: auto;">
            <table class="table table-bordered option-table total-table">
                <tr>
                    <td style="padding: 0px;">
                        <table class="table-c-l">
                            <tr>
                                <td style="width: 50%">
                                    <table class="table-c-l">
                                        <tr>
                                            <td style="width: 80px;">K线</td>
                                            <td>
                                                <select id="typeField" style="width: 100%;">
                                                    <option value="day" selected>日K线</option>
                                                    <option value="week">周K线</option>
                                                    <option value="month">月K线</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width: 50%">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 80px;">时间</td>
                                            <td><input id="dayField" type="text" style="width: 100%;" value="<%=request.getAttribute("lowDay")%>"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td rowspan="2" style="width: 80px; vertical-align: middle;">
                        <a id="searchBtn" class="btn-sm btn-info">查询</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div id="vixChart" style="height:380px;"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div id="percentChart" style="height:380px;"></div>
        </div>
    </div>
</div>
</body>
</html>
