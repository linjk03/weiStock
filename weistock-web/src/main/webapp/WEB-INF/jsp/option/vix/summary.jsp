<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>VIX统计</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <style>
        .option-table td{
            white-space: nowrap;
            padding: 2px 2px 2px 2px;
        }
        .option-table .text-right{
            text-align: right;
        }
        .option-table .tit{
            font-weight: 600;
            text-align: center;
        }
        .total-table td{
            text-align: center;
        }
    </style>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";

        $(function () {
            loadSummaryInfo();
        });

        function loadSummaryInfo() {
            $.ajax({
                url: CONTEXT_PATH + "/option/vix/loadSummaryInfo",
                cache: false,
                data: {},
                type: "get",
                dataType: 'json',
                success: function(result){
                    if(result.code=="200"){
                        renderSummaryInfo(result.data);
                    }else{
                        alert(result.returnMsg);
                    }
                }
            });
        }

        function renderSummaryInfo(monitorInfo){
            // 头部信息
            $("#newestVixField").html(monitorInfo.newestVix);
            $("#newestDayField").html(monitorInfo.newestDay);
            $("#recentPercentField").html(monitorInfo.recentPercent);
            $("#recentStartDayField").html(monitorInfo.recentStartDay);
            $("#recentEndDayField").html(monitorInfo.recentEndDay);
            $("#recentDayNumberField").html(monitorInfo.recentDayNumber);
            $("#allPercentField").html(monitorInfo.allPercent);
            $("#allStartDayField").html(monitorInfo.allStartDay);
            $("#allEndDayField").html(monitorInfo.allEndDay);
            $("#allDayNumberField").html(monitorInfo.allDayNumber);

            // VIX区间列表
            var regionArray = [];
            if(monitorInfo.regionList){
                for(var i=0;i<monitorInfo.regionList.length;i++){
                    var oneInfo = monitorInfo.regionList[i];
                    var oneStr = "<tr>"+
                        "<td>" + formatVix(oneInfo.highVix) + "-" + formatVix(oneInfo.lowVix) + "</td>"+
                        "<td class=\"text-right\">" + (oneInfo.recentDayNumber > 0 ? oneInfo.recentDayNumber : "-") +"</td>"+
                        "<td class=\"text-right\">" + (oneInfo.recentDayNumber > 0 ? oneInfo.recentDayPercent + "%" : "-") + "</td>"+
                        "<td class=\"text-right\">" + (oneInfo.recentDayNumber > 0 ? oneInfo.recentTotalDayNumber : "-") + "</td>"+
                        "<td class=\"text-right\">" + (oneInfo.recentDayNumber > 0 ? oneInfo.recentTotalDayPercent + "%" : "-") + "</td>"+
                        "<td class=\"text-right\">" + (oneInfo.allDayNumber > 0 ? oneInfo.allDayNumber : "-") + "</td>"+
                        "<td class=\"text-right\">" + (oneInfo.allDayNumber > 0 ? oneInfo.allDayPercent + "%" : "-") + "</td>"+
                        "<td class=\"text-right\">" + (oneInfo.allDayNumber > 0 ? oneInfo.allTotalDayNumber : "-") + "</td>"+
                        "<td class=\"text-right\">" + (oneInfo.allDayNumber > 0 ? oneInfo.allTotalDayPercent + "%" : "-") + "</td>"+
                        "</tr>";
                    regionArray.push(oneStr);
                }
            }
            $("#regionListCt").html(regionArray.join(""));
        }

        function formatVix(val){
            val = "" + val;
            if(val.indexOf(".")==-1){
                return val + ".0";
            }
            return val;
        }
    </script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12" style="padding:0px;">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/option/index">期权模块</a></li>
                    <li class="breadcrumb-item active">VIX统计</li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12" style="overflow-x: auto;">
                <table class="table table-bordered option-table total-table">
                    <tr>
                        <td class="tit">最新VIX</td>
                        <td class="tit" colspan="3">最新天</td>
                    </tr>
                    <tr>
                        <td id="newestVixField"></td>
                        <td id="newestDayField" colspan="3"></td>
                    </tr>
                    <tr>
                        <td class="tit">recent百分位</td>
                        <td class="tit">recent开始</td>
                        <td class="tit">recent结束</td>
                        <td class="tit">recent天数</td>
                    </tr>
                    <tr>
                        <td id="recentPercentField"></td>
                        <td id="recentStartDayField"></td>
                        <td id="recentEndDayField"></td>
                        <td id="recentDayNumberField"></td>
                    </tr>
                    <tr>
                        <td class="tit">all百分位</td>
                        <td class="tit">all开始</td>
                        <td class="tit">all结束</td>
                        <td class="tit">all天数</td>
                    </tr>
                    <tr>
                        <td id="allPercentField"></td>
                        <td id="allStartDayField"></td>
                        <td id="allEndDayField"></td>
                        <td id="allDayNumberField"></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12" style="width: 100%; overflow-x: auto;">
                <table class="table table-bordered option-table">
                    <thead>
                        <tr>
                            <td colspan="1">&nbsp;</td>
                            <td colspan="4">recent</td>
                            <td colspan="4">all</td>
                        </tr>
                        <tr>
                            <td class="tit">VIX</td>
                            <td class="tit">天数</td>
                            <td class="tit">占比</td>
                            <td class="tit">累积</td>
                            <td class="tit">累积占比</td>
                            <td class="tit">天数</td>
                            <td class="tit">占比</td>
                            <td class="tit">累积</td>
                            <td class="tit">累积占比</td>
                        </tr>
                    </thead>
                    <tbody id="regionListCt">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
