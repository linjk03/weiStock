// dateSum 表格渲染
var DateSumTable = function () {};

DateSumTable.prototype = {

    tableHeader: null,
    tableBody: null,
    dateSumJson: null,

    init: function () {
        this.tableHeader = $("#dateSumHeader");
        this.tableBody = $("#dateSumBody");
    },

    render: function(dateSumJson){
        this.tableHeader.empty();
        this.tableBody.empty();
        this.dateSumJson = dateSumJson;

        if(dateSumJson.length<1){
            return;
        }

        this.renderHeader(dateSumJson);
        this.renderBody(dateSumJson);
    },

    renderHeader: function (dateSumJson) {
        var firstDateSum = dateSumJson[0];
        var sb = [];
        sb.push("<tr class=\"addNewTr\">");
        sb.push("<th>指标名称</th>");
        sb.push("<th>累计</th>");
        if(firstDateSum.bucketList && firstDateSum.bucketList.length>0){
            for(var i=0;i<firstDateSum.bucketList.length;i++){
                var daySum = firstDateSum.bucketList[i];
                sb.push("<th>"+daySum.day+"</th>");
            }
        }
        sb.push("</tr>");

        this.tableHeader.html(sb.join(""));
    },

    renderBody: function (dateSumJson) {
        var sb = [];
        for(var i=0;i<dateSumJson.length;i++){
            var exceptionSum = dateSumJson[i];
            sb.push("<tr>");
            sb.push("<td>"+exceptionSum.name+"</td>");
            sb.push("<td><a href=\"javascript:void(0)\" exceptionIndex=\""+i+"\" bucketIndex=\"all\">"+exceptionSum.total+"</a></td>");

            if(exceptionSum.bucketList && exceptionSum.bucketList.length>0){
                for(var j=0;j<exceptionSum.bucketList.length;j++){
                    var daySum = exceptionSum.bucketList[j];
                    sb.push("<td><a href=\"javascript:void(0)\" exceptionIndex=\""+i+"\" bucketIndex=\""+j+"\">"+daySum.value+"</a></td>");
                }
            }

            sb.push("</tr>");
        }
        this.tableBody.html(sb.join(""));

        var self = this;
        this.tableBody.find("a").click(function(){
            var $a = $(this);
            var exceptionIndex = $a.attr("exceptionIndex");
            var bucketIndex = $a.attr("bucketIndex");
            var exceptionSum = self.dateSumJson[exceptionIndex];
            self.dateSumClick(exceptionSum, bucketIndex);
        });
    },

    dateSumClick: function (exceptionSum, bucketIndex) {
        alert("name: " + exceptionSum.name + ", bucketIndex: " + bucketIndex);
    }
};