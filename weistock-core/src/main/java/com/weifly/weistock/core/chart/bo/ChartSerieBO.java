package com.weifly.weistock.core.chart.bo;

import java.util.ArrayList;
import java.util.List;

/**
 * chart数据
 *
 * @author weifly
 * @since 2021/1/20
 */
public class ChartSerieBO {

    private String name; // serie名称
    private String type; // chart类型
    private List<Double> dataList = new ArrayList<>(); // 数据集合

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Double> getDataList() {
        return dataList;
    }

    public void setDataList(List<Double> dataList) {
        this.dataList = dataList;
    }
}
