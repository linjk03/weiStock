package com.weifly.weistock.core.config;

/**
 * 配置常量定义
 *
 * @author weifly
 * @since 2020/11/30
 */
public class ConfigConstants {

    public static final String ELE_ROOT = "root";
    public static final String ELE_RECORD = "record";
    public static final String ELE_CONFIG = "config";
    public static final String ELE_OPEN = "open";

    public static final String ATTR_YEAR = "year";
    public static final String ATTR_DAY = "day";
    public static final String ATTR_OPEN = "open";
    public static final String ATTR_HIGH = "high";
    public static final String ATTR_LOW = "low";
    public static final String ATTR_CLOSE = "close";
}
