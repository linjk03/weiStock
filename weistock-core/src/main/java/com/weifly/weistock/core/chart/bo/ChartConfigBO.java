package com.weifly.weistock.core.chart.bo;

import com.weifly.weistock.core.constant.ChartTypeEnum;

import java.util.List;

/**
 * chart配置
 *
 * @author weifly
 * @since 2021/1/20
 */
public class ChartConfigBO {

    private ChartTypeEnum type; // chart类型
    private String day; // 数据记录开始日期
    private List<String> overList; // 叠加列表

    public ChartTypeEnum getType() {
        return type;
    }

    public void setType(ChartTypeEnum type) {
        this.type = type;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public List<String> getOverList() {
        return overList;
    }

    public void setOverList(List<String> overList) {
        this.overList = overList;
    }
}
