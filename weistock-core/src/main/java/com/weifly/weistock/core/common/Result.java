package com.weifly.weistock.core.common;

/**
 * 通用返回结果
 *
 * @author weifly
 * @since 2019/1/31
 */
public class Result {

    private String code;
    private String message;
    private Object data;

    public Result(){

    }

    public Result(String code, String message){
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public boolean isSuccess(){
        return ErrorCode.RESULT_SUCC.equals(this.code);
    }

    public static Result createSuccessResult(Object data){
        Result res = new Result();
        res.code = ErrorCode.RESULT_SUCC;
        res.data = data;
        return res;
    }

    public static Result createErrorResult(String msg){
        return createErrorResult(msg, null);
    }

    public static Result createErrorResult(String msg, Object data){
        Result res = new Result();
        res.code = ErrorCode.RESULT_SYS_ERROR;
        res.message = msg;
        res.data = data;
        return res;
    }
}
