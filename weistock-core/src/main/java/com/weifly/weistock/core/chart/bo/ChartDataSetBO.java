package com.weifly.weistock.core.chart.bo;

import java.util.ArrayList;
import java.util.List;

/**
 * chart数据集合
 *
 * @author weifly
 * @since 2021/1/20
 */
public class ChartDataSetBO {

    private List<String> pointList = new ArrayList<>(); // x坐标点集合
    private List<ChartSerieBO> serieList = new ArrayList<>(); // serie集合

    public List<String> getPointList() {
        return pointList;
    }

    public void setPointList(List<String> pointList) {
        this.pointList = pointList;
    }

    public List<ChartSerieBO> getSerieList() {
        return serieList;
    }

    public void setSerieList(List<ChartSerieBO> serieList) {
        this.serieList = serieList;
    }
}
