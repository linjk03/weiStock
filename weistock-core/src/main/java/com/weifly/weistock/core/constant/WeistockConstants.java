package com.weifly.weistock.core.constant;

/**
 * 常量定义
 *
 * @author weifly
 * @since 2021/2/1
 */
public class WeistockConstants {

    public static final int PAGE_LIMIT = 50; // 每页记录数
}
