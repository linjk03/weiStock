package com.weifly.weistock.core.task;

import com.weifly.weistock.core.task.AbstractUpdateService;

/**
 * 更新状态服务
 *
 * @author weifly
 * @since 2019/2/3
 */
public class UpdateStatus {

    private int status = AbstractUpdateService.STATUS_INIT; // 状态
    private long sleepTime; // 休眠时间，毫秒
    private int preStatus; // 前一个状态

    private int checkYear; // 年
    private int checkMonth; // 月
    private int checkDay; // 日
    private int checkWeek; // 周
    private int checkHour; // 时
    private int checkMinute; // 分
    private int checkSecond; // 秒

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getSleepTime() {
        return sleepTime;
    }

    public void setSleepTime(long sleepTime) {
        this.sleepTime = sleepTime;
    }

    public int getPreStatus() {
        return preStatus;
    }

    public void setPreStatus(int preStatus) {
        this.preStatus = preStatus;
    }

    public int getCheckYear() {
        return checkYear;
    }

    public void setCheckYear(int checkYear) {
        this.checkYear = checkYear;
    }

    public int getCheckMonth() {
        return checkMonth;
    }

    public void setCheckMonth(int checkMonth) {
        this.checkMonth = checkMonth;
    }

    public int getCheckDay() {
        return checkDay;
    }

    public void setCheckDay(int checkDay) {
        this.checkDay = checkDay;
    }

    public int getCheckWeek() {
        return checkWeek;
    }

    public void setCheckWeek(int checkWeek) {
        this.checkWeek = checkWeek;
    }

    public int getCheckHour() {
        return checkHour;
    }

    public void setCheckHour(int checkHour) {
        this.checkHour = checkHour;
    }

    public int getCheckMinute() {
        return checkMinute;
    }

    public void setCheckMinute(int checkMinute) {
        this.checkMinute = checkMinute;
    }

    public int getCheckSecond() {
        return checkSecond;
    }

    public void setCheckSecond(int checkSecond) {
        this.checkSecond = checkSecond;
    }

    public String makeDateString(){
        StringBuilder sb = new StringBuilder();
        sb.append(this.checkYear);
        if(this.checkMonth<10){
            sb.append("-0").append(this.checkMonth);
        }else{
            sb.append("-").append(this.checkMonth);
        }
        if(this.checkDay<10){
            sb.append("-0").append(this.checkDay);
        }else{
            sb.append("-").append(this.checkDay);
        }
        return sb.toString();
    }

    public String makeTimeString(){
        StringBuilder sb = new StringBuilder();
        if(this.checkHour<10){
            sb.append("0").append(this.checkHour);
        }else{
            sb.append(this.checkHour);
        }
        if(this.checkMinute<10){
            sb.append(":0").append(this.checkMinute);
        }else{
            sb.append(":").append(this.checkMinute);
        }
        if(this.checkSecond<10){
            sb.append(":0").append(this.checkSecond);
        }else{
            sb.append(":").append(this.checkSecond);
        }
        return sb.toString();
    }
}
