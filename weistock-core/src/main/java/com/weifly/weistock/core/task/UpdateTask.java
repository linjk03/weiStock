package com.weifly.weistock.core.task;

/**
 * 更新任务基类
 *
 * @author weifly
 * @since 2019/2/3
 */
public interface UpdateTask {

    /**
     * 执行更新
     */
    void doRunTask(UpdateStatus updateStatus) throws Exception;
}