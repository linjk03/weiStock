package com.weifly.weistock.core.util.grid;

/**
 * 网格价格点
 *
 * @author weifly
 * @since 2019/8/13
 */
public class GridPricePoint {

    private double price; // 价格
    private double diff; // 价差
    private double step; // 上涨幅度

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiff() {
        return diff;
    }

    public void setDiff(double diff) {
        this.diff = diff;
    }

    public double getStep() {
        return step;
    }

    public void setStep(double step) {
        this.step = step;
    }
}
