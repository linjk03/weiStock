package com.weifly.weistock.core.constant;

/**
 * 股票分类
 *
 * @author weifly
 * @since 2021/1/19
 */
public enum StockCatalogEnum {

    SH_STOCK("上海股票", "1", 0.01),
    SH_FUND("上海基金", "1", 0.001),
    SZ_STOCK("深圳股票", "0", 0.01),
    SZ_FUND("深圳基金", "0", 0.001),
    HK_STOCK("香港股票", null, null);

    private String desc;

    /**
     * 交易所ID
     */
    private String exchangeId = null;

    /**
     * 最小变动价格，股票为0.01，基金为0.001
     */
    private Double minStepPrice = null;

    StockCatalogEnum(String desc, String exchangeId, Double minStepPrice) {
        this.desc = desc;
        this.exchangeId = exchangeId;
        this.minStepPrice = minStepPrice;
    }

    public String getDesc() {
        return desc;
    }

    public String getExchangeId() {
        return exchangeId;
    }

    public Double getMinStepPrice() {
        return minStepPrice;
    }
}
