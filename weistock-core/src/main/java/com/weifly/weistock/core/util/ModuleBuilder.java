package com.weifly.weistock.core.util;

/**
 * 用于构建模块
 *
 * @author weifly
 * @since 2019/9/24
 */
public interface ModuleBuilder {

    /**
     * 构建模块
     *
     * @param configPath 配置文件路径
     */
    void build(String configPath);
}
