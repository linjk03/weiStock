package com.weifly.weistock.core.monitor;

/**
 * 监控常量定义
 *
 * @author weifly
 * @since 2020/12/02
 */
public class MonitorConstants {

    public static final int TIME_9HOUR_0MINUTE = 9*3600*1000 + 0*60*1000; // 09:00:00
    public static final int TIME_9HOUR_15MINUTE = 9*3600*1000 + 15*60*1000; // 09:15:00
    public static final int TIME_15HOUR_3MINUTE = 15*3600*1000 + 3*60*1000; // 15:03:00
}
