package com.weifly.weistock.trade.task;

import com.weifly.weistock.core.common.Result;
import com.weifly.weistock.core.market.StockPriceDto;
import com.weifly.weistock.trade.config.StockConfigDto;
import com.weifly.weistock.trade.enums.BuyOrSellEnum;
import com.weifly.weistock.trade.enums.OrderStatusEnum;
import com.weifly.weistock.trade.enums.TradeResultEnum;
import com.weifly.weistock.trade.bo.StockMonitorBO;
import com.weifly.weistock.trade.monitor.StockMonitorService;
import com.weifly.weistock.trade.monitor.impl.StockMonitorServiceImpl;
import com.weifly.weistock.trade.trade.SendOrderInfo;
import com.weifly.weistock.trade.trade.StockOrderInfo;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 测试StockCheckAction
 *
 * @author weifly
 * @since 2019/3/12
 */
public class TestStockCheckAction {

    @Test
    public void testCheck(){
        StockMonitorBO stockMonitorDto = createStockMonitorDto();

        StockUpdateContext ctx = new StockUpdateContext() {
            public StockPriceDto getStockPrice(String stockCode) {
                return null;
            }

            public List<StockOrderInfo> getOrderList() {
                return TestStockCheckAction.getOrderList();
            }

            public Result sendOrder(SendOrderInfo sendOrderInfo) {
                if(sendOrderInfo.getBuyOrSell().equals(BuyOrSellEnum.buy)){
                    return Result.createErrorResult("buy error", null);
                }else{
                    return Result.createErrorResult("[251005][证券可用数量不足]", TradeResultEnum.SELL_NO_MORE_STOCK);
                }
            }

            public Result cancelOrder(StockOrderInfo stockOrderInfo) {
                return Result.createSuccessResult("2");
            }

            public void fireStockListChange() {}

            @Override
            public StockMonitorService getStockMonitorService() {
                return null;
            }

            @Override
            public Long getStopSendOrderTime() {
                return null;
            }
        };

        StockCheckAction checkAction = new StockCheckAction();
        checkAction.check(stockMonitorDto, ctx);
    }

    @Test
    public void testStopSendOrderTime(){
        StockMonitorBO stockMonitorDto = createStockMonitorDto();

        StockMonitorServiceImpl stockMonitorService = new StockMonitorServiceImpl();
        Long stopSendOrderTime = StockUpdateTask.createStopSendOrderTime(14, 58);
        StockUpdateContext ctx = new StockUpdateContext() {
            public StockPriceDto getStockPrice(String stockCode) {
                return null;
            }

            public List<StockOrderInfo> getOrderList() {
                return TestStockCheckAction.getOrderList();
            }

            public Result sendOrder(SendOrderInfo sendOrderInfo) {
                if(sendOrderInfo.getBuyOrSell().equals(BuyOrSellEnum.buy)){
                    return Result.createErrorResult("buy error", null);
                }else{
                    return Result.createErrorResult("[251005][证券可用数量不足]", TradeResultEnum.SELL_NO_MORE_STOCK);
                }
            }

            public Result cancelOrder(StockOrderInfo stockOrderInfo) {
                return Result.createSuccessResult("2");
            }

            public void fireStockListChange() {}

            @Override
            public StockMonitorService getStockMonitorService() {
                return stockMonitorService;
            }

            @Override
            public Long getStopSendOrderTime() {
                return stopSendOrderTime;
            }
        };

        StockCheckAction checkAction = new StockCheckAction();
        checkAction.check(stockMonitorDto, ctx);
    }

    private static StockMonitorBO createStockMonitorDto(){
        StockConfigDto stockConfigDto = new StockConfigDto();
        stockConfigDto.setStockCode("510050");
        stockConfigDto.setStockName("50ETF");
        stockConfigDto.setExchangeId("1");
        stockConfigDto.setBasePrice(2.693);
        stockConfigDto.setCreditBuy(false);
        stockConfigDto.setTradeUnit(1000);
        List<Double> priceList = new ArrayList<Double>();
        priceList.add(2.833);
        priceList.add(2.804);
        priceList.add(2.776);
        priceList.add(2.748);
        priceList.add(2.72);
        priceList.add(2.693);
        priceList.add(2.666);
        priceList.add(2.639);
        priceList.add(2.612);
        priceList.add(2.586);
        stockConfigDto.setGridPriceList(priceList);

        StockMonitorBO stockMonitorDto = new StockMonitorBO();
        stockMonitorDto.setStockConfig(stockConfigDto);
        return stockMonitorDto;
    }

    private static List<StockOrderInfo> getOrderList(){
        List<StockOrderInfo> orderList = new ArrayList<>();

        StockOrderInfo order1 = new StockOrderInfo();
        order1.setStockCode("510050");
        order1.setStockName("50ETF");
        order1.setOrderNumber("1");
        order1.setOrderStatus(OrderStatusEnum.status_8);
        order1.setOrderTime(System.currentTimeMillis());
        order1.setBuyOrSell(BuyOrSellEnum.sell);
        order1.setEntrustPrice(2.748);
        order1.setEntrustSize(1000);
        orderList.add(order1);

        return orderList;
    }
}
