package com.weifly.weistock.trade.config;

import com.weifly.weistock.trade.config.impl.XmlFileStockConfigService;
import org.junit.Test;

import java.util.List;

/**
 * 测试StockConfigService
 *
 * @author weifly
 * @since 2018/12/6
 */
public class TestStockConfigService {

    @Test
    public void testParseConfig(){
        XmlFileStockConfigService configService = new XmlFileStockConfigService();
        configService.setConfigPath("D:/TradeInfo.xml");
        AllStockConfig allStockConfig = configService.parseConfig();

        AccountConfigDto accountConfigDto = allStockConfig.getAccountConfig();

        List<StockConfigDto> stockConfigList = allStockConfig.getStockConfigList();
        System.out.println(stockConfigList.size());

        StockConfigDto stockConfig = stockConfigList.get(0);
        System.out.println(stockConfig.getStockCode());
        System.out.println(stockConfig.getExchangeId());
        System.out.println(stockConfig.getGridPriceList().size());

        stockConfig.setBasePrice(1.35);

        configService.saveConfig(allStockConfig);
    }
}
