package com.weifly.weistock.trade.market;

import com.weifly.weistock.core.market.StockMarketService;
import com.weifly.weistock.core.market.StockPriceDto;
import com.weifly.weistock.core.market.impl.SinaStockMarketService;
import com.weifly.weistock.core.util.WeistockUtils;
import org.junit.Test;

/**
 * 测试StockMarketService
 *
 * @author weifly
 * @since 2019/8/13
 */
public class TestStockMarketService {

    @Test
    public void testGetStockPrice(){
        StockMarketService stockMarketService = new SinaStockMarketService();
        StockPriceDto stockPrice = stockMarketService.getStockPrice("510300");
        System.out.println(WeistockUtils.toJsonString(stockPrice));
    }

    @Test
    public void testGetHkStockPrice(){
        StockMarketService stockMarketService = new SinaStockMarketService();
        StockPriceDto stockPrice = stockMarketService.getStockPrice("hk00981");
        System.out.println(WeistockUtils.toJsonString(stockPrice));
    }
}
