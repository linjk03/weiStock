package com.weifly.weistock.trade.trade;

import com.weifly.weistock.trade.enums.BuyOrSellEnum;
import com.weifly.weistock.trade.enums.OrderStatusEnum;

/**
 * 委托单信息
 *
 * @author weifly
 * @since 2018/9/20
 */
public class StockOrderInfo {

    private String stockName; // 名称
    private String stockCode; // 代码
    private String exchangeId; // 交易所ID，0-深圳，1-上海
    private String orderNumber;  // 委托单号
    private OrderStatusEnum orderStatus;    // 委托状态
    private Long orderTime;  // 委托时间
    private BuyOrSellEnum buyOrSell; // 委托方向，1-买入、2-卖出
    private Double entrustPrice;   // 委托价格
    private Integer entrustSize;   // 委托数量
    private boolean creditBuy;  // 融资买入

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getExchangeId() {
        return exchangeId;
    }

    public void setExchangeId(String exchangeId) {
        this.exchangeId = exchangeId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public OrderStatusEnum getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatusEnum orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Long getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Long orderTime) {
        this.orderTime = orderTime;
    }

    public BuyOrSellEnum getBuyOrSell() {
        return buyOrSell;
    }

    public void setBuyOrSell(BuyOrSellEnum buyOrSell) {
        this.buyOrSell = buyOrSell;
    }

    public Double getEntrustPrice() {
        return entrustPrice;
    }

    public void setEntrustPrice(Double entrustPrice) {
        this.entrustPrice = entrustPrice;
    }

    public Integer getEntrustSize() {
        return entrustSize;
    }

    public void setEntrustSize(Integer entrustSize) {
        this.entrustSize = entrustSize;
    }

    public boolean isCreditBuy() {
        return creditBuy;
    }

    public void setCreditBuy(boolean creditBuy) {
        this.creditBuy = creditBuy;
    }
}
