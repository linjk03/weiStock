package com.weifly.weistock.trade.bo;

import com.weifly.weistock.core.common.StockException;
import com.weifly.weistock.core.market.StockPriceDto;
import com.weifly.weistock.trade.config.StockConfigDto;
import com.weifly.weistock.trade.enums.BuyOrSellEnum;
import com.weifly.weistock.trade.trade.StockOrderInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * 股票监控对象
 *
 * @author weifly
 * @since 2019/8/9
 */
public class StockMonitorBO {

    private StockConfigDto stockConfig; // 股票配置

    private Double newestPrice; // 最新价
    private Long newestPriceUpdateTime; // 最新价更新时间
    private StockPriceDto newestPriceDto; // 最新价信息
    private List<StockPriceDto> priceList = new ArrayList<>(); // 股票历史价格

    private StockOrderInfo buyOrderInfo;  // 买入委托单
    private Long buyOrderUpdateTime; // 买入委托单更新时间

    private StockOrderInfo sellOrderInfo;// 卖出委托单
    private Long sellOrderUpdateTime; // 卖出委托单更新时间

    /**
     * 获取买入价格
     *
     * @return
     */
    public Double takeBuyPrice(){
        int basePriceIndex = this.takeBasePriceIndex();
        if(basePriceIndex==-1){
            throw new RuntimeException("gridPriceList中查不到basePrice");
        }
        // 计算buyPriceIndex
        int buyPriceIndex;
        if(BuyOrSellEnum.buy.equals(this.stockConfig.getLastOperation())){
            buyPriceIndex = basePriceIndex + 1;
        }else if(BuyOrSellEnum.sell.equals(this.stockConfig.getLastOperation())){
            buyPriceIndex = basePriceIndex + this.stockConfig.getGridGapNumber();
        }else{
            throw new RuntimeException("不支持的lastOperation: " + this.stockConfig.getLastOperation());
        }
        if(buyPriceIndex>this.stockConfig.getGridPriceList().size()-1){
            throw new RuntimeException("gridPrice集合不够用了，不能确定买入价格，buyPriceIndex=" + buyPriceIndex);
        }
        return this.stockConfig.getGridPriceList().get(buyPriceIndex);
    }

    /**
     * 获取卖出价格
     *
     * @return
     */
    public Double takeSellPrice(){
        int basePriceIndex = this.takeBasePriceIndex();
        if(basePriceIndex==-1){
            throw new RuntimeException("gridPriceList中查不到basePrice");
        }
        // 计算sellPriceIndex
        int sellPriceIndex;
        if(BuyOrSellEnum.buy.equals(this.stockConfig.getLastOperation())){
            sellPriceIndex = basePriceIndex - this.stockConfig.getGridGapNumber();
        }else if(BuyOrSellEnum.sell.equals(this.stockConfig.getLastOperation())){
            sellPriceIndex = basePriceIndex - 1;
        }else{
            throw new RuntimeException("不支持的lastOperation: " + this.stockConfig.getLastOperation());
        }
        if(sellPriceIndex<0){
            throw new RuntimeException("gridPrice集合不够用了，不能确定卖出价格，sellPriceIndex=" + sellPriceIndex);
        }
        return this.stockConfig.getGridPriceList().get(sellPriceIndex);
    }

    private int takeBasePriceIndex(){
        Double basePrice = this.stockConfig.getBasePrice();
        List<Double> gridPriceList = this.stockConfig.getGridPriceList();
        if(basePrice==null){
            throw new StockException("basePrice为空！");
        }
        if(gridPriceList==null || gridPriceList.isEmpty()){
            throw new StockException("gridPriceList为空！");
        }

        int basePriceIndex = -1;
        for(int i=0;i<gridPriceList.size();i++){
            if(basePrice.equals(gridPriceList.get(i))){
                basePriceIndex = i;
                break;
            }
        }
        return basePriceIndex;
    }

    public StockConfigDto getStockConfig() {
        return stockConfig;
    }

    public void setStockConfig(StockConfigDto stockConfig) {
        this.stockConfig = stockConfig;
    }

    public Double getNewestPrice() {
        return newestPrice;
    }

    public void setNewestPrice(Double newestPrice) {
        this.newestPrice = newestPrice;
    }

    public Long getNewestPriceUpdateTime() {
        return newestPriceUpdateTime;
    }

    public void setNewestPriceUpdateTime(Long newestPriceUpdateTime) {
        this.newestPriceUpdateTime = newestPriceUpdateTime;
    }

    public StockPriceDto getNewestPriceDto() {
        return newestPriceDto;
    }

    public void setNewestPriceDto(StockPriceDto newestPriceDto) {
        this.newestPriceDto = newestPriceDto;
    }

    public List<StockPriceDto> getPriceList() {
        return priceList;
    }

    public void setPriceList(List<StockPriceDto> priceList) {
        this.priceList = priceList;
    }

    public StockOrderInfo getBuyOrderInfo() {
        return buyOrderInfo;
    }

    public void setBuyOrderInfo(StockOrderInfo buyOrderInfo) {
        this.buyOrderInfo = buyOrderInfo;
    }

    public Long getBuyOrderUpdateTime() {
        return buyOrderUpdateTime;
    }

    public void setBuyOrderUpdateTime(Long buyOrderUpdateTime) {
        this.buyOrderUpdateTime = buyOrderUpdateTime;
    }

    public StockOrderInfo getSellOrderInfo() {
        return sellOrderInfo;
    }

    public void setSellOrderInfo(StockOrderInfo sellOrderInfo) {
        this.sellOrderInfo = sellOrderInfo;
    }

    public Long getSellOrderUpdateTime() {
        return sellOrderUpdateTime;
    }

    public void setSellOrderUpdateTime(Long sellOrderUpdateTime) {
        this.sellOrderUpdateTime = sellOrderUpdateTime;
    }
}
