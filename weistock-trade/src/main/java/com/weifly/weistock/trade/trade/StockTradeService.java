package com.weifly.weistock.trade.trade;

import com.weifly.weistock.core.common.Result;
import com.weifly.weistock.core.common.StockException;
import com.weifly.weistock.trade.config.AccountConfigDto;
import com.weifly.weistock.trade.enums.BuyOrSellEnum;
import com.weifly.weistock.trade.enums.OrderStatusEnum;
import com.weifly.weistock.trade.monitor.StockMonitorService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 股票交易服务
 *
 * @author weifly
 * @since 2018/8/20
 */
public abstract class StockTradeService {

    protected Logger log = LoggerFactory.getLogger(this.getClass());

    private StockMonitorService stockMonitorService;

    public void setStockMonitorService(StockMonitorService stockMonitorService) {
        this.stockMonitorService = stockMonitorService;
    }

    /**
     * 获得股票委托列表
     *
     * @return
     */
    public abstract List<StockOrderInfo> getOrderList();

    /**
     * 委托下单
     * @param sendOrderInfo 下单信息
     * @return 委托信息
     */
    public abstract Result sendOrder(SendOrderInfo sendOrderInfo);

    /**
     * 撤销委托
     *
     * @param stockOrderInfo
     * @return
     */
    public abstract Result cancelOrder(StockOrderInfo stockOrderInfo);

    /**
     * 退出登录
     */
    public abstract void logoff();

    protected void setOrderTime(StockOrderInfo orderInfo, String orderTime){
        String[] timePart = orderTime.split(":");
        Calendar calc = Calendar.getInstance();
        calc.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timePart[0]));
        calc.set(Calendar.MINUTE, Integer.parseInt(timePart[1]));
        calc.set(Calendar.SECOND, Integer.parseInt(timePart[2]));
        calc.set(Calendar.MILLISECOND, 0);
        orderInfo.setOrderTime(calc.getTimeInMillis());
    }

    protected void setBuyOrSell(StockOrderInfo orderInfo, String flag){
        if(flag.equals("0")){
            orderInfo.setBuyOrSell(BuyOrSellEnum.buy); // 买入
        }else if(flag.equals("1")){
            orderInfo.setBuyOrSell(BuyOrSellEnum.sell); // 卖出
        }
    }

    protected void setOrderStatus(StockOrderInfo orderInfo, String status){
        for(OrderStatusEnum statusEnum : OrderStatusEnum.values()){
            if(statusEnum.getStatus().equals(status)){
                orderInfo.setOrderStatus(statusEnum);
                break;
            }
        }
    }

    protected void printOrderList(List<StockOrderInfo> orderList){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for(StockOrderInfo orderInfo : orderList){
            StringBuilder sb = new StringBuilder();
            sb.append("委托时间：").append(sdf.format(new Date(orderInfo.getOrderTime())));
            sb.append(", 申请编号: ").append(orderInfo.getOrderNumber());
            sb.append(", 证券代码: ").append(orderInfo.getStockCode());
            sb.append(", 证券名称: ").append(orderInfo.getStockName());
            sb.append(", 委托方向: ").append(orderInfo.getBuyOrSell().getName());
            sb.append(", 委托状态: ").append(orderInfo.getOrderStatus()==null?null:orderInfo.getOrderStatus().getStatus());
            sb.append(", 委托价格：").append(orderInfo.getEntrustPrice());
            sb.append(", 委托数量: ").append(orderInfo.getEntrustSize());
            log.info(sb.toString());
        }
    }

    protected AccountConfigDto takeAccountConfig(){
        AccountConfigDto accountConfig = this.stockMonitorService.getAccountConfig();
        if(accountConfig==null){
            throw new StockException("缺少股票账号配置");
        }
        if(accountConfig.getQsid()==null){
            throw new StockException("缺少股票账号配置: qsid");
        }
        if(StringUtils.isBlank(accountConfig.getServerIp())){
            throw new StockException("缺少股票账号配置: serverIp");
        }
        if(accountConfig.getServerPort()==null){
            throw new StockException("缺少股票账号配置: serverPort");
        }
        if(StringUtils.isBlank(accountConfig.getVersion())){
            throw new StockException("缺少股票账号配置: version");
        }
        if(accountConfig.getYybID()==null){
            throw new StockException("缺少股票账号配置: yybID");
        }
        if(accountConfig.getAccountType()==null){
            throw new StockException("缺少股票账号配置: accountType");
        }
        if(StringUtils.isBlank(accountConfig.getAccountNo())){
            throw new StockException("缺少股票账号配置: accountNo");
        }
        if(StringUtils.isBlank(accountConfig.getTradeAccount())){
            throw new StockException("缺少股票账号配置: tradeAccount");
        }
        if(StringUtils.isBlank(accountConfig.getJyPassword())){
            throw new StockException("缺少股票账号配置: jyPassword");
        }
        return accountConfig;
    }
}
