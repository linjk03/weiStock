package com.weifly.weistock.trade.config;

import com.weifly.weistock.trade.enums.BuyOrSellEnum;

import java.util.ArrayList;
import java.util.List;

/**
 * 股票配置信息
 *
 * @author weifly
 * @since 2019/8/7
 */
public class StockConfigDto {

    private String stockName; // 名称
    private String stockCode; // 代码
    private String exchangeId; // 交易所ID，0-深圳，1-上海
    private Boolean open; // 是否开启
    private Double basePrice; // 基准价格
    private Integer tradeUnit; // 每次交易量
    private BuyOrSellEnum lastOperation; // 最后一次操作类型，买或卖
    private Integer gridGapNumber; // 网格间隔数，1为对称网格，大于1为非对称网格
    private Double step; // 网格间距，支持小数，0 - 10
    private Boolean creditBuy;  // 融资买入
    private List<Double> gridPriceList = new ArrayList<Double>(); // 预设的网格价格，从高到低

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getExchangeId() {
        return exchangeId;
    }

    public void setExchangeId(String exchangeId) {
        this.exchangeId = exchangeId;
    }

    public Boolean getOpen() {
        return open;
    }

    public void setOpen(Boolean open) {
        this.open = open;
    }

    public Double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(Double basePrice) {
        this.basePrice = basePrice;
    }

    public Integer getTradeUnit() {
        return tradeUnit;
    }

    public void setTradeUnit(Integer tradeUnit) {
        this.tradeUnit = tradeUnit;
    }

    public BuyOrSellEnum getLastOperation() {
        return lastOperation;
    }

    public void setLastOperation(BuyOrSellEnum lastOperation) {
        this.lastOperation = lastOperation;
    }

    public Integer getGridGapNumber() {
        return gridGapNumber;
    }

    public void setGridGapNumber(Integer gridGapNumber) {
        this.gridGapNumber = gridGapNumber;
    }

    public Double getStep() {
        return step;
    }

    public void setStep(Double step) {
        this.step = step;
    }

    public Boolean getCreditBuy() {
        return creditBuy;
    }

    public void setCreditBuy(Boolean creditBuy) {
        this.creditBuy = creditBuy;
    }

    public List<Double> getGridPriceList() {
        return gridPriceList;
    }

    public void setGridPriceList(List<Double> gridPriceList) {
        this.gridPriceList = gridPriceList;
    }
}
