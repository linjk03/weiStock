package com.weifly.weistock.trade;

import com.weifly.weistock.core.util.ModuleBuilder;
import com.weifly.weistock.trade.config.AllStockConfig;
import com.weifly.weistock.trade.config.StockConfigService;
import com.weifly.weistock.trade.monitor.StockMonitorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * 网格交易builder
 *
 * @author weifly
 * @since 2019/8/8
 */
public class GridTradeBuilder implements ModuleBuilder {

    private Logger log = LoggerFactory.getLogger(GridTradeBuilder.class);

    private StockConfigService stockConfigService;
    private StockMonitorService stockMonitorService;

    public void build(String configPath){
        String tradeInfoXml = new File(configPath, "TradeInfo.xml").getAbsolutePath();
        log.info("构建网格交易模块: " + tradeInfoXml);

        this.stockConfigService.setConfigPath(tradeInfoXml);

        // 加载配置
        AllStockConfig allStockConfig = this.stockConfigService.parseConfig();
        this.stockMonitorService.updateAccountConfig(allStockConfig.getAccountConfig(), false);
        this.stockMonitorService.updateStockConfigList(allStockConfig.getStockConfigList(), false);
    }

    public void setStockConfigService(StockConfigService stockConfigService) {
        this.stockConfigService = stockConfigService;
    }

    public void setStockMonitorService(StockMonitorService stockMonitorService) {
        this.stockMonitorService = stockMonitorService;
    }
}
