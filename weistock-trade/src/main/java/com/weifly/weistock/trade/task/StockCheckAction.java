package com.weifly.weistock.trade.task;

import com.weifly.weistock.core.common.Result;
import com.weifly.weistock.core.market.StockPriceDto;
import com.weifly.weistock.trade.config.StockConfigDto;
import com.weifly.weistock.trade.enums.BuyOrSellEnum;
import com.weifly.weistock.trade.enums.OrderStatusEnum;
import com.weifly.weistock.trade.enums.SendCategory;
import com.weifly.weistock.trade.enums.TradeResultEnum;
import com.weifly.weistock.trade.bo.StockMonitorBO;
import com.weifly.weistock.trade.trade.SendOrderInfo;
import com.weifly.weistock.trade.trade.StockOrderInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 网格交易逻辑处理
 *
 * @author weifly
 * @since 2018/11/3
 */
public class StockCheckAction {

    private Logger logger = LoggerFactory.getLogger(StockCheckAction.class);

    /**
     * 检查一只股票的信息
     * @param stockMonitorDto
     * @param ctx
     */
    public void check(StockMonitorBO stockMonitorDto, StockUpdateContext ctx){
        // 获得股票价格
        StockPriceDto price = ctx.getStockPrice(stockMonitorDto.getStockConfig().getStockCode());
        if(price==null || price.getNowPrice()==null || price.getNowPrice()<=0){
            logger.warn("无法获得价格："+stockMonitorDto.getStockConfig().getStockCode());
        }else{
            // logger.info("获得股票价格，代码="+stockInfo.getStockCode()+", 最新价="+price.getNowPrice());
            this.updateStockPrice(stockMonitorDto, price);
        }

        // 处理买入
        if(stockMonitorDto.getBuyOrderInfo()==null){
            // 没有委托单号，查询委托情况
            this.findOrCreateBuyOrder(stockMonitorDto, ctx);
        }else{
            // 有委托单号，检查是否成交
            this.checkBuyOrder(stockMonitorDto, ctx);
        }

        // 处理卖出
        if(stockMonitorDto.getSellOrderInfo()==null){
            // 没有委托单号，查询委托情况
            this.findOrCreateSellOrder(stockMonitorDto, ctx);
        }else{
            // 有委托单号，检查是否成交
            this.checkSellOrder(stockMonitorDto, ctx);
        }

        // 比对价格
        if(stockMonitorDto.getBuyOrderInfo()!=null){
            if(!stockMonitorDto.getBuyOrderInfo().getEntrustPrice().equals(stockMonitorDto.takeBuyPrice())){
                // 买入委托价格 != 网格买入价，说明卖出成交了
                stockMonitorDto.setBuyOrderInfo(null);
                stockMonitorDto.setBuyOrderUpdateTime(System.currentTimeMillis());
            }
        }
        if(stockMonitorDto.getSellOrderInfo()!=null){
            if(!stockMonitorDto.getSellOrderInfo().getEntrustPrice().equals(stockMonitorDto.takeSellPrice())){
                // 卖出委托价格 != 网格卖出价，说明买入成交了
                stockMonitorDto.setSellOrderInfo(null);
                stockMonitorDto.setSellOrderUpdateTime(System.currentTimeMillis());
            }
        }
    }

    // 更新股票价格信息
    private void updateStockPrice(StockMonitorBO stockMonitorDto, StockPriceDto price){
        stockMonitorDto.setNewestPrice(price.getNowPrice());
        stockMonitorDto.setNewestPriceUpdateTime(price.getUpdateTime());
        stockMonitorDto.setNewestPriceDto(price);
        stockMonitorDto.getPriceList().add(price);

        // 保留2分钟的价格信息
        long startTime = System.currentTimeMillis() - 120*1000;
        List<StockPriceDto> removedPriceList = new ArrayList<>();
        for(StockPriceDto priceInfo : stockMonitorDto.getPriceList()){
            if(priceInfo.getUpdateTime()<startTime){
                removedPriceList.add(priceInfo);
            }else{
                break;
            }
        }
        for(StockPriceDto removedPrice : removedPriceList){
            stockMonitorDto.getPriceList().remove(removedPrice);
        }
    }

    private void findOrCreateBuyOrder(StockMonitorBO stockMonitorDto, StockUpdateContext ctx){
        List<StockOrderInfo> orderList = ctx.getOrderList();
        Double buyPrice = stockMonitorDto.takeBuyPrice();
        StockOrderInfo targetOrder = this.findOrder(orderList, stockMonitorDto.getStockConfig().getStockCode(), buyPrice, BuyOrSellEnum.buy);
        if(targetOrder==null){
            // 下单
            this.sendBuyOrder(stockMonitorDto, ctx, buyPrice);
        }else{
            // 设置单
            stockMonitorDto.setBuyOrderInfo(targetOrder);
            stockMonitorDto.setBuyOrderUpdateTime(System.currentTimeMillis());
            this.printOperateInfo(ctx,"关联买入单", stockMonitorDto, targetOrder);
        }
    }

    private void checkBuyOrder(StockMonitorBO stockMonitorDto, StockUpdateContext ctx){
        boolean checkReady = (System.currentTimeMillis() - stockMonitorDto.getBuyOrderUpdateTime()) > 30 *1000; // 买入委托单更新间隔大于30秒
        if(!checkReady){
            long buyOrderUpdateTime = stockMonitorDto.getBuyOrderUpdateTime();
            double buyOrderPrice = stockMonitorDto.getBuyOrderInfo().getEntrustPrice();
            for(StockPriceDto priceInfo : stockMonitorDto.getPriceList()){
                if(priceInfo.getUpdateTime()>=buyOrderUpdateTime && priceInfo.getNowPrice().doubleValue()<=buyOrderPrice){
                    checkReady = true; // 上次更新时间 到 当前时间，价格曾经低于买入价格
                    break;
                }
            }
        }

        if(checkReady){
            List<StockOrderInfo> orderList = ctx.getOrderList();
            StockOrderInfo targetBuyOrder = this.findOrder(orderList, stockMonitorDto.getBuyOrderInfo().getOrderNumber());
            if(targetBuyOrder==null){
                logger.info("没有查找到指定的委托单："+stockMonitorDto.getBuyOrderInfo().getOrderNumber());
                return;
            }

            switch(targetBuyOrder.getOrderStatus()){
                case status_1: // 待撤
                case status_2: // 正撤
                case status_3: // 部撤
                case status_4: // 已撤
                    ctx.getStockMonitorService().writeMessage("买入委托单，已申请撤销，清除买入委托单 " + targetBuyOrder.getOrderNumber());
                    stockMonitorDto.setBuyOrderInfo(null);
                    stockMonitorDto.setBuyOrderUpdateTime(System.currentTimeMillis());
                    break;

                case status_5: // 未报
                case status_6: // 待报
                case status_7: // 正报
                case status_8: // 已报
                    ctx.getStockMonitorService().writeMessage("买入委托单，还没有成交 "+targetBuyOrder.getOrderNumber());
                    stockMonitorDto.setBuyOrderInfo(targetBuyOrder);
                    stockMonitorDto.setBuyOrderUpdateTime(System.currentTimeMillis());
                    break;

                case status_9: // 部成
                    ctx.getStockMonitorService().writeMessage("买入委托单，部分成交 "+targetBuyOrder.getOrderNumber());
                    stockMonitorDto.setBuyOrderInfo(targetBuyOrder);
                    stockMonitorDto.setBuyOrderUpdateTime(System.currentTimeMillis());
                    break;

                case status_10: // 已成
                    this.updateBuyOrder(stockMonitorDto, ctx, targetBuyOrder);
                    break;

                case status_11: // 撤废
                    ctx.getStockMonitorService().writeMessage("买入委托单，撤废！等待下次检查 " + targetBuyOrder.getOrderNumber());
                    break;

                case status_12: // 废单
                    ctx.getStockMonitorService().writeMessage("买入委托单，废单！清除买入委托单 "+targetBuyOrder.getOrderNumber());
                    stockMonitorDto.setBuyOrderInfo(null);
                    stockMonitorDto.setBuyOrderUpdateTime(System.currentTimeMillis());
                    break;
            }
        }
    }

    /**
     * 买入委托单成交，更新各种信息
     *
     * @param stockMonitorDto
     * @param ctx
     * @param buyOrder
     */
    private void updateBuyOrder(StockMonitorBO stockMonitorDto, StockUpdateContext ctx, StockOrderInfo buyOrder){
        this.printOperateInfo(ctx,"已成交", stockMonitorDto, buyOrder);

        // 更新基准价、最后一次操作类型
        Double newBasePrice = stockMonitorDto.takeBuyPrice();
        stockMonitorDto.getStockConfig().setBasePrice(newBasePrice);
        stockMonitorDto.getStockConfig().setLastOperation(BuyOrSellEnum.buy);

        // 写回TradeInfo.xml
        ctx.getStockMonitorService().saveConfig();

        // 清除买入委托单
        stockMonitorDto.setBuyOrderInfo(null);
        stockMonitorDto.setBuyOrderUpdateTime(System.currentTimeMillis());

        // 下单或关联已有单
        this.findOrCreateBuyOrder(stockMonitorDto, ctx);
    }

    // 买入委托单，下单
    private void sendBuyOrder(StockMonitorBO stockMonitorDto, StockUpdateContext ctx, Double buyPrice){
        // 判断跌停价
        if(stockMonitorDto.getNewestPriceDto()!=null){
            Double downPrice = stockMonitorDto.getNewestPriceDto().getDownPrice();
            if(downPrice!=null && buyPrice<downPrice){
                logger.info("买入价小于跌停价，不发送买入委托单 stockCode=" + stockMonitorDto.getStockConfig().getStockCode());
                return;
            }
        }
        // 判断停止下单时间
        if(ctx.getStopSendOrderTime()!=null){
            if(System.currentTimeMillis()>=ctx.getStopSendOrderTime()){
                String msg = "超过" + StockUpdateTask.formatStopSendOrderTime(ctx.getStopSendOrderTime()) +" 不发送买入委托单 stockCode=" + stockMonitorDto.getStockConfig().getStockCode();
                ctx.getStockMonitorService().writeMessage(msg);
                return;
            }
        }

        SendOrderInfo sendOrder = this.createSendOrder(stockMonitorDto, BuyOrSellEnum.buy, buyPrice);
        Result sendResult = ctx.sendOrder(sendOrder);
        if(sendResult.isSuccess()){
            StockOrderInfo targetOrder = (StockOrderInfo)sendResult.getData();
            stockMonitorDto.setBuyOrderInfo(targetOrder);
            stockMonitorDto.setBuyOrderUpdateTime(System.currentTimeMillis());
            this.printOperateInfo(ctx,"下单买入", stockMonitorDto, targetOrder);
        }else{
            // 判断原因
            ctx.getStockMonitorService().writeMessage("买入下单失败，请查找原因！"+stockMonitorDto.getStockConfig().getStockCode()+" -- "+sendResult.getMessage());
        }
    }

    private void printOperateInfo(StockUpdateContext ctx, String msg, StockMonitorBO stockMonitorDto, StockOrderInfo orderInfo){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        StringBuilder sb = new StringBuilder();
        sb.append(msg);
        sb.append(", 委托时间：").append(sdf.format(new Date(orderInfo.getOrderTime())));
        sb.append(", 申请编号: ").append(orderInfo.getOrderNumber());
        sb.append(", 证券代码: ").append(orderInfo.getStockCode());
        sb.append(", 证券名称: ").append(orderInfo.getStockName());
        sb.append(", 委托方向: ").append(orderInfo.getBuyOrSell().getName());
        sb.append(", 委托状态: ").append(orderInfo.getOrderStatus().getStatus());
        sb.append(", 委托价格：").append(orderInfo.getEntrustPrice());
        sb.append(", 委托数量: ").append(orderInfo.getEntrustSize());
        ctx.getStockMonitorService().writeMessage(sb.toString());
    }

    // 查找指定价格、未成交的订单
    private StockOrderInfo findOrder(List<StockOrderInfo> orderList, String stockCode, Double targetPrice, BuyOrSellEnum buyEnum){
        for(StockOrderInfo order : orderList){
            boolean validOrder = (order.getOrderStatus()!=null) && ((order.getOrderStatus().equals(OrderStatusEnum.status_5)
                    || order.getOrderStatus().equals(OrderStatusEnum.status_6)
                    || order.getOrderStatus().equals(OrderStatusEnum.status_7)
                    || order.getOrderStatus().equals(OrderStatusEnum.status_8)));
            if(order.getStockCode().equals(stockCode) // 指定股票
                    && order.getEntrustPrice().equals(targetPrice) // 指定价格
                    && order.getBuyOrSell().equals(buyEnum) // 买或卖
                    && validOrder){ // 未成交
                return order;
            }
        }
        return null;
    }

    /**
     * 根据委托单号，查找委托单
     *
     * @param orderList
     * @param orderNumber
     * @return
     */
    private StockOrderInfo findOrder(List<StockOrderInfo> orderList, String orderNumber){
        for(StockOrderInfo order : orderList){
            if(orderNumber.equals(order.getOrderNumber())){
                return order;
            }
        }
        return null;
    }

    /**
     * 创建委托单
     *
     * @param stockMonitorDto
     * @param buyOrSell
     * @param entrustPrice 委托价格
     * @return
     */
    private SendOrderInfo createSendOrder(StockMonitorBO stockMonitorDto, BuyOrSellEnum buyOrSell, Double entrustPrice){
        SendOrderInfo sendOrder = new SendOrderInfo();

        StockConfigDto stockConfigDto = stockMonitorDto.getStockConfig();
        if(buyOrSell.equals(BuyOrSellEnum.buy)){
            if(stockConfigDto.getCreditBuy()){
                sendOrder.setCategory(SendCategory.category_2); // 融资买入
            }else{
                sendOrder.setCategory(SendCategory.category_0); // 担保品买入
            }
        }else if(buyOrSell.equals(BuyOrSellEnum.sell)){
            if(stockConfigDto.getCreditBuy()){
                sendOrder.setCategory(SendCategory.category_1); // 卖出
            }else{
                sendOrder.setCategory(SendCategory.category_1); // 卖出
            }
        }else{
            throw new RuntimeException("不支持的类型 "+buyOrSell);
        }

        sendOrder.setStockCode(stockConfigDto.getStockCode());
        sendOrder.setEntrustPrice(entrustPrice);
        sendOrder.setEntrustSize(stockConfigDto.getTradeUnit());

        sendOrder.setStockName(stockConfigDto.getStockName());
        sendOrder.setBuyOrSell(buyOrSell);
        sendOrder.setCreditBuy(stockConfigDto.getCreditBuy());

        return sendOrder;
    }

    private void findOrCreateSellOrder(StockMonitorBO stockMonitorDto, StockUpdateContext ctx){
        List<StockOrderInfo> orderList = ctx.getOrderList();
        Double sellPrice = stockMonitorDto.takeSellPrice();
        StockOrderInfo targetOrder = this.findOrder(orderList, stockMonitorDto.getStockConfig().getStockCode(), sellPrice, BuyOrSellEnum.sell);
        if(targetOrder==null){
            // 下单
            this.sendSellOrder(stockMonitorDto, ctx, sellPrice);
        }else{
            // 设置单
            stockMonitorDto.setSellOrderInfo(targetOrder);
            stockMonitorDto.setSellOrderUpdateTime(System.currentTimeMillis());
            this.printOperateInfo(ctx,"关联卖出单", stockMonitorDto, targetOrder);
        }
    }

    private void checkSellOrder(StockMonitorBO stockMonitorDto, StockUpdateContext ctx){
        boolean checkReady = (System.currentTimeMillis() - stockMonitorDto.getSellOrderUpdateTime()) > 30 *1000; // 卖出委托单更新间隔大于30秒
        if(!checkReady){
            long sellOrderUpdateTime = stockMonitorDto.getSellOrderUpdateTime();
            double sellOrderPrice = stockMonitorDto.getSellOrderInfo().getEntrustPrice();
            for(StockPriceDto priceInfo : stockMonitorDto.getPriceList()){
                if(priceInfo.getUpdateTime()>=sellOrderUpdateTime && priceInfo.getNowPrice().doubleValue()>=sellOrderPrice){
                    checkReady = true; // 上次更新时间 到 当前时间，价格曾经大于卖出价格
                    break;
                }
            }
        }

        if(checkReady){
            List<StockOrderInfo> orderList = ctx.getOrderList();
            StockOrderInfo targetSellOrder = this.findOrder(orderList, stockMonitorDto.getSellOrderInfo().getOrderNumber());
            if(targetSellOrder==null){
                logger.info("没有查找到指定的卖出委托单："+stockMonitorDto.getSellOrderInfo().getOrderNumber());
                return;
            }

            switch(targetSellOrder.getOrderStatus()){
                case status_1: // 待撤
                case status_2: // 正撤
                case status_3: // 部撤
                case status_4: // 已撤
                    ctx.getStockMonitorService().writeMessage("卖出委托单，已申请撤销，清除卖出委托单 " + targetSellOrder.getOrderNumber());
                    stockMonitorDto.setSellOrderInfo(null);
                    stockMonitorDto.setSellOrderUpdateTime(System.currentTimeMillis());
                    break;

                case status_5: // 未报
                case status_6: // 待报
                case status_7: // 正报
                case status_8: // 已报
                    ctx.getStockMonitorService().writeMessage("卖出委托单，还没有成交 "+targetSellOrder.getOrderNumber());
                    stockMonitorDto.setSellOrderInfo(targetSellOrder);
                    stockMonitorDto.setSellOrderUpdateTime(System.currentTimeMillis());
                    break;

                case status_9: // 部成
                    ctx.getStockMonitorService().writeMessage("卖出委托单，部分成交 "+targetSellOrder.getOrderNumber());
                    stockMonitorDto.setSellOrderInfo(targetSellOrder);
                    stockMonitorDto.setSellOrderUpdateTime(System.currentTimeMillis());
                    break;

                case status_10: // 已成
                    this.updateSellOrder(stockMonitorDto, ctx, targetSellOrder);
                    break;

                case status_11: // 撤废
                    ctx.getStockMonitorService().writeMessage("卖出委托单，撤废！等待下次检查 " + targetSellOrder.getOrderNumber());
                    break;

                case status_12: // 废单
                    ctx.getStockMonitorService().writeMessage("卖出委托单，废单！清除卖出委托单 "+targetSellOrder.getOrderNumber());
                    stockMonitorDto.setSellOrderInfo(null);
                    stockMonitorDto.setSellOrderUpdateTime(System.currentTimeMillis());
                    break;
            }
        }
    }

    // 卖出委托单，下单
    private void sendSellOrder(StockMonitorBO stockMonitorDto, StockUpdateContext ctx, Double sellPrice){
        // 判断涨停价
        if(stockMonitorDto.getNewestPriceDto()!=null){
            Double upPrice = stockMonitorDto.getNewestPriceDto().getUpPrice();
            if(upPrice!=null && sellPrice>upPrice){
                logger.info("卖出价大于涨停价，不发送卖出委托单 stockCode=" + stockMonitorDto.getStockConfig().getStockCode());
                return;
            }
        }
        // 判断停止下单时间
        if(ctx.getStopSendOrderTime()!=null){
            if(System.currentTimeMillis()>=ctx.getStopSendOrderTime()){
                String msg = "超过" + StockUpdateTask.formatStopSendOrderTime(ctx.getStopSendOrderTime()) +" 不发送卖出委托单 stockCode=" + stockMonitorDto.getStockConfig().getStockCode();
                ctx.getStockMonitorService().writeMessage(msg);
                return;
            }
        }

        SendOrderInfo sendOrder = this.createSendOrder(stockMonitorDto, BuyOrSellEnum.sell, sellPrice);
        Result sendResult = ctx.sendOrder(sendOrder);
        if(sendResult.isSuccess()){
            StockOrderInfo targetOrder = (StockOrderInfo)sendResult.getData();
            stockMonitorDto.setSellOrderInfo(targetOrder);
            stockMonitorDto.setSellOrderUpdateTime(System.currentTimeMillis());
            this.printOperateInfo(ctx,"下单卖出", stockMonitorDto, targetOrder);
        }else{
            // 判断原因
            ctx.getStockMonitorService().writeMessage("卖出下单失败，请查找原因！"+stockMonitorDto.getStockConfig().getStockCode()+" -- "+sendResult.getMessage());
            TradeResultEnum tradeResult = (TradeResultEnum) sendResult.getData();
            if(TradeResultEnum.SELL_NO_MORE_STOCK.equals(tradeResult)){
                this.cancelStockSellOrder(stockMonitorDto, ctx);
            }
        }
    }

    // 撤销指定证券的委托单
    private void cancelStockSellOrder(StockMonitorBO stockMonitorDto, StockUpdateContext ctx){
        ctx.getStockMonitorService().writeMessage("尝试撤卖单：" + stockMonitorDto.getStockConfig().getStockCode());
        List<StockOrderInfo> orderList = ctx.getOrderList();
        if(orderList!=null && orderList.size()>0){
            for(StockOrderInfo orderInfo : orderList){
                // 相同的股票
                if(stockMonitorDto.getStockConfig().getStockCode().equals(orderInfo.getStockCode())){
                    // 卖出委托
                    if(BuyOrSellEnum.sell.equals(orderInfo.getBuyOrSell())){
                        switch(orderInfo.getOrderStatus()){
                            case status_1: // 待撤
                            case status_2: // 正撤
                            case status_3: // 部撤
                            case status_4: // 已撤
                                break;
                            case status_5: // 未报
                            case status_6: // 待报
                            case status_7: // 正报
                            case status_8: // 已报
                                orderInfo.setExchangeId(stockMonitorDto.getStockConfig().getExchangeId()); // 设置交易所ID
                                Result cancelResult = ctx.cancelOrder(orderInfo);
                                if(cancelResult.isSuccess()){
                                    this.printOperateInfo(ctx,"撤销卖出", stockMonitorDto, orderInfo);
                                }else{
                                    ctx.getStockMonitorService().writeMessage("撤销卖出失败，请查找原因！"+stockMonitorDto.getStockConfig().getStockCode()+" -- "+cancelResult.getMessage());
                                }
                                break;
                            case status_9: // 部成
                                break;
                            case status_10: // 已成
                                break;
                            case status_11: // 撤废
                                break;
                            case status_12: // 废单
                                break;
                        }
                    }
                }
            }
        }
    }

    /**
     * 卖出委托单成交，更新各种信息
     *
     * @param stockMonitorDto
     * @param ctx
     * @param sellOrder
     */
    private void updateSellOrder(StockMonitorBO stockMonitorDto, StockUpdateContext ctx, StockOrderInfo sellOrder){
        this.printOperateInfo(ctx,"已成交", stockMonitorDto, sellOrder);

        // 更新基准价、最后一次操作类型
        Double newBasePrice = stockMonitorDto.takeSellPrice();
        stockMonitorDto.getStockConfig().setBasePrice(newBasePrice);
        stockMonitorDto.getStockConfig().setLastOperation(BuyOrSellEnum.sell);

        // 写回TradeInfo.xml
        ctx.getStockMonitorService().saveConfig();

        // 清除卖出委托单
        stockMonitorDto.setSellOrderInfo(null);
        stockMonitorDto.setSellOrderUpdateTime(System.currentTimeMillis());

        // 下单或关联已有单
        this.findOrCreateSellOrder(stockMonitorDto, ctx);
    }
}
