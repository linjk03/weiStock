package com.weifly.weistock.trade.config;

import java.util.List;

/**
 * 所有配置信息
 *
 * @author weifly
 * @since 2019/8/13
 */
public class AllStockConfig {

    private AccountConfigDto accountConfig;
    private List<StockConfigDto> stockConfigList;

    public AccountConfigDto getAccountConfig() {
        return accountConfig;
    }

    public void setAccountConfig(AccountConfigDto accountConfig) {
        this.accountConfig = accountConfig;
    }

    public List<StockConfigDto> getStockConfigList() {
        return stockConfigList;
    }

    public void setStockConfigList(List<StockConfigDto> stockConfigList) {
        this.stockConfigList = stockConfigList;
    }
}
