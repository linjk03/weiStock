package com.weifly.weistock.trade.enums;

/**
 * @author weifly
 * @since 2018/11/16
 */
public enum BuyOrSellEnum {
    buy(1, "买"), sell(2, "卖");

    private int value;
    private String name;

    BuyOrSellEnum(int value, String name){
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static BuyOrSellEnum findByEnumName(String name){
        for(BuyOrSellEnum en : BuyOrSellEnum.values()){
            if(en.name().equalsIgnoreCase(name)){
                return en;
            }
        }
        return null;
    }
}
