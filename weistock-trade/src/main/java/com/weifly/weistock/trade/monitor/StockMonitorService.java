package com.weifly.weistock.trade.monitor;

import com.weifly.weistock.core.task.UpdateStatus;
import com.weifly.weistock.trade.bo.StockMonitorBO;
import com.weifly.weistock.trade.config.AccountConfigDto;
import com.weifly.weistock.trade.config.StockConfigDto;
import com.weifly.weistock.trade.dto.GridStatusDTO;

import java.util.List;

/**
 * 股票监听服务
 *
 * @author weifly
 * @since 2018/8/20
 */
public interface StockMonitorService {

    /**
     * 更新股票账号
     */
    void updateAccountConfig(AccountConfigDto accountConfig, boolean save);

    /**
     * 获得股票账号
     */
    AccountConfigDto getAccountConfig();

    /**
     * 更新股票配置列表
     */
    void updateStockConfigList(List<StockConfigDto> stockConfigList, boolean save);

    /**
     * 手工更新股票配置
     */
    void updateStockConfig(StockConfigDto stockConfig);

    /**
     * 清空股票监控信息
     */
    void clearStockMonitorInfo();

    /**
     * 获得股票监控列表
     */
    List<StockMonitorBO> getStockList();

    /**
     * 更新状态信息
     */
    void updateStatus(UpdateStatus status);

    /**
     * 计算统计信息
     *
     * @return
     */
    GridStatusDTO calcMonitorInfo();

    /**
     * 保存股票配置信息
     */
    void saveConfig();

    /**
     * 写入日志消息
     */
    void writeMessage(String message);
}
