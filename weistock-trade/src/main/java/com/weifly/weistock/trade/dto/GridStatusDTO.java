package com.weifly.weistock.trade.dto;

import com.weifly.weistock.core.monitor.bo.MessageBO;

import java.util.List;

/**
 * 网格交易监控信息
 *
 * @author weifly
 * @since 2019/2/1
 */
public class GridStatusDTO {

    private int status; // 状态
    private String date; // 日期
    private String time; // 时间
    private List<StockDTO> stockList; // 股票列表
    private List<MessageBO> messageList; // 消息列表

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<StockDTO> getStockList() {
        return stockList;
    }

    public void setStockList(List<StockDTO> stockList) {
        this.stockList = stockList;
    }

    public List<MessageBO> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<MessageBO> messageList) {
        this.messageList = messageList;
    }
}
