package com.weifly.weistock.trade.trade.jni;

import com.sun.jna.Library;
import com.sun.jna.Pointer;

/**
 * 通达信dll接口
 *
 * @author weifly
 * @since 2019/1/28
 */
public interface TdxLibrary extends Library
{
    //基本版函数
    public void OpenTdx();
    public void CloseTdx();
    public int Logon(String IP, short  Port, String Version, short  YybID, String AccountNo, String TradeAccount, String JyPassword, String TxPassword, byte[] ErrInfo);
    public void Logoff(int ClientID);
    public void QueryData(int ClientID, int Category,  byte[] Result,  byte[] ErrInfo);
    public void SendOrder(int ClientID, int Category, int PriceType, String Gddm, String Zqdm, float Price, int Quantity, byte[] Result,  byte[] ErrInfo);
    public void CancelOrder(int ClientID, String ExchangeID, String hth,  byte[] Result,  byte[] ErrInfo);
    public void GetQuote(int ClientID, String Zqdm,  byte[] Result,  byte[] ErrInfo);
    public void Repay(int ClientID, String Amount, byte[] Result,  byte[] ErrInfo);

    //普通批量版新增的函数
    public  void QueryHistoryData(int ClientID, int Category, String StartDate, String EndDate, byte[] Result,  byte[] ErrInfo);
    public  void QueryDatas(int ClientID, int[] Category, int Count, Pointer[] Result, Pointer[] ErrInfo);
    public  void SendOrders(int ClientID, int[] Category, int[] PriceType, String[] Gddm, String[] Zqdm, float[] Price, int[] Quantity, int Count,Pointer[] Result, Pointer[] ErrInfo);
    public  void CancelOrders(int ClientID, String[] ExchangeID,  String[] hth, int Count, Pointer[] Result, Pointer[] ErrInfo);
    public  void GetQuotes(int ClientID, String[] Zqdm, int Count,Pointer[] Result, Pointer[] ErrInfo);
}
