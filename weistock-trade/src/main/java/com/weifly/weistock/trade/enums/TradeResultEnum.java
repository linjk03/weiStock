package com.weifly.weistock.trade.enums;

/**
 * 交易结果枚举
 *
 * @author weifly
 * @since 2019/3/7
 */
public enum TradeResultEnum {

    PARAM_ERROR(1, "缺少参数"),
    SELL_NO_MORE_STOCK(2, "证券可用数量不足"),
    UNKOWN(3, "未知错误");

    private int value;
    private String name;

    TradeResultEnum(int value, String name){
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
