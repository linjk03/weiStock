package com.weifly.weistock.trade.enums;

/**
 * 委托种类
 *
 * @author weifly
 * @since 2018/11/19
 */
public enum SendCategory {
    category_0("0", "买入"),
    category_1("1", "卖出"), // 也是担保品卖出
    category_2("2", "融资买入"),
    category_3("3", "融券卖出"),
    category_4("4", "买券还券"),
    category_5("5", "卖券还款"),
    category_6("6", "现券还券"),
    category_7("7", "担保品买入"), // 实际是：担保品买入
    category_8("8", "担保品卖出"); // 实际是：担保品买入

    private String value;
    private String name;

    SendCategory(String value, String name){
        this.value = value;
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
