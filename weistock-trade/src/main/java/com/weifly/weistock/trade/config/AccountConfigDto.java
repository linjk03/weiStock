package com.weifly.weistock.trade.config;

/**
 * 股票账号信息
 *
 * @author weifly
 * @since 2019/8/7
 */
public class AccountConfigDto {

    private Integer qsid; // 券商id
    private String serverIp; // 交易服务器ip
    private Integer serverPort; // 交易服务器端口
    private String version; // 版本
    private Integer yybID; // 营业部id
    private Integer accountType; // 账户类型
    private String accountNo; // 客户账号
    private String tradeAccount; // 交易账号
    private String jyPassword; // 交易密码
    private String txPassword; // 通信密码

    public Integer getQsid() {
        return qsid;
    }

    public void setQsid(Integer qsid) {
        this.qsid = qsid;
    }

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public Integer getServerPort() {
        return serverPort;
    }

    public void setServerPort(Integer serverPort) {
        this.serverPort = serverPort;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getYybID() {
        return yybID;
    }

    public void setYybID(Integer yybID) {
        this.yybID = yybID;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getTradeAccount() {
        return tradeAccount;
    }

    public void setTradeAccount(String tradeAccount) {
        this.tradeAccount = tradeAccount;
    }

    public String getJyPassword() {
        return jyPassword;
    }

    public void setJyPassword(String jyPassword) {
        this.jyPassword = jyPassword;
    }

    public String getTxPassword() {
        return txPassword;
    }

    public void setTxPassword(String txPassword) {
        this.txPassword = txPassword;
    }
}
