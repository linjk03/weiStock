package com.weifly.weistock.trade.task;

import com.weifly.weistock.core.common.Result;
import com.weifly.weistock.core.market.StockPriceDto;
import com.weifly.weistock.core.market.StockMarketService;
import com.weifly.weistock.trade.monitor.StockMonitorService;
import com.weifly.weistock.trade.trade.SendOrderInfo;
import com.weifly.weistock.trade.trade.StockOrderInfo;
import com.weifly.weistock.trade.trade.StockTradeService;

import java.util.List;

/**
 * 股票更新上下文实现
 *
 * @author weifly
 * @since 2018/10/31
 */
public class StockUpdateContextImpl implements StockUpdateContext {

    private StockMarketService stockMarketService;
    private StockTradeService stockTradeService;
    private StockMonitorService stockMonitorService;

    private List<StockOrderInfo> orderList; // 已加载的委托单列表
    private Long stopSendOrderTime; // 停止下单时间

    public void setStockUpdateService(StockMarketService stockMarketService) {
        this.stockMarketService = stockMarketService;
    }

    public void setStockTradeService(StockTradeService stockTradeService) {
        this.stockTradeService = stockTradeService;
    }

    public void setStockMonitorService(StockMonitorService stockMonitorService) {
        this.stockMonitorService = stockMonitorService;
    }

    public StockPriceDto getStockPrice(String stockCode) {
        return this.stockMarketService.getStockPrice(stockCode);
    }

    public List<StockOrderInfo> getOrderList() {
        if(this.orderList==null){
            this.orderList = this.stockTradeService.getOrderList();
        }
        return this.orderList;
    }

    public Result sendOrder(SendOrderInfo sendOrderInfo) {
        return this.stockTradeService.sendOrder(sendOrderInfo);
    }

    public Result cancelOrder(StockOrderInfo stockOrderInfo) {
        return this.stockTradeService.cancelOrder(stockOrderInfo);
    }

    @Override
    public StockMonitorService getStockMonitorService() {
        return this.stockMonitorService;
    }

    @Override
    public Long getStopSendOrderTime() {
        return this.stopSendOrderTime;
    }

    public void setStopSendOrderTime(Long stopSendOrderTime) {
        this.stopSendOrderTime = stopSendOrderTime;
    }
}
