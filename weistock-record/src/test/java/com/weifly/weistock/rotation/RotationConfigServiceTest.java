package com.weifly.weistock.rotation;

import com.weifly.weistock.rotation.bo.RotationConfigBO;
import com.weifly.weistock.core.util.WeistockUtils;
import com.weifly.weistock.rotation.impl.RotationConfigServiceImpl;
import org.junit.Test;

public class RotationConfigServiceTest {

    @Test
    public void testSave(){
        RotationConfigServiceImpl configService = new RotationConfigServiceImpl();
        configService.setConfigPath("D:\\weistock\\rotation");

        RotationConfigBO configBO = new RotationConfigBO();
        configBO.setOpen(true);
        configBO.setBaseStockCode("510300");
        configBO.setBaseStockName("300ETF");
        configBO.setBaseStockPrice(4.96);
        configBO.setCompareStockCode("163417");
        configBO.setCompareStockName("兴全合宜");
        configBO.setCompareStockPrice(1.777);
        configService.saveRotationConfig(configBO);
    }

    @Test
    public void testLoad(){
        RotationConfigServiceImpl configService = new RotationConfigServiceImpl();
        configService.setConfigPath("D:\\weistock\\rotation");

        RotationConfigBO configBO = configService.loadRotationConfig();
        System.out.println(WeistockUtils.toJsonString(configBO));
    }
}
