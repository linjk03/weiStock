package com.weifly.weistock.optionvix;

import com.weifly.weistock.core.util.WeistockUtils;
import com.weifly.weistock.optionvix.bo.VixDayBO;
import com.weifly.weistock.optionvix.bo.VixYearBO;
import com.weifly.weistock.optionvix.impl.OptionVixConfigServiceImpl;
import org.junit.Test;

import java.util.List;

public class OptionVixConfigServiceTest {

    @Test
    public void testSave() {
        VixYearBO yearBO = new VixYearBO();
        yearBO.setYear("2021");
        VixDayBO dayBO = new VixDayBO();
        dayBO.setDay("20210120");
        dayBO.setOpenValue(1.1);
        dayBO.setHighValue(1.1);
        dayBO.setLowValue(1.1);
        dayBO.setCloseValue(1.1);
        yearBO.getDayList().add(dayBO);

        OptionVixConfigServiceImpl configService = new OptionVixConfigServiceImpl();
        configService.setConfigPath("D:\\weistock\\optionvix");
        configService.saveYear(yearBO);
    }

    @Test
    public void testLoad(){
        OptionVixConfigServiceImpl configService = new OptionVixConfigServiceImpl();
        configService.setConfigPath("D:\\weistock\\optionvix");

        List<VixYearBO> yearList = configService.loadYearList();
        System.out.println(WeistockUtils.toJsonString(yearList));
    }
}
