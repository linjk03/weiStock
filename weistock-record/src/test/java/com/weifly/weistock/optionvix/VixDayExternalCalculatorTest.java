package com.weifly.weistock.optionvix;

import com.weifly.weistock.core.util.WeistockUtils;
import com.weifly.weistock.optionvix.bo.VixDayExternalBO;
import com.weifly.weistock.optionvix.bo.VixYearBO;
import com.weifly.weistock.optionvix.impl.OptionVixConfigServiceImpl;
import com.weifly.weistock.optionvix.impl.VixDayExternalCalculator;
import org.junit.Test;

import java.util.List;
import java.util.Map;

public class VixDayExternalCalculatorTest {

    @Test
    public void testCalc() {
        OptionVixConfigServiceImpl configService = new OptionVixConfigServiceImpl();
        configService.setConfigPath("D:\\weistock\\optionvix");
        List<VixYearBO> yearList = configService.loadYearList();

        VixDayExternalCalculator calculator = new VixDayExternalCalculator(yearList);
        Map<String, VixDayExternalBO> dayExternalMap = calculator.calc();
        for (VixDayExternalBO dayExternal : dayExternalMap.values()) {
            System.out.println(WeistockUtils.toJsonString(dayExternal));
        }
    }
}
