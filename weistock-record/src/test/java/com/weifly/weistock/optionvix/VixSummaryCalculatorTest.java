package com.weifly.weistock.optionvix;

import com.weifly.weistock.core.util.WeistockUtils;
import com.weifly.weistock.optionvix.bo.VixYearBO;
import com.weifly.weistock.optionvix.dto.VixSummaryDTO;
import com.weifly.weistock.optionvix.impl.OptionVixConfigServiceImpl;
import com.weifly.weistock.optionvix.impl.VixSummaryCalculator;
import org.junit.Test;

import java.util.List;

public class VixSummaryCalculatorTest {

    @Test
    public void testCalc() {
        OptionVixConfigServiceImpl configService = new OptionVixConfigServiceImpl();
        configService.setConfigPath("D:\\weistock\\optionvix");
        List<VixYearBO> yearList = configService.loadYearList();

        VixSummaryCalculator calculator = new VixSummaryCalculator(yearList);
        VixSummaryDTO summaryDTO = calculator.calc();
        WeistockUtils.toJsonString(summaryDTO);
    }
}
