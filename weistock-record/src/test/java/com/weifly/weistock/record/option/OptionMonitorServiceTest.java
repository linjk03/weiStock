package com.weifly.weistock.record.option;

import com.weifly.weistock.record.bo.MergeRecordResult;
import com.weifly.weistock.record.option.domain.OptionRecordDto;
import com.weifly.weistock.record.option.impl.OptionMonitorServiceImpl;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 测试
 *
 * @author weifly
 * @since 2019/11/18
 */
public class OptionMonitorServiceTest {

    @Test
    public void testUpdateOption(){
        OptionStoreService optionStoreService = OptionTestUtils.createOptionStoreService();
        OptionMonitorServiceImpl optionMonitorService = new OptionMonitorServiceImpl();
        optionMonitorService.setOptionStoreService(optionStoreService);

        List<OptionRecordDto> recordList = OptionTestUtils.createRecordList();
        MergeRecordResult result = optionMonitorService.mergeRecordList(recordList);
        System.out.println(result.desc());
    }

    @Test
    public void testParseFile() throws IOException {
        OptionMonitorServiceImpl optionMonitorService = new OptionMonitorServiceImpl();
        optionMonitorService.setOptionStoreService(OptionTestUtils.createOptionStoreService());
        OptionParseService optionParseService = OptionTestUtils.createOptionParseService();

        File recordFile = new File("d:/weistock/test.txt");
        List<OptionRecordDto> recordList = optionParseService.parseRecord(recordFile);
        MergeRecordResult result = optionMonitorService.mergeRecordList(recordList);
        System.out.println(result.desc());
    }

    @Test
    public void testCalcOneContract(){
        OptionMonitorServiceImpl optionMonitorService = new OptionMonitorServiceImpl();
        List<OptionRecordDto> recordList = new ArrayList<>();

        // 卖出开仓
        OptionRecordDto sellRecord = new OptionRecordDto();
        sellRecord.setOperationType(OptionConst.OperationType.TYPE_5);
        sellRecord.setTradeNumber(1);
        sellRecord.setClearAmount(381.00);
        recordList.add(sellRecord);

        // 买入平仓
        OptionRecordDto buyRecord = new OptionRecordDto();
        buyRecord.setOperationType(OptionConst.OperationType.TYPE_4);
        buyRecord.setTradeNumber(1);
        buyRecord.setClearAmount(-102.30);
        recordList.add(buyRecord);

        optionMonitorService.calcOneContract(recordList);
        System.out.println(recordList.size());
    }
}
