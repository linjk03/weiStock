package com.weifly.weistock.record.bill;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ZxzqBillParseServiceTest extends AbstractBillParseServiceTest {

    public ZxzqBillParseServiceTest() {
        super("发生日期        成交时间        业务名称                证券代码        证券名称          成交价格            成交数量        成交金额         股份余额        手续费        印花税        过户费        附加费        交易所清算费        发生金额          资金本次余额        委托编号        流水号                   股东代码          资金帐号            币种          备注");
    }

    @Test
    public void testParseSell() {
        List<String> lineList = new ArrayList<>();
        lineList.add("20210423        09:47:57        证券卖出                160133          南方天元          4.58300000          -500.00         2291.50          27068.00        0.46          0.00          0.00          0.00          0.00                2291.04           38136.21            3920            86803106626013030        0219484563        880002292016        人民币        证券卖出                                                                                                                                                                                    ");
        this.testParse(lineList);
    }

    @Test
    public void testParseBuy() {
        List<String> lineList = new ArrayList<>();
        lineList.add("20210426        14:30:24        证券买入                160133          南方天元          4.58300000          500.00          2291.50          26068.00        0.46          0.00          0.00          0.00          0.00                -2291.96          34893.42            5705            86803119512333527        0219484563        880002292016        人民币        证券买入                                                                                                                                                                                    ");
        this.testParse(lineList);
    }
}
