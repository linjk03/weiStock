package com.weifly.weistock.record.bill;

import com.weifly.weistock.record.bill.domain.StockRecordDto;
import com.weifly.weistock.record.bill.parse.AbstractBillAnalyzer;
import com.weifly.weistock.record.bill.parse.AutoJudgeBillParseService;
import com.weifly.weistock.record.bill.parse.HbzqBillAnalyzer;
import com.weifly.weistock.record.bill.parse.ZxzqBillAnalyzer;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class AbstractBillParseServiceTest {

    private String headLine;

    protected AbstractBillParseServiceTest(String headLine) {
        this.headLine = headLine;
    }

    protected List<StockRecordDto> testParse(List<String> lineList) {
        try {
            AutoJudgeBillParseService parseService = this.createParseService();
            List<StockRecordDto> recordList = parseService.parseRecord(this.createInputStream(lineList));
            this.printRecordList(recordList);
            return recordList;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private AutoJudgeBillParseService createParseService() {
        AutoJudgeBillParseService parseService = new AutoJudgeBillParseService();
        List<AbstractBillAnalyzer> analyzerList = new ArrayList<>();
        analyzerList.add(new ZxzqBillAnalyzer());
        analyzerList.add(new HbzqBillAnalyzer());
        parseService.setAnalyzerList(analyzerList);
        return parseService;
    }

    private InputStream createInputStream(List<String> lineList) throws IOException {
        lineList.add(0, this.headLine);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try (PrintStream printStream = new PrintStream(outputStream, true, "GBK")) {
            for (String line : lineList) {
                printStream.println(line);
            }
        }
        return new ByteArrayInputStream(outputStream.toByteArray());
    }

    private void printRecordList(List<StockRecordDto> recordList) {
        if (recordList == null || recordList.isEmpty()) {
            System.out.println("recordList empty");
        } else {
            System.out.println("record size : " + recordList.size());
            for (StockRecordDto record : recordList) {
                System.out.println(record.desc());
            }
        }
    }
}
