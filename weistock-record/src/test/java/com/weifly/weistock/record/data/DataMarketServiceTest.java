package com.weifly.weistock.record.data;

import com.weifly.weistock.record.data.domain.StockDataDto;
import com.weifly.weistock.record.data.domain.StockDayDto;
import com.weifly.weistock.record.data.impl.SinaDataMarketServiceImpl;
import org.junit.Test;

/**
 * 测试
 *
 * @author weifly
 * @since 2019/9/29
 */
public class DataMarketServiceTest {

    @Test
    public void testLoadStockData(){
        SinaDataMarketServiceImpl dataMarketService = new SinaDataMarketServiceImpl();
        StockDataDto dataDto = dataMarketService.loadStockData("510050");
        if(dataDto!=null){
            for(StockDayDto dayDto : dataDto.getDayList()){
                System.out.println(dayDto.getDay() + " : " + dayDto.getOpen() + " : " + dayDto.getHigh() + " : " + dayDto.getLow() + " : " + dayDto.getClose());
            }
        }
    }
}
