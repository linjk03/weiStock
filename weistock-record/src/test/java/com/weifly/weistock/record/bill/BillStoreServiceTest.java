package com.weifly.weistock.record.bill;

import com.weifly.weistock.core.util.WeistockUtils;
import com.weifly.weistock.record.bill.bo.StockHoldDayBO;
import com.weifly.weistock.record.bill.bo.StockHoldSummaryBO;
import com.weifly.weistock.record.bill.domain.StockRecordDto;
import com.weifly.weistock.record.bill.domain.StockSummaryDto;
import com.weifly.weistock.record.bill.impl.BillMonitorServiceImpl;
import com.weifly.weistock.record.bill.impl.BillParseServiceImpl;
import com.weifly.weistock.record.bill.impl.BillStoreServiceImpl;
import com.weifly.weistock.record.bo.MergeRecordResult;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * 测试
 *
 * @author weifly
 * @since 2020/01/16
 */
public class BillStoreServiceTest {

    @Test
    public void testSaveStockSummary() throws IOException {
        BillStoreServiceImpl billStoreService = new BillStoreServiceImpl();
        billStoreService.setConfigPath("d:/weistock/bill");

        BillMonitorServiceImpl billMonitorService = new BillMonitorServiceImpl();
        billMonitorService.setBillStoreService(billStoreService);

        // 解析对账单并保存
        File recordFile = new File("d:/weistock/test.txt");
        BillParseService billParseService = new BillParseServiceImpl();
        List<StockRecordDto> recordList = billParseService.parseRecord(recordFile);
        MergeRecordResult result = billMonitorService.mergeRecordList(recordList);
        System.out.println(result.desc());
    }

    @Test
    public void testCalcStockHoldSummary(){
        BillStoreServiceImpl billStoreService = new BillStoreServiceImpl();
        billStoreService.setConfigPath("d:/weistock/bill");

        StockSummaryDto stockSummaryDto = billStoreService.loadStockSummary("160133");
        StockHoldSummaryBO holdSummaryBO = BillUtils.convertToStockHoldSummaryBO(stockSummaryDto);
        for(StockHoldDayBO dayBO : holdSummaryBO.getDayList()){
            System.out.println(WeistockUtils.toJsonString(dayBO));
        }
    }
}
