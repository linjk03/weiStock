package com.weifly.weistock.record.bill;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class HbzqBillParseServiceTest extends AbstractBillParseServiceTest {

    public HbzqBillParseServiceTest() {
        super("成交日期        成交时间        股东代码          证券代码        证券名称        委托类别        成交价格        成交数量        发生金额         剩余金额         佣金          印花税        过户费        成交费        成交编号                委托编号        ");
    }

    @Test
    public void testParseSell() {
        List<String> lineList = new ArrayList<>();
        lineList.add("20210308        13:27:45        0124706221        300271          华宇软件        卖出            20.320          100             2032.000         0.100            0.370         2.030         0.000         0.000         0101000025481678        318476          ");
        this.testParse(lineList);
    }
}
