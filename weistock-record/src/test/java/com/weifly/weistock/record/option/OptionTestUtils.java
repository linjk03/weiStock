package com.weifly.weistock.record.option;

import com.weifly.weistock.record.option.domain.OptionRecordDto;
import com.weifly.weistock.record.option.impl.OptionStoreServiceImpl;
import com.weifly.weistock.record.option.impl.OriginalFileParseService;

import java.util.ArrayList;
import java.util.List;

/**
 * 单元测试相关方法
 *
 * @author weifly
 * @since 2019/11/18
 */
public class OptionTestUtils {

    public static OptionStoreService createOptionStoreService() {
        OptionStoreServiceImpl optionStoreService = new OptionStoreServiceImpl();
        optionStoreService.setConfigPath("d:/weistock/option");
        return optionStoreService;
    }

    public static OptionParseService createOptionParseService(){
        OptionParseService optionParseService = new OriginalFileParseService();
        return optionParseService;
    }

    public static List<OptionRecordDto> createRecordList(){
        List<OptionRecordDto> recordList = new ArrayList<>();

        OptionRecordDto dto1 = new OptionRecordDto();
        dto1.setDate("20180803");
        dto1.setContractName("50ETF购9月2600");
        dto1.setContractCode("10001241");
        dto1.setStockCode("510050");
        dto1.setStockName("50ETF");
        dto1.setBusinessName(OptionConst.BusinessName.NAME_5);
        dto1.setOperationType(OptionConst.OperationType.TYPE_5);
        dto1.setHoldPosition(OptionConst.HoldPosition.HOLD_POSITION_3);
        dto1.setTradePrice(0.044900);
        dto1.setTradeNumber(1);
        dto1.setTradeAmount(449.00);
        dto1.setFeeService(0.00);
        dto1.setFeeStamp(0.00);
        dto1.setFeeTransfer(0.00);
        dto1.setFeeExtra(0.00);
        dto1.setFeeClear(0.00);
        dto1.setClearAmount(449.00);
        dto1.setAfterAmount(10449.00);
        dto1.setAfterNumber(1);
        dto1.setEntrustCode("3");
        dto1.setTime("10:38:12");
        recordList.add(dto1);

        return recordList;
    }
}
