package com.weifly.weistock.record.bill;

import com.weifly.weistock.record.bill.domain.StockRecordDto;
import com.weifly.weistock.record.bill.impl.BillParseServiceImpl;
import com.weifly.weistock.record.bill.impl.CompoundBillParseServiceImpl;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * 测试
 *
 * @author weifly
 * @since 2020/01/15
 */
public class BillMonitorServiceTest {

    @Test
    public void testParseFile() throws IOException {
        BillParseServiceImpl billParseService = new BillParseServiceImpl();
        File recordFile = new File("d:/weistock/test.txt");
        List<StockRecordDto> recordList = billParseService.parseRecord(recordFile);
        if (recordList == null || recordList.isEmpty()) {
            System.out.println("recordList empty");
        } else {
            System.out.println("record size : " + recordList.size());
            for (StockRecordDto record : recordList) {
                System.out.println(record.desc());
            }
        }
    }

    @Test
    public void testParseFile1() throws IOException {
        CompoundBillParseServiceImpl billParseService = new CompoundBillParseServiceImpl();
        File recordFile = new File("d:/weistock/test1.txt");
        List<StockRecordDto> recordList = billParseService.parseRecord(recordFile);
        if (recordList == null || recordList.isEmpty()) {
            System.out.println("recordList empty");
        } else {
            System.out.println("record size : " + recordList.size());
            for (StockRecordDto record : recordList) {
                System.out.println(record.desc());
            }
        }
    }
}
