package com.weifly.weistock.record.option.domain;

import com.weifly.weistock.core.util.visitor.DataListContainer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 某年的期权交易记录
 *
 * @author weifly
 * @since 2019/11/17
 */
public class OptionYearDto implements DataListContainer<OptionRecordDto> {

    private String year; // 年，4位数字 2019
    private List<OptionRecordDto> recordList = new ArrayList<>(); // 交易记录列表
    private Map<String, OptionRecordDto> recordMap = new HashMap<>(); // 交易记录集合

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public List<OptionRecordDto> getRecordList() {
        return recordList;
    }

    public void setRecordList(List<OptionRecordDto> recordList) {
        this.recordList = recordList;
    }

    public Map<String, OptionRecordDto> getRecordMap() {
        return recordMap;
    }

    public void setRecordMap(Map<String, OptionRecordDto> recordMap) {
        this.recordMap = recordMap;
    }

    @Override
    public List<OptionRecordDto> getDataList() {
        return recordList;
    }
}
