package com.weifly.weistock.record.bill.bo;

/**
 * 股票持仓，某日买入情况
 *
 * @author weifly
 * @since 2020/12/24
 */
public class StockHoldDayBO {

    private String date; // 购买日期, 20191224
    private int holdNumber; // 持仓数量, 200
    private int holdDays; // 持仓天数，60
    private int totalNumber; // 截止当天的总持仓量

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getHoldNumber() {
        return holdNumber;
    }

    public void setHoldNumber(int holdNumber) {
        this.holdNumber = holdNumber;
    }

    public int getHoldDays() {
        return holdDays;
    }

    public void setHoldDays(int holdDays) {
        this.holdDays = holdDays;
    }

    public int getTotalNumber() {
        return totalNumber;
    }

    public void setTotalNumber(int totalNumber) {
        this.totalNumber = totalNumber;
    }
}
