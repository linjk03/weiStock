package com.weifly.weistock.record.bill.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 股票交易统计信息
 *
 * @author weifly
 * @since 2020/01/15
 */
public class StockSummaryDto {

    private String stockCode; // 证券名称, 510300
    private String stockName; // 证券代码, 300ETF
    private Double diffAmount; // 差额
    private Double feeService; // 手续费
    private Double feeStamp; // 印花税
    private Integer afterNumber; // 剩余数量
    private List<StockRecordDto> recordList = new ArrayList<>(); // 股票交易记录列表
    private Map<String, StockRecordDto> recordMap = new HashMap<>(); // 股票交易记录集合

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public Double getDiffAmount() {
        return diffAmount;
    }

    public void setDiffAmount(Double diffAmount) {
        this.diffAmount = diffAmount;
    }

    public Double getFeeService() {
        return feeService;
    }

    public void setFeeService(Double feeService) {
        this.feeService = feeService;
    }

    public Double getFeeStamp() {
        return feeStamp;
    }

    public void setFeeStamp(Double feeStamp) {
        this.feeStamp = feeStamp;
    }

    public Integer getAfterNumber() {
        return afterNumber;
    }

    public void setAfterNumber(Integer afterNumber) {
        this.afterNumber = afterNumber;
    }

    public List<StockRecordDto> getRecordList() {
        return recordList;
    }

    public void setRecordList(List<StockRecordDto> recordList) {
        this.recordList = recordList;
    }

    public Map<String, StockRecordDto> getRecordMap() {
        return recordMap;
    }

    public void setRecordMap(Map<String, StockRecordDto> recordMap) {
        this.recordMap = recordMap;
    }
}
