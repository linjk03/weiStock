package com.weifly.weistock.record.bill;

import com.weifly.weistock.core.util.DateUtils;
import com.weifly.weistock.record.bill.bo.StockHoldDayBO;
import com.weifly.weistock.record.bill.bo.StockHoldSummaryBO;
import com.weifly.weistock.record.bo.MergeRecordResult;
import com.weifly.weistock.record.bo.RecordPair;
import com.weifly.weistock.record.bill.domain.StockRecordDto;
import com.weifly.weistock.record.bill.domain.StockSummaryDto;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 账单工具方法
 *
 * @author weifly
 * @since 2020/01/15
 */
public class BillUtils {

    /**
     * 是卖出操作，则返回true
     */
    public static boolean isCloseRecord(StockRecordDto record){
        BillConst.BusinessName businessName = record.getBusinessName();
        // 证券卖出
        if(BillConst.BusinessName.NAME_2.equals(businessName)){
            return true;
        }
        // 担保品划出
        if(BillConst.BusinessName.NAME_4.equals(businessName)){
            return true;
        }
        // 还款卖出
        if(BillConst.BusinessName.NAME_6.equals(businessName)){
            return true;
        }
        return false;
    }

    public static boolean isOpenRecord(StockRecordDto record) {
        BillConst.BusinessName businessName = record.getBusinessName();
        // 证券买入
        if (BillConst.BusinessName.NAME_1.equals(businessName)) {
            return true;
        }
        // 担保品划入
        if (BillConst.BusinessName.NAME_3.equals(businessName)) {
            return true;
        }
        // 融资买入
        if (BillConst.BusinessName.NAME_5.equals(businessName)) {
            return true;
        }
        // 红股入帐
        if (BillConst.BusinessName.NAME_8.equals(businessName)) {
            return true;
        }
        // 基金申购
        if (BillConst.BusinessName.NAME_10.equals(businessName)) {
            return true;
        }
        return false;
    }

    /**
     * 获得未配对成交量
     */
    public static int getNotPairNumber(StockRecordDto record){
        int totalPairNumber = 0;
        if(record.getPairList()!=null){
            for(RecordPair pair : record.getPairList()){
                totalPairNumber += pair.getNumber();
            }
        }
        return record.getTradeNumber() - totalPairNumber;
    }

    public static void addRecordToSummary(StockSummaryDto summaryDto, StockRecordDto recordDto, MergeRecordResult result){
        // key规则：发生日期_成交时间_委托编号
        String key = recordDto.getDate() + "_" + recordDto.getTime() + "_" + recordDto.getEntrustCode();
        StockRecordDto targetDto = summaryDto.getRecordMap().get(key);
        if(targetDto==null){
            summaryDto.getRecordList().add(recordDto);
            summaryDto.getRecordMap().put(key, recordDto);
            if(result!=null){
                result.setInsert(result.getInsert()+1);
            }
        }else{
            targetDto.copyPropForm(recordDto);
            if(result!=null){
                result.setUpdate(result.getUpdate()+1);
            }
        }
    }

    public static StockHoldSummaryBO convertToStockHoldSummaryBO(StockSummaryDto stockSummaryDto) {
        if (stockSummaryDto == null) {
            return null;
        }
        // 计算dayList
        StockHoldSummaryBO holdSummaryBO = new StockHoldSummaryBO();
        holdSummaryBO.setStockCode(stockSummaryDto.getStockCode());
        holdSummaryBO.setStockName(stockSummaryDto.getStockName());
        List<StockHoldDayBO> dayList = holdSummaryBO.getDayList();
        Map<String, StockHoldDayBO> dayMap = new HashMap<>();
        for (StockRecordDto recordDto : stockSummaryDto.getRecordList()) {
            if (BillConst.BusinessName.NAME_1.equals(recordDto.getBusinessName())) {
                addHold(dayMap, dayList, recordDto);
            } else if (BillConst.BusinessName.NAME_2.equals(recordDto.getBusinessName())) {
                subtractHold(dayMap, dayList, recordDto);
            } else if (BillConst.BusinessName.NAME_7.equals(recordDto.getBusinessName())) {
                // 股息入帐，忽略
            } else {
                throw new RuntimeException("不支持的BusinessName: " + recordDto.getBusinessName());
            }
        }
        // 计算date, totalHoldNumber
        SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.FORMATTER_DATE);
        long currTime = System.currentTimeMillis();
        int totalHoldNumber = 0;
        for (StockHoldDayBO dayBO : dayList) {
            totalHoldNumber += dayBO.getHoldNumber();
            long holdDays = (currTime - DateUtils.parseDate(sdf, dayBO.getDate()).getTime()) / (1000 * 60 * 60 * 24);
            dayBO.setHoldDays((int) holdDays);
            dayBO.setTotalNumber(totalHoldNumber);
        }
        holdSummaryBO.setHoldNumber(totalHoldNumber);

        return holdSummaryBO;
    }

    private static void addHold(Map<String, StockHoldDayBO> dayMap, List<StockHoldDayBO> dayList, StockRecordDto recordDto) {
        String date = recordDto.getDate();
        if (dayMap.containsKey(date)) {
            StockHoldDayBO dayBO = dayMap.get(date);
            dayBO.setHoldNumber(dayBO.getHoldNumber() + recordDto.getTradeNumber());
        } else {
            StockHoldDayBO dayBO = new StockHoldDayBO();
            dayBO.setDate(date);
            dayBO.setHoldNumber(recordDto.getTradeNumber());
            dayMap.put(date, dayBO);
            dayList.add(dayBO);
        }
    }

    private static void subtractHold(Map<String, StockHoldDayBO> dayMap, List<StockHoldDayBO> dayList, StockRecordDto recordDto) {
        int subtractNumber = recordDto.getTradeNumber();
        while (subtractNumber > 0) {
            if (dayList.isEmpty()) {
                throw new RuntimeException("持仓数量不够，无法核销卖出数量: " + subtractNumber);
            }
            StockHoldDayBO dayBO = dayList.get(0);
            if (dayBO.getHoldNumber() <= subtractNumber) {
                dayMap.remove(dayBO.getDate());
                dayList.remove(0);
                subtractNumber = subtractNumber - dayBO.getHoldNumber();
            } else {
                dayBO.setHoldNumber(dayBO.getHoldNumber() - subtractNumber);
                subtractNumber = 0;
            }
        }
    }
}
