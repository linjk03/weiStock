package com.weifly.weistock.record.asset.impl;

import com.weifly.weistock.core.config.AbstractConfigService;
import com.weifly.weistock.core.config.ConfigConstants;
import com.weifly.weistock.record.asset.AssetConfigService;
import com.weifly.weistock.record.asset.domain.AssetDayDto;
import com.weifly.weistock.record.asset.domain.AssetYearDto;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户资产配置服务实现
 *
 * @author weifly
 * @since 2019/9/17
 */
public class AssetConfigServiceImpl extends AbstractConfigService implements AssetConfigService {

    private static final String ATTR_ASSET = "asset";

    @Override
    public List<AssetYearDto> loadYearList() {
        List<AssetYearDto> yearList = new ArrayList<>();
        for(File xmlFile : this.getConfigFolder().listFiles()){
            if(xmlFile.getName().endsWith(".xml")){
                log.info("加载资产记录：" + xmlFile.getAbsolutePath());
                AssetYearDto yearDto = this.loadYearXml(xmlFile);
                yearList.add(yearDto);
            }
        }
        return yearList;
    }

    private AssetYearDto loadYearXml(File xmlFile){
        AssetYearDto yearDto = new AssetYearDto();
        try{
            SAXReader reader = new SAXReader();
            Document document = reader.read(xmlFile);
            Element rootEle = document.getRootElement();

            String year = rootEle.attributeValue(ConfigConstants.ATTR_YEAR);
            yearDto.setYear(year);

            List<Element> recordEleList = rootEle.elements(ConfigConstants.ELE_RECORD);
            if(recordEleList!=null){
                for(Element recordEle : recordEleList){
                    AssetDayDto dayDto = new AssetDayDto();
                    String day = recordEle.attributeValue(ConfigConstants.ATTR_DAY);
                    dayDto.setDay(day);
                    String assetStr = recordEle.attributeValue(ATTR_ASSET);
                    double asset = Double.parseDouble(assetStr);
                    dayDto.setAsset(asset);
                    yearDto.getDayList().add(dayDto);
                }
            }
            return yearDto;
        }catch(DocumentException e){
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void saveYear(AssetYearDto yearDto) {
        File configFolder = this.getConfigFolder();
        DocumentFactory factory = DocumentFactory.getInstance();
        Document document = factory.createDocument();
        document.setXMLEncoding("UTF8");

        Element rootEle = factory.createElement(ConfigConstants.ELE_ROOT);
        rootEle.addAttribute(ConfigConstants.ATTR_YEAR, yearDto.getYear());
        document.setRootElement(rootEle);

        for(AssetDayDto dayDto : yearDto.getDayList()){
            Element recordEle = factory.createElement(ConfigConstants.ELE_RECORD);
            recordEle.addAttribute(ConfigConstants.ATTR_DAY, dayDto.getDay());
            recordEle.addAttribute(ATTR_ASSET, String.valueOf(dayDto.getAsset()));
            rootEle.add(recordEle);
        }

        File xmlFile = new File(configFolder, "asset_" + yearDto.getYear() + ".xml");
        log.info("保存资产记录, file={}", xmlFile.getAbsolutePath());
        this.saveConfig(xmlFile, document);
    }
}
