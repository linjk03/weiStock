package com.weifly.weistock.record.asset;

import com.weifly.weistock.record.asset.domain.AssetYearDto;

import java.util.List;

/**
 * 用户资产配置服务
 *
 * @author weifly
 * @since 2019/9/17
 */
public interface AssetConfigService {

    /**
     * 设置配置文件路径
     */
    void setConfigPath(String configPath);

    /**
     * 加载年份列表
     */
    List<AssetYearDto> loadYearList();

    /**
     * 保存年资金记录
     */
    void saveYear(AssetYearDto yearDto);
}
