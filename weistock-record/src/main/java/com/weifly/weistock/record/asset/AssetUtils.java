package com.weifly.weistock.record.asset;

import com.weifly.weistock.core.chart.bo.ChartDataSetBO;
import com.weifly.weistock.core.chart.bo.ChartSerieBO;
import com.weifly.weistock.core.util.WeistockUtils;
import com.weifly.weistock.record.asset.domain.AssetDayDto;
import com.weifly.weistock.record.asset.domain.AssetYearDto;

import java.text.DecimalFormat;
import java.util.Comparator;
import java.util.List;

/**
 * 用户资产工具类
 *
 * @author weifly
 * @since 2019/9/24
 */
public class AssetUtils {

    public static void updateDayOfYear(AssetYearDto yearDto, AssetDayDto dayDto) {
        AssetDayDto targetDay = null;
        for (AssetDayDto dayInYear : yearDto.getDayList()) {
            if (dayInYear.getDay().equals(dayDto.getDay())) {
                // 同一天
                targetDay = dayInYear;
                break;
            }
        }

        if (targetDay == null) {
            yearDto.getDayList().add(dayDto);
            // 记录排序
            yearDto.getDayList().sort(Comparator.comparing(AssetDayDto::getDay));
        } else {
            targetDay.copyPropForm(dayDto);
        }
    }

    /**
     * 装入资产DataSet
     *
     * @param dataSet
     * @param dayList
     */
    public static void fillAssetDataSet(ChartDataSetBO dataSet, List<AssetDayDto> dayList) {
        if (dayList.isEmpty()) {
            return;
        }

        ChartSerieBO chartSerie = new ChartSerieBO();
        chartSerie.setName("资产");
        chartSerie.setType("line");
        double baseAsset = dayList.get(0).getAsset();
        DecimalFormat decimalFormat = new DecimalFormat("0.####");
        for (AssetDayDto dayDto : dayList) {
            dataSet.getPointList().add(dayDto.getDay());
            double rate = WeistockUtils.divide(dayDto.getAsset(), baseAsset);
            chartSerie.getDataList().add(Double.valueOf(decimalFormat.format(rate)));
        }
        dataSet.getSerieList().add(chartSerie);
    }
}
