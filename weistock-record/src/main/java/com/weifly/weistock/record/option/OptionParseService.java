package com.weifly.weistock.record.option;

import com.weifly.weistock.record.option.domain.OptionRecordDto;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 解析期权对账单
 *
 * @author weifly
 * @since 2019/11/18
 */
public interface OptionParseService {

    /**
     * 解析交易记录
     */
    List<OptionRecordDto> parseRecord(File recordFile) throws IOException;

    /**
     * 解析交易记录
     */
    List<OptionRecordDto> parseRecord(InputStream input) throws IOException;
}
