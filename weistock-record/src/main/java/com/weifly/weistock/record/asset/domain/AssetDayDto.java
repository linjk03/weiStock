package com.weifly.weistock.record.asset.domain;

/**
 * 某天的资产信息
 *
 * @author weifly
 * @since 2019/9/17
 */
public class AssetDayDto {

    private String day; // 日期
    private double asset; // 总资产

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public double getAsset() {
        return asset;
    }

    public void setAsset(double asset) {
        this.asset = asset;
    }

    /**
     * 拷贝属性
     */
    public void copyPropForm(AssetDayDto source) {
        this.day = source.day;
        this.asset = source.asset;
    }
}
