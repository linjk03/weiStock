package com.weifly.weistock.record.data.domain;

/**
 * 某天的股票日数据
 *
 * @author weifly
 * @since 2019/9/29
 */
public class StockDayDto {

    private String day; // 日期
    private double open; // 开盘价
    private double high; // 最高价
    private double low; // 最低价
    private double close; // 收盘价

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public double getOpen() {
        return open;
    }

    public void setOpen(double open) {
        this.open = open;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }

    public double getClose() {
        return close;
    }

    public void setClose(double close) {
        this.close = close;
    }
}
