package com.weifly.weistock.record.bill.domain;

import com.weifly.weistock.record.bo.AbstractRecordBO;
import com.weifly.weistock.record.bill.BillConst;

/**
 * 股票交易记录
 *
 * @author weifly
 * @since 2019/11/17
 */
public class StockRecordDto extends AbstractRecordBO {
    // 共17个属性, 父类3个属性
    private BillConst.BusinessName businessName; // 必有: 业务名称 证券买入
    private String stockCode; // 必有: 证券名称, 510300
    private String stockName; // 必有: 证券代码, 300ETF
    private Double tradePrice; // 必有: 成交价格, 3.9710
    private Integer tradeNumber; // 必有: 成交数量, 200.00
    private Double tradeAmount; // 成交金额, 794.20
    private Integer afterNumber; // 股份余额, 2200.00
    private Double feeService; // 必有: 手续费
    private Double feeStamp; // 必有: 印花税
    private Double feeTransfer; // 必有: 过户费
    private Double feeExtra; // 附加费
    private Double feeClear; // 必有: 交易所清算费
    private Double clearAmount; // 必有: 发生金额
    private Double afterAmount; // 必有: 资金本次余额

    public BillConst.BusinessName getBusinessName() {
        return businessName;
    }

    public void setBusinessName(BillConst.BusinessName businessName) {
        this.businessName = businessName;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public Double getTradePrice() {
        return tradePrice;
    }

    public void setTradePrice(Double tradePrice) {
        this.tradePrice = tradePrice;
    }

    public Integer getTradeNumber() {
        return tradeNumber;
    }

    public void setTradeNumber(Integer tradeNumber) {
        this.tradeNumber = tradeNumber;
    }

    public Double getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(Double tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public Integer getAfterNumber() {
        return afterNumber;
    }

    public void setAfterNumber(Integer afterNumber) {
        this.afterNumber = afterNumber;
    }

    public Double getFeeService() {
        return feeService;
    }

    public void setFeeService(Double feeService) {
        this.feeService = feeService;
    }

    public Double getFeeStamp() {
        return feeStamp;
    }

    public void setFeeStamp(Double feeStamp) {
        this.feeStamp = feeStamp;
    }

    public Double getFeeTransfer() {
        return feeTransfer;
    }

    public void setFeeTransfer(Double feeTransfer) {
        this.feeTransfer = feeTransfer;
    }

    public Double getFeeExtra() {
        return feeExtra;
    }

    public void setFeeExtra(Double feeExtra) {
        this.feeExtra = feeExtra;
    }

    public Double getFeeClear() {
        return feeClear;
    }

    public void setFeeClear(Double feeClear) {
        this.feeClear = feeClear;
    }

    public Double getClearAmount() {
        return clearAmount;
    }

    public void setClearAmount(Double clearAmount) {
        this.clearAmount = clearAmount;
    }

    public Double getAfterAmount() {
        return afterAmount;
    }

    public void setAfterAmount(Double afterAmount) {
        this.afterAmount = afterAmount;
    }

    /**
     * 描述信息
     */
    public String desc() {
        StringBuilder sb = new StringBuilder();
        sb.append("date=").append(this.getDate());
        sb.append(", time=").append(this.getTime());
        sb.append(", businessName=").append(this.businessName.getName());
        sb.append(", stockCode=").append(this.stockCode);
        sb.append(", stockName=").append(this.stockName);
        sb.append(", tradePrice=").append(this.tradePrice);
        sb.append(", tradeNumber=").append(this.tradeNumber);
        sb.append(", tradeAmount=").append(this.tradeAmount);
        sb.append(", afterNumber=").append(this.afterNumber);
        sb.append(", feeService=").append(this.feeService);
        sb.append(", feeStamp=").append(this.feeStamp);
        sb.append(", clearAmount=").append(this.clearAmount);
        sb.append(", afterAmount=").append(this.afterAmount);
        sb.append(", entrustCode=").append(this.getEntrustCode());
        return sb.toString();
    }

    /**
     * 拷贝属性
     */
    public void copyPropForm(StockRecordDto source) {
        // 共17个属性
        this.setDate(source.getDate());
        this.setTime(source.getTime());
        this.setBusinessName(source.getBusinessName());
        this.setStockCode(source.getStockCode());
        this.setStockName(source.getStockName());
        this.setTradePrice(source.getTradePrice());
        this.setTradeNumber(source.getTradeNumber());
        this.setTradeAmount(source.getTradeAmount());
        this.setAfterNumber(source.getAfterNumber());
        this.setFeeService(source.getFeeService());
        this.setFeeStamp(source.getFeeStamp());
        this.setFeeTransfer(source.getFeeTransfer());
        this.setFeeExtra(source.getFeeExtra());
        this.setFeeClear(source.getFeeClear());
        this.setClearAmount(source.getClearAmount());
        this.setAfterAmount(source.getAfterAmount());
        this.setEntrustCode(source.getEntrustCode());

        this.setPairList(source.getPairList());
        this.setDiffAmount(source.getDiffAmount());
    }
}
