package com.weifly.weistock.record;

import com.weifly.weistock.core.util.ModuleBuilder;
import com.weifly.weistock.record.asset.AssetConfigService;
import com.weifly.weistock.record.asset.AssetMonitorService;
import com.weifly.weistock.record.asset.domain.AssetYearDto;
import com.weifly.weistock.record.bill.BillMonitorService;
import com.weifly.weistock.record.bill.BillStoreService;
import com.weifly.weistock.record.bill.domain.StockSummaryDto;
import com.weifly.weistock.record.data.DataHistoryService;
import com.weifly.weistock.record.data.DataStoreService;
import com.weifly.weistock.record.data.domain.StockDataDto;
import com.weifly.weistock.record.option.OptionMonitorService;
import com.weifly.weistock.record.option.OptionStoreService;
import com.weifly.weistock.record.option.domain.OptionYearDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;

/**
 * 记录管理builder
 *
 * @author weifly
 * @since 2019/9/24
 */
public class RecordManageBuilder implements ModuleBuilder {

    private Logger log = LoggerFactory.getLogger(RecordManageBuilder.class);

    private AssetConfigService assetConfigService;
    private AssetMonitorService assetMonitorService;
    private DataStoreService dataStoreService;
    private DataHistoryService dataHistoryService;
    private OptionStoreService optionStoreService;
    private OptionMonitorService optionMonitorService;
    private BillStoreService billStoreService;
    private BillMonitorService billMonitorService;

    public void setAssetConfigService(AssetConfigService assetConfigService) {
        this.assetConfigService = assetConfigService;
    }

    public void setAssetMonitorService(AssetMonitorService assetMonitorService) {
        this.assetMonitorService = assetMonitorService;
    }

    public void setDataStoreService(DataStoreService dataStoreService) {
        this.dataStoreService = dataStoreService;
    }

    public void setDataHistoryService(DataHistoryService dataHistoryService) {
        this.dataHistoryService = dataHistoryService;
    }

    public void setOptionStoreService(OptionStoreService optionStoreService) {
        this.optionStoreService = optionStoreService;
    }

    public void setOptionMonitorService(OptionMonitorService optionMonitorService) {
        this.optionMonitorService = optionMonitorService;
    }

    public void setBillStoreService(BillStoreService billStoreService) {
        this.billStoreService = billStoreService;
    }

    public void setBillMonitorService(BillMonitorService billMonitorService) {
        this.billMonitorService = billMonitorService;
    }

    @Override
    public void build(String configPath) {
        String assetPath = new File(configPath, "asset").getAbsolutePath();
        log.info("构建记录管理模块: "+assetPath);
        this.assetConfigService.setConfigPath(assetPath);
        List<AssetYearDto> yearList = this.assetConfigService.loadYearList();
        this.assetMonitorService.updateYearList(yearList);

        String dataPath = new File(configPath, "data").getAbsolutePath();
        log.info("构建历史数据模块: " + dataPath);
        this.dataStoreService.setConfigPath(dataPath);
        List<StockDataDto> stockList = this.dataStoreService.loadStockDataList();
        this.dataHistoryService.updateStockList(stockList);

        String optionPath = new File(configPath, "option").getAbsolutePath();
        log.info("构建期权记录模块: " + optionPath);
        this.optionStoreService.setConfigPath(optionPath);
        List<OptionYearDto> optionYearList = this.optionStoreService.loadYearList();
        this.optionMonitorService.updateYearList(optionYearList);

        String billPath = new File(configPath, "bill").getAbsolutePath();
        log.info("构建股票记录模块: " + billPath);
        this.billStoreService.setConfigPath(billPath);
        List<StockSummaryDto> stockSummaryList = this.billStoreService.loadStockList();
        this.billMonitorService.updateStockList(stockSummaryList);
    }
}
