package com.weifly.weistock.record.data;

import com.weifly.weistock.core.chart.bo.ChartDataSetBO;
import com.weifly.weistock.core.chart.bo.ChartSerieBO;
import com.weifly.weistock.core.util.WeistockUtils;
import com.weifly.weistock.record.data.domain.StockDayDto;
import org.apache.commons.lang3.StringUtils;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * converter
 *
 * @author weifly
 * Create at 2021/1/20
 */
public class DataConverter {

    public static void fillStockDataSet(ChartDataSetBO dataSet, List<StockDayDto> stockDayList, String stockCode, String stockName) {
        if (stockDayList.isEmpty()) {
            return; // 无股票数据
        }
        if (dataSet.getPointList().isEmpty()) {
            return; // 无坐标数据
        }
        String basePoint = dataSet.getPointList().get(0);

        // 生成stockDayMap, 并寻找基准价格
        Map<String, StockDayDto> stockDayMap = new HashMap<>();
        StockDayDto baseDayDto = null;
        for (StockDayDto stockDayDto : stockDayList) {
            stockDayMap.put(stockDayDto.getDay(), stockDayDto);
            if (stockDayDto.getDay().compareTo(basePoint) >= 0) {
                if (baseDayDto == null) {
                    baseDayDto = stockDayDto; // 和basePoint最接近的数据
                }
            }
        }
        if (baseDayDto == null || baseDayDto.getClose() <= 0) {
            return; // 没有找到基准价
        }

        // 生成股票数据集
        double basePrice = baseDayDto.getClose();
        ChartSerieBO stockSerie = new ChartSerieBO();
        stockSerie.setName(StringUtils.isBlank(stockName) ? stockCode : stockName);
        stockSerie.setType("line");
        DecimalFormat decimalFormat = new DecimalFormat("0.####");
        for (Object point : dataSet.getPointList()) {
            StockDayDto dayDto = stockDayMap.get(point);
            double currPrice = 0;
            if (dayDto == null) {
                currPrice = basePrice;
            } else {
                currPrice = dayDto.getClose();
            }
            double rate = WeistockUtils.divide(currPrice, basePrice);
            stockSerie.getDataList().add(Double.valueOf(decimalFormat.format(rate)));
        }
        dataSet.getSerieList().add(stockSerie);
    }
}
