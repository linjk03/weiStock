package com.weifly.weistock.record.bo;

/**
 * 合并交易记录结果
 *
 * @author weifly
 * @since 2019/11/18
 */
public class MergeRecordResult {

    private int total = 0; // 总记录数
    private int insert = 0; // 插入记录数
    private int update = 0; // 更新记录数

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getInsert() {
        return insert;
    }

    public void setInsert(int insert) {
        this.insert = insert;
    }

    public int getUpdate() {
        return update;
    }

    public void setUpdate(int update) {
        this.update = update;
    }

    public String desc(){
        StringBuilder sb = new StringBuilder();
        sb.append("合并结果, total=").append(this.total);
        sb.append(", insert=").append(this.insert);
        sb.append(", update=").append(this.update);
        return sb.toString();
    }
}
