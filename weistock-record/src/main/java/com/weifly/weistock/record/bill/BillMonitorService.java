package com.weifly.weistock.record.bill;

import com.weifly.weistock.record.bill.bo.StockHoldSummaryBO;
import com.weifly.weistock.record.bill.domain.GetStockRecordRequest;
import com.weifly.weistock.record.bill.domain.StockRecordDto;
import com.weifly.weistock.record.bo.MergeRecordResult;
import com.weifly.weistock.record.bill.domain.StockSummaryDto;

import java.util.List;

/**
 * 股票交易记录管理服务
 *
 * @author weifly
 * @since 2020/01/15
 */
public interface BillMonitorService {

    /**
     * 合并交易记录
     */
    MergeRecordResult mergeRecordList(List<StockRecordDto> recordList);

    /**
     * 获得股票列表
     */
    List<StockSummaryDto> getStockList();

    /**
     * 获得股票信息
     */
    StockSummaryDto getStockSummary(String stockCode);

    /**
     * 获得股票净持仓统计信息
     */
    StockHoldSummaryBO getStockHold(String stockCode);

    /**
     * 更新股票列表
     */
    void updateStockList(List<StockSummaryDto> stockList);

    /**
     * 加载记录列表
     */
    List<StockRecordDto> getRecordList(GetStockRecordRequest queryRequest);
}
