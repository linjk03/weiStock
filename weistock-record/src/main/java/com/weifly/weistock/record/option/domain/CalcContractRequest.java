package com.weifly.weistock.record.option.domain;

import java.util.Set;

/**
 * 计算合约请求
 *
 * @author weifly
 * @since 2019/11/24
 */
public class CalcContractRequest {

    private boolean all; // 处理所有合约，则为true
    private Set<String> contractSet; // 需要处理的合约集合

    public boolean isAll() {
        return all;
    }

    public void setAll(boolean all) {
        this.all = all;
    }

    public Set<String> getContractSet() {
        return contractSet;
    }

    public void setContractSet(Set<String> contractSet) {
        this.contractSet = contractSet;
    }
}
