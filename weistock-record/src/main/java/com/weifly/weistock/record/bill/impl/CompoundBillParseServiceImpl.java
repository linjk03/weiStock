package com.weifly.weistock.record.bill.impl;

import com.weifly.weistock.core.common.StockException;
import com.weifly.weistock.record.bill.BillConst;
import com.weifly.weistock.record.bill.BillParseService;
import com.weifly.weistock.record.bill.BillUtils;
import com.weifly.weistock.record.bill.constant.BillColumnTypeEnum;
import com.weifly.weistock.record.bill.domain.StockRecordDto;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 对账单解析service
 *
 * @author weifly
 * @since 2021/2/3
 */
public class CompoundBillParseServiceImpl implements BillParseService {

    private Logger log = LoggerFactory.getLogger(CompoundBillParseServiceImpl.class);

    @Override
    public List<StockRecordDto> parseRecord(File recordFile) throws IOException {
        try (FileInputStream fileInput = new FileInputStream(recordFile)) {
            return this.parseRecord(fileInput);
        }
    }

    @Override
    public List<StockRecordDto> parseRecord(InputStream input) throws IOException {
        List<String> lineList = this.readLines(input);
        // 解析head
        List<BillHeadColumn> columnList = this.parseHeadInfo(lineList);
        // 解析line
        return this.parseLines(lineList, columnList);
    }

    private List<String> readLines(InputStream input) throws IOException {
        List<String> lineList = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(input, "GBK"));
        String line;
        while ((line = reader.readLine()) != null) {
            lineList.add(line);
        }
        return lineList;
    }

    private List<BillHeadColumn> parseHeadInfo(List<String> lineList) {
        String headLine = null;
        for (String line : lineList) {
            if (line.startsWith("成交日期") || line.startsWith("发生日期")) {
                headLine = line;
                break;
            }
        }
        if (headLine == null) {
            throw new StockException("找不到headLine");
        }

        // 解析head
        List<BillHeadColumn> columnList = new ArrayList<>();
        Matcher matcher = Pattern.compile("\\S+\\s*").matcher(headLine);
        int start = 0;
        int index = 0;
        while (matcher.find(start)) {
            this.makeOneColumn(columnList, matcher.group().trim(), index++);
            start = matcher.end();
        }
        // 验证head
        this.checkHeadInfo(columnList);
        return columnList;
    }

    private void makeOneColumn(List<BillHeadColumn> columnList, String columnName, int columnIndex) {
        BillColumnTypeEnum columnType = BillColumnTypeEnum.findColumnType(columnName);
        if (columnType == null) {
            throw new StockException("不支持的列名：" + columnName);
        }
        columnList.add(new BillHeadColumn(columnType, columnName, columnIndex));
        // System.out.println(columnName + " - " + columnIndex);
    }

    private void checkHeadInfo(List<BillHeadColumn> columnList) {
        if (findHeadColumn(columnList, BillColumnTypeEnum.DATE) == null) {
            throw new StockException("缺少列：发生日期");
        }
        if (findHeadColumn(columnList, BillColumnTypeEnum.TIME) == null) {
            throw new StockException("缺少列：成交时间");
        }
        if (findHeadColumn(columnList, BillColumnTypeEnum.BUSINESS_NAME) == null) {
            throw new StockException("缺少列：业务名称");
        }
        if (findHeadColumn(columnList, BillColumnTypeEnum.STOCK_CODE) == null) {
            throw new StockException("缺少列：证券代码");
        }
        if (findHeadColumn(columnList, BillColumnTypeEnum.STOCK_NAME) == null) {
            throw new StockException("缺少列：证券名称");
        }
        if (findHeadColumn(columnList, BillColumnTypeEnum.TRADE_PRICE) == null) {
            throw new StockException("缺少列：成交价格");
        }
        if (findHeadColumn(columnList, BillColumnTypeEnum.TRADE_NUMBER) == null) {
            throw new StockException("缺少列：成交数量");
        }
        if (findHeadColumn(columnList, BillColumnTypeEnum.FEE_SERVICE) == null) {
            throw new StockException("缺少列：手续费");
        }
        if (findHeadColumn(columnList, BillColumnTypeEnum.FEE_STAMP) == null) {
            throw new StockException("缺少列：印花税");
        }
        if (findHeadColumn(columnList, BillColumnTypeEnum.FEE_TRANSFER) == null) {
            throw new StockException("缺少列：过户费");
        }
        if (findHeadColumn(columnList, BillColumnTypeEnum.FEE_CLEAR) == null) {
            throw new StockException("缺少列：交易所清算费");
        }
        if (findHeadColumn(columnList, BillColumnTypeEnum.CLEAR_AMOUNT) == null) {
            throw new StockException("缺少列：发生金额");
        }
        if (findHeadColumn(columnList, BillColumnTypeEnum.AFTER_AMOUNT) == null) {
            throw new StockException("缺少列：资金本次余额");
        }
        if (findHeadColumn(columnList, BillColumnTypeEnum.ENTRUST_CODE) == null) {
            throw new StockException("缺少列：委托编号");
        }
    }

    private BillHeadColumn findHeadColumn(List<BillHeadColumn> columnList, BillColumnTypeEnum columnType) {
        for (BillHeadColumn column : columnList) {
            if (column.columnType == columnType) {
                return column;
            }
        }
        return null;
    }

    private List<StockRecordDto> parseLines(List<String> lineList, List<BillHeadColumn> columnList) {
        List<StockRecordDto> recordList = new ArrayList<>();
        for (String line : lineList) {
            if (StringUtils.isBlank(line) || line.startsWith("----") || line.startsWith(columnList.get(0).columnName)) {
                continue;
            }
            StockRecordDto recordDto = this.parseOneRecord(line, columnList);
            if (recordDto != null) {
                recordList.add(recordDto);
            }
        }
        return recordList;
    }

    private StockRecordDto parseOneRecord(String line, List<BillHeadColumn> columnList) {
        String[] parts = line.split("\\s+");
        if (parts == null || parts.length < 2) {
            log.warn("非法行, line = " + line);
            return null;
        }

        StockRecordDto recordDto = new StockRecordDto();
        int valueIndex = 0;
        for (BillHeadColumn headColumn : columnList) {
            if (headColumn.columnType == BillColumnTypeEnum.DATE) { // 发生日期
                String date = parts[valueIndex];
                if (StringUtils.isBlank(date)) {
                    throw new RuntimeException("日期列空值, line = " + line);
                }
                recordDto.setDate(date);
            } else if (headColumn.columnType == BillColumnTypeEnum.TIME) { // 成交时间
                String time = parts[valueIndex];
                if (StringUtils.isBlank(time)) {
                    throw new RuntimeException("时间列空值, line = " + line);
                }
                recordDto.setTime(time);
            } else if (headColumn.columnType == BillColumnTypeEnum.BUSINESS_NAME) { // 业务名称
                String businessName = parts[valueIndex];
                BillConst.BusinessName businessNameEnum = BillConst.BusinessName.getEnumByName(businessName);
                if (businessNameEnum == null) {
                    throw new RuntimeException("业务名称不支持: " + businessName + ", line = " + line);
                }
                recordDto.setBusinessName(businessNameEnum);
            } else if (headColumn.columnType == BillColumnTypeEnum.STOCK_CODE) { // 证券代码
                String stockCode = parts[valueIndex];
                if (StringUtils.isBlank(stockCode)) {
                    throw new RuntimeException("证券代码空值, line = " + line);
                }
                recordDto.setStockCode(stockCode);
            } else if (headColumn.columnType == BillColumnTypeEnum.STOCK_NAME) { // 证券名称 其中可能包含空格
                String stockName = parts[valueIndex];
                if (StringUtils.isBlank(stockName)) {
                    throw new RuntimeException("证券名称空值, line = " + line);
                }
                recordDto.setStockName(stockName);
            } else if (headColumn.columnType == BillColumnTypeEnum.TRADE_PRICE) { // 成交价格
                String tradePrice = parts[valueIndex];
                if (StringUtils.isBlank(tradePrice)) {
                    throw new RuntimeException("成交价格空值, line = " + line);
                }
                recordDto.setTradePrice(Double.valueOf(tradePrice));
            } else if (headColumn.columnType == BillColumnTypeEnum.TRADE_NUMBER) { // 成交数量，取绝对值
                String tradeNumber = parts[valueIndex];
                if (StringUtils.isBlank(tradeNumber)) {
                    throw new RuntimeException("成交数量空值, line = " + line);
                }
                recordDto.setTradeNumber(Math.abs(Double.valueOf(tradeNumber).intValue()));
            } else if (headColumn.columnType == BillColumnTypeEnum.TRADE_AMOUNT) { // 成交金额
                String tradeAmount = parts[valueIndex];
                if (StringUtils.isBlank(tradeAmount)) {
                    throw new RuntimeException("成交金额空值, line = " + line);
                }
                recordDto.setTradeAmount(Double.valueOf(tradeAmount));
            } else if (headColumn.columnType == BillColumnTypeEnum.AFTER_NUMBER) { // 股份余额
                String afterNumber = parts[valueIndex];
                if (StringUtils.isBlank(afterNumber)) {
                    throw new RuntimeException("股份余额空值, line = " + line);
                }
                recordDto.setAfterNumber(Double.valueOf(afterNumber).intValue());
            } else if (headColumn.columnType == BillColumnTypeEnum.FEE_SERVICE) { // 手续费
                String feeService = parts[valueIndex];
                if (StringUtils.isBlank(feeService)) {
                    throw new RuntimeException("手续费空值, line = " + line);
                }
                recordDto.setFeeService(Double.valueOf(feeService));
            } else if (headColumn.columnType == BillColumnTypeEnum.FEE_STAMP) { // 印花税
                String feeStamp = parts[valueIndex];
                if (StringUtils.isBlank(feeStamp)) {
                    throw new RuntimeException("印花税空值, line = " + line);
                }
                recordDto.setFeeStamp(Double.valueOf(feeStamp));
            } else if (headColumn.columnType == BillColumnTypeEnum.FEE_TRANSFER) { // 过户费
                String feeTransfer = parts[valueIndex];
                if (StringUtils.isBlank(feeTransfer)) {
                    throw new RuntimeException("过户费空值, line = " + line);
                }
                recordDto.setFeeTransfer(Double.valueOf(feeTransfer));
            } else if (headColumn.columnType == BillColumnTypeEnum.FEE_EXTRA) { // 附加费
                String feeExtra = parts[valueIndex];
                if (StringUtils.isBlank(feeExtra)) {
                    throw new RuntimeException("附加费空值, line = " + line);
                }
                recordDto.setFeeExtra(Double.valueOf(feeExtra));
            } else if (headColumn.columnType == BillColumnTypeEnum.FEE_CLEAR) { // 交易所清算费
                String feeClear = parts[valueIndex];
                if (StringUtils.isBlank(feeClear)) {
                    throw new RuntimeException("交易所清算费空值, line = " + line);
                }
                recordDto.setFeeClear(Double.valueOf(feeClear));
            } else if (headColumn.columnType == BillColumnTypeEnum.CLEAR_AMOUNT) { // 发生金额
                String clearAmountStr = parts[valueIndex];
                if (StringUtils.isBlank(clearAmountStr)) {
                    throw new RuntimeException("发生金额空值, line = " + line);
                }
                double clearAmount = Double.valueOf(clearAmountStr);
                if (BillUtils.isOpenRecord(recordDto)) {
                    // 开仓操作为负值
                    clearAmount = clearAmount > 0 ? -clearAmount : clearAmount;
                } else if (BillUtils.isCloseRecord(recordDto)) {
                    // 平仓操作为正值
                    clearAmount = clearAmount < 0 ? -clearAmount : clearAmount;
                }
                recordDto.setClearAmount(clearAmount);
            } else if (headColumn.columnType == BillColumnTypeEnum.AFTER_AMOUNT) { // 资金本次余额
                String afterAmount = parts[valueIndex];
                if (StringUtils.isBlank(afterAmount)) {
                    throw new RuntimeException("资金本次余额空值, line = " + line);
                }
                recordDto.setAfterAmount(Double.valueOf(afterAmount));
            } else if (headColumn.columnType == BillColumnTypeEnum.ENTRUST_CODE) { // 委托编号
                String entrustCode = parts[valueIndex];
                if (StringUtils.isBlank(entrustCode)) {
                    throw new RuntimeException("委托编号空值, line = " + line);
                }
                recordDto.setEntrustCode(entrustCode);
            }
            valueIndex++;
        }
        return recordDto;
    }

    // 头部列
    private class BillHeadColumn {
        private BillColumnTypeEnum columnType;
        private String columnName;
        private int columnIndex;

        public BillHeadColumn(BillColumnTypeEnum columnType, String columnName, int columnIndex) {
            this.columnType = columnType;
            this.columnName = columnName;
            this.columnIndex = columnIndex;
        }
    }
}
