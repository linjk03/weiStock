package com.weifly.weistock.record.bill;

import com.weifly.weistock.record.bill.domain.StockSummaryDto;

import java.util.List;

/**
 * 从xml中加载、保存交易记录
 *
 * @author weifly
 * @since 2020/01/16
 */
public interface BillStoreService {

    /**
     * 设置配置文件路径
     */
    void setConfigPath(String configPath);

    /**
     * 加载股票交易记录
     */
    StockSummaryDto loadStockSummary(String stockCode);

    /**
     * 保存股票交易记录
     */
    void saveStockSummary(StockSummaryDto summaryDto);

    /**
     * 加载股票列表
     */
    List<StockSummaryDto> loadStockList();
}
