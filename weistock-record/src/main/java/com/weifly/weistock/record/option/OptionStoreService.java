package com.weifly.weistock.record.option;

import com.weifly.weistock.record.option.domain.OptionYearDto;

import java.util.List;

/**
 * 从xml中加载、保存交易记录
 *
 * @author weifly
 * @since 2019/11/17
 */
public interface OptionStoreService {

    /**
     * 设置配置文件路径
     */
    void setConfigPath(String configPath);

    /**
     * 加载年份列表
     */
    List<OptionYearDto> loadYearList();

    /**
     * 保存年交易记录
     */
    void saveOptionYear(OptionYearDto yearDto);
}
