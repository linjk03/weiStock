package com.weifly.weistock.record.bill;

import com.weifly.weistock.record.bill.domain.StockRecordDto;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 解析股票对账单
 *
 * @author weifly
 * @since 2020/01/15
 */
public interface BillParseService {

    /**
     * 解析交易记录
     */
    List<StockRecordDto> parseRecord(File recordFile) throws IOException;

    /**
     * 解析交易记录
     */
    List<StockRecordDto> parseRecord(InputStream input) throws IOException;
}
