package com.weifly.weistock.record.bill;

/**
 * 股票账单常量定义
 *
 * @author weifly
 * @since 2020/01/15
 */
public class BillConst {

    /**
     * 业务名称枚举
     */
    public enum BusinessName {
        NAME_1("1", "证券买入", "买入"),
        NAME_2("2", "证券卖出", "卖出"),
        NAME_3("3", "担保品划入"),
        NAME_4("4", "担保品划出"),
        NAME_5("5", "融资买入"),
        NAME_6("6", "还款卖出"),
        NAME_7("7", "股息入帐"),
        NAME_8("8", "红股入帐"),
        NAME_9("9", "股息红利税补缴"),
        NAME_10("10", "基金申购");

        private String code; // 业务编码
        private String[] names; // 业务名称集合

        BusinessName(String code, String... names) {
            this.code = code;
            this.names = names;
        }

        public String getCode() {
            return code;
        }

        public String getName() {
            return names[0];
        }

        public static BillConst.BusinessName getEnumByName(String name) {
            for (BillConst.BusinessName nameEnum : BillConst.BusinessName.values()) {
                for (String oneName : nameEnum.names) {
                    if (oneName.equals(name)) {
                        return nameEnum;
                    }
                }
            }
            return null;
        }
    }
}
