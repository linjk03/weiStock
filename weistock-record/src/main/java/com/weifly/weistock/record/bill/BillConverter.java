package com.weifly.weistock.record.bill;

import com.weifly.weistock.record.RecordUtils;
import com.weifly.weistock.record.bill.domain.GetStockRecordRequest;
import com.weifly.weistock.record.bill.domain.StockRecordDto;
import com.weifly.weistock.record.bill.domain.StockSummaryDto;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * converter
 *
 * @author weifly
 * Create at 2021/01/13
 */
public class BillConverter {

    public static GetStockRecordRequest convertToGetStockRecordRequest(HttpServletRequest request, int limit) {
        GetStockRecordRequest queryRequest = new GetStockRecordRequest();
        queryRequest.setStockCode(request.getParameter("stockCode")); // 股票代码
        queryRequest.setDate(request.getParameter("date")); // 发生日期
        queryRequest.setTime(request.getParameter("time")); // 成交时间
        queryRequest.setEntrust(request.getParameter("entrust")); // 委托编号
        queryRequest.setLimit(limit + 1);
        return queryRequest;
    }

    public static List<StockSummaryDto> sortStockList(List<StockSummaryDto> stockList) {
        stockList.sort((d1, d2) -> {
            String d1Date = "", d1Time = "", d2Date = "", d2Time = "";
            if (d1.getRecordList().size() > 0) {
                StockRecordDto lastRecord = d1.getRecordList().get(d1.getRecordList().size() - 1);
                d1Date = lastRecord.getDate();
                d1Time = lastRecord.getTime();
            }
            if (d2.getRecordList().size() > 0) {
                StockRecordDto lastRecord = d2.getRecordList().get(d2.getRecordList().size() - 1);
                d2Date = lastRecord.getDate();
                d2Time = lastRecord.getTime();
            }
            if (!d2Date.equals(d1Date)) {
                return d2Date.compareTo(d1Date);
            } else {
                return d2Time.compareTo(d1Time);
            }
        });
        return stockList;
    }

    public static Map<String, Object> convertStockSummary(StockSummaryDto stockSummary) {
        Map<String, Object> recordInfo = new HashMap<>();
        recordInfo.put("stockCode", stockSummary.getStockCode() == null ? "" : stockSummary.getStockCode());
        recordInfo.put("stockName", stockSummary.getStockName() == null ? "" : stockSummary.getStockName());
        recordInfo.put("recordSize", stockSummary.getRecordList().size()); // 交易次数
        recordInfo.put("diffAmount", stockSummary.getDiffAmount() == null ? "" : stockSummary.getDiffAmount()); // 差额
        recordInfo.put("feeService", stockSummary.getFeeService() == null ? "" : stockSummary.getFeeService()); // 手续费
        recordInfo.put("feeStamp", stockSummary.getFeeStamp() == null ? "" : stockSummary.getFeeStamp()); // 印花税
        recordInfo.put("afterNumber", stockSummary.getAfterNumber() == null ? "" : stockSummary.getAfterNumber()); // 剩余数量
        String lastOrderTime = "";
        if (stockSummary.getRecordList().size() > 0) {
            StockRecordDto lastRecord = stockSummary.getRecordList().get(stockSummary.getRecordList().size() - 1);
            lastOrderTime = lastRecord.getDate() + " " + lastRecord.getTime();
        }
        recordInfo.put("lastOrderTime", lastOrderTime); // 最后交易时间
        return recordInfo;
    }

    public static Map<String, Object> convertStockRecord(StockRecordDto stockRecord) {
        Map<String, Object> recordInfo = new HashMap<>();
        recordInfo.put("date", stockRecord.getDate());
        recordInfo.put("time", stockRecord.getTime());
        recordInfo.put("entrustCode", stockRecord.getEntrustCode());
        recordInfo.put("businessName", stockRecord.getBusinessName().getName());
        recordInfo.put("tradePrice", stockRecord.getTradePrice());
        recordInfo.put("tradeNumber", stockRecord.getTradeNumber());
        recordInfo.put("tradeAmount", stockRecord.getTradeAmount());
        recordInfo.put("feeService", stockRecord.getFeeService());
        recordInfo.put("feeStamp", stockRecord.getFeeStamp());
        recordInfo.put("clearAmount", stockRecord.getClearAmount());
        recordInfo.put("diffAmount", stockRecord.getDiffAmount());
        recordInfo.put("pair", RecordUtils.makePairAttr(stockRecord.getPairList(), stockRecord.getTradeNumber()));
        return recordInfo;
    }
}
