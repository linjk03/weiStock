package com.weifly.weistock.record.bill.bo;

import com.weifly.weistock.record.bill.constant.BillColumnTypeEnum;

/**
 * 股票对账单头部，某列信息
 *
 * @author weifly
 * @since 2021/05/14
 */
public class BillHeadColumn {

    /**
     * 列类型
     */
    private BillColumnTypeEnum columnType;

    /**
     * 列名称
     */
    private String columnName;

    /**
     * 列索引
     */
    private int columnIndex;

    public BillColumnTypeEnum getColumnType() {
        return columnType;
    }

    public void setColumnType(BillColumnTypeEnum columnType) {
        this.columnType = columnType;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public int getColumnIndex() {
        return columnIndex;
    }

    public void setColumnIndex(int columnIndex) {
        this.columnIndex = columnIndex;
    }
}
