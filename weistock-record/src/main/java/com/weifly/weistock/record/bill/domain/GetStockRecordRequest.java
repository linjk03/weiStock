package com.weifly.weistock.record.bill.domain;

/**
 * 获取记录请求
 *
 * @author weifly
 * @since 2020/01/17
 */
public class GetStockRecordRequest {

    private String stockCode;
    private String date; // 发生日期
    private String time; // 成交时间
    private String entrust; // 委托编号
    private int limit; // 返回记录数

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEntrust() {
        return entrust;
    }

    public void setEntrust(String entrust) {
        this.entrust = entrust;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
