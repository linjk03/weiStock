package com.weifly.weistock.record.bo;

import java.util.List;

/**
 * 交易记录
 *
 * @author weifly
 * @since 2020/01/16
 */
public class AbstractRecordBO {

    private String date; // 发生日期, 20191224
    private String time; // 成交时间, 09:42:51
    private String entrustCode; // 委托编号

    private List<RecordPair> pairList; // 成交配对列表
    private Double diffAmount; // 配对结算金额

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEntrustCode() {
        return entrustCode;
    }

    public void setEntrustCode(String entrustCode) {
        this.entrustCode = entrustCode;
    }

    public List<RecordPair> getPairList() {
        return pairList;
    }

    public void setPairList(List<RecordPair> pairList) {
        this.pairList = pairList;
    }

    public Double getDiffAmount() {
        return diffAmount;
    }

    public void setDiffAmount(Double diffAmount) {
        this.diffAmount = diffAmount;
    }
}
