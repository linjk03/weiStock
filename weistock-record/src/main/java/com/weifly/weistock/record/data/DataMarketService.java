package com.weifly.weistock.record.data;

import com.weifly.weistock.record.data.domain.StockDataDto;

/**
 * 数据市场服务
 *
 * @author weifly
 * @since 2019/9/29
 */
public interface DataMarketService {

    /**
     * 加载股票数据
     */
    StockDataDto loadStockData(String stockCode);

    /**
     * 获取股票名称
     */
    String getStockName(String stockCode);
}
