package com.weifly.weistock.record.option.impl;

import com.weifly.weistock.record.option.domain.OptionRecordDto;
import com.weifly.weistock.record.option.domain.OptionYearDto;

import java.util.*;

/**
 * 遍历期权记录
 *
 * @author weifly
 * @since 2019/11/23
 */
public class OptionRecordVisitor {

    private List<OptionYearDto> yearList;

    public OptionRecordVisitor(Collection<OptionYearDto> years){
        this.yearList = new ArrayList<>();
        this.yearList.addAll(years);
    }

    public Iterator<OptionRecordDto> nextIter(){
        return new NextIterator();
    }

    private class NextIterator<OptionRecordDto> implements Iterator<OptionRecordDto>{

        private Iterator<OptionYearDto> yearIter;
        private Iterator<OptionRecordDto> recordIter;

        public NextIterator(){
            this.yearIter = yearList.iterator();
        }

        @Override
        public boolean hasNext() {
            while(this.recordIter==null || !this.recordIter.hasNext()){
                // 获得新的recordIter
                this.recordIter = this.takeNewRecordIter();
                if(this.recordIter==null){
                    return false;
                }
            }
            return true;
        }

        private Iterator takeNewRecordIter(){
            while(this.yearIter.hasNext()){
                OptionYearDto yearDto = this.yearIter.next();
                if(yearDto.getRecordList().size()>0){
                    return yearDto.getRecordList().iterator();
                }
            }
            return null;
        }

        @Override
        public OptionRecordDto next() {
            return this.recordIter.next();
        }
    }
}
