package com.weifly.weistock.record.data.impl;

import com.weifly.weistock.core.config.AbstractConfigService;
import com.weifly.weistock.core.config.ConfigConstants;
import com.weifly.weistock.record.data.DataStoreService;
import com.weifly.weistock.record.data.domain.StockDataDto;
import com.weifly.weistock.record.data.domain.StockDayDto;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 数据存储服务实现
 *
 * @author weifly
 * @since 2019/9/29
 */
public class DataStoreServiceImpl extends AbstractConfigService implements DataStoreService {

    private static final String ATTR_STOCK = "stock";
    private static final String ATTR_OPEN = "open";
    private static final String ATTR_HIGH = "high";
    private static final String ATTR_LOW = "low";
    private static final String ATTR_CLOSE = "close";

    @Override
    public List<StockDataDto> loadStockDataList() {
        List<StockDataDto> stockList = new ArrayList<>();
        for(File xmlFile : this.getConfigFolder().listFiles()){
            if(xmlFile.getName().endsWith(".xml")){
                log.info("加载股票数据：" + xmlFile.getAbsolutePath());
                StockDataDto stockDto = this.loadDataXml(xmlFile);
                stockList.add(stockDto);
            }
        }
        return stockList;
    }

    private StockDataDto loadDataXml(File xmlFile){
        StockDataDto stockDto = new StockDataDto();
        try{
            SAXReader reader = new SAXReader();
            Document document = reader.read(xmlFile);
            Element rootEle = document.getRootElement();

            String stockCode = rootEle.attributeValue(ATTR_STOCK);
            stockDto.setCode(stockCode);

            List<Element> recordEleList = rootEle.elements(ConfigConstants.ELE_RECORD);
            if(recordEleList!=null){
                for(Element recordEle : recordEleList){
                    StockDayDto dayDto = new StockDayDto();
                    String day = recordEle.attributeValue(ConfigConstants.ATTR_DAY);
                    dayDto.setDay(day);

                    String openStr = recordEle.attributeValue(ATTR_OPEN);
                    double open = Double.parseDouble(openStr);
                    dayDto.setOpen(open);

                    String highStr = recordEle.attributeValue(ATTR_HIGH);
                    double high = Double.parseDouble(highStr);
                    dayDto.setHigh(high);

                    String lowStr = recordEle.attributeValue(ATTR_LOW);
                    double low = Double.parseDouble(lowStr);
                    dayDto.setLow(low);

                    String closeStr = recordEle.attributeValue(ATTR_CLOSE);
                    double close = Double.parseDouble(closeStr);
                    dayDto.setClose(close);

                    stockDto.getDayList().add(dayDto);
                }
            }
            return stockDto;
        }catch(DocumentException e){
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void saveStockData(StockDataDto stockDto) {
        DocumentFactory factory = DocumentFactory.getInstance();
        Document document = factory.createDocument();
        document.setXMLEncoding("UTF8");

        Element rootEle = factory.createElement(ConfigConstants.ELE_ROOT);
        rootEle.addAttribute(ATTR_STOCK, stockDto.getCode());
        document.setRootElement(rootEle);

        for(StockDayDto dayDto : stockDto.getDayList()){
            Element recordEle = factory.createElement(ConfigConstants.ELE_RECORD);
            recordEle.addAttribute(ConfigConstants.ATTR_DAY, dayDto.getDay());
            recordEle.addAttribute(ATTR_OPEN, String.valueOf(dayDto.getOpen()));
            recordEle.addAttribute(ATTR_HIGH, String.valueOf(dayDto.getHigh()));
            recordEle.addAttribute(ATTR_LOW, String.valueOf(dayDto.getLow()));
            recordEle.addAttribute(ATTR_CLOSE, String.valueOf(dayDto.getClose()));
            rootEle.add(recordEle);
        }

        File xmlFile = new File(this.getConfigFolder(), "stock_" + stockDto.getCode() + ".xml");
        log.info("保存股票数据, file={}", xmlFile.getAbsolutePath());
        this.saveConfig(xmlFile, document);
    }
}
