package com.weifly.weistock.record.option;

/**
 * 期权常量定义
 *
 * @author weifly
 * @since 2019/11/17
 */
public class OptionConst {

    /**
     * 业务名称枚举
     */
    public enum BusinessName{
        NAME_1("1", "备兑开仓"),
        NAME_2("2", "备兑平仓"),
        NAME_3("3", "买入开仓"),
        NAME_4("4", "买入平仓"),
        NAME_5("5", "卖出开仓"),
        NAME_6("6", "卖出平仓"),
        NAME_7("7", "期权仓位对冲"),
        NAME_8("8", "期权持仓到期注销"),
        NAME_9("9", "行权指派"),
        NAME_10("10", "行权资金拨出");

        private String code; // 业务编码
        private String name; // 业务名称

        private BusinessName(String code, String name){
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

        public static BusinessName getEnumByName(String name){
            for(BusinessName nameEnum : BusinessName.values()){
                if(nameEnum.getName().equals(name)){
                    return nameEnum;
                }
            }
            return null;
        }
    }

    /**
     * 买卖标志枚举
     */
    public enum OperationType {

        TYPE_1("1", "备兑开仓"),
        TYPE_2("2", "备兑平仓"),
        TYPE_3("3", "买入开仓"),
        TYPE_4("4", "买入平仓"),
        TYPE_5("5", "卖出开仓"),
        TYPE_6("6", "卖出平仓"),
        TYPE_7("7", "买入");

        private String code; // 编码
        private String name; // 名称

        private OperationType(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

        public static OperationType getEnumByName(String name) {
            for (OperationType nameEnum : OperationType.values()) {
                if (nameEnum.getName().equals(name)) {
                    return nameEnum;
                }
            }
            return null;
        }
    }

    /**
     * 持仓方向
     */
    public enum HoldPosition {

        HOLD_POSITION_1("1", "备兑仓"),
        HOLD_POSITION_2("2", "权利仓"),
        HOLD_POSITION_3("3", "义务仓");

        private String code; // 编码
        private String name; // 名称

        private HoldPosition(String code, String name){
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

        public static HoldPosition getEnumByName(String name){
            for(HoldPosition nameEnum : HoldPosition.values()){
                if(nameEnum.getName().equals(name)){
                    return nameEnum;
                }
            }
            return null;
        }

        public static HoldPosition getEnumByTypeName(String typeName){
            if("备兑".equals(typeName)){
                return HOLD_POSITION_1;
            }else if("权利".equals(typeName)){
                return HOLD_POSITION_2;
            }else if("义务".equals(typeName)){
                return HOLD_POSITION_3;
            }else{
                throw new RuntimeException("不支持："+typeName);
            }
        }
    }
}
