package com.weifly.weistock.record.data.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * 某股票日数据集合
 *
 * @author weifly
 * @since 2019/9/29
 */
public class StockDataDto {

    private String code; // 股票代码
    private List<StockDayDto> dayList = new ArrayList<>(); // 记录列表

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<StockDayDto> getDayList() {
        return dayList;
    }

    public void setDayList(List<StockDayDto> dayList) {
        this.dayList = dayList;
    }
}
