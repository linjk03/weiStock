package com.weifly.weistock.record.asset;

import com.weifly.weistock.record.asset.domain.AssetDayDto;
import com.weifly.weistock.record.asset.domain.AssetYearDto;

import java.util.List;

/**
 * 用户资产服务，记录每天的总资产
 *
 * @author weifly
 * @since 2019/9/17
 */
public interface AssetMonitorService {

    /**
     * 更新所有资产记录
     */
    void updateYearList(List<AssetYearDto> yearList);

    /**
     * 更新某天的总资产
     */
    void updateAsset(AssetDayDto assetDay);

    /**
     * 加载资产记录列表
     *
     * @param day 时间下限，返回的记录都小于day
     * @param size 返回记录数
     */
    List<AssetDayDto> getAssetDayList(String day, int size);

    /**
     * 加载资产记录列表，用于chart显示
     *
     * @param lowDay 日期下限
     * @return lowDay到最新日期之间的所有记录
     */
    List<AssetDayDto> loadChartDayList(String lowDay);
}
