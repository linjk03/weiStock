package com.weifly.weistock.record.data;

import com.weifly.weistock.record.data.domain.StockDataDto;

import java.util.List;

/**
 * 数据存储服务
 *
 * @author weifly
 * @since 2019/9/29
 */
public interface DataStoreService {

    /**
     * 设置配置文件路径
     */
    void setConfigPath(String configPath);

    /**
     * 加载股票数据列表
     */
    List<StockDataDto> loadStockDataList();

    /**
     * 保存股票数据
     */
    void saveStockData(StockDataDto stockDto);
}
