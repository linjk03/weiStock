package com.weifly.weistock.record.data;

import com.weifly.weistock.record.data.domain.StockDayDto;
import com.weifly.weistock.record.data.domain.StockDataDto;

import java.util.List;

/**
 * 市场数据服务
 *
 * @author weifly
 * @since 2019/9/17
 */
public interface DataHistoryService {

    /**
     * 更新所有股票数据
     */
    void updateStockList(List<StockDataDto> stockList);

    /**
     * 更新股票数据
     */
    void updateStockData(StockDataDto stockDto);

    /**
     * 加载数据列表，用于chart显示
     *
     * @param stockCode 股票代码
     * @param lowDay 日期下限
     * @return lowDay到最新日期之间的所有记录
     */
    List<StockDayDto> loadChartDayList(String stockCode, String lowDay);
}
