package com.weifly.weistock.optionvix;

import com.weifly.weistock.core.util.ModuleBuilder;
import com.weifly.weistock.optionvix.bo.VixYearBO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;

/**
 * builder
 *
 * @author weifly
 * @since 2021/1/22
 */
public class OptionVixBuilder implements ModuleBuilder {

    private Logger log = LoggerFactory.getLogger(OptionVixBuilder.class);

    private OptionVixConfigService optionVixConfigService;
    private OptionVixMonitorService optionVixMonitorService;

    public void setOptionVixConfigService(OptionVixConfigService optionVixConfigService) {
        this.optionVixConfigService = optionVixConfigService;
    }

    public void setOptionVixMonitorService(OptionVixMonitorService optionVixMonitorService) {
        this.optionVixMonitorService = optionVixMonitorService;
    }

    @Override
    public void build(String configPath) {
        String vixPath = new File(configPath, "optionvix").getAbsolutePath();
        log.info("构建期权VIX模块: " + vixPath);
        this.optionVixConfigService.setConfigPath(vixPath);
        List<VixYearBO> yearList = this.optionVixConfigService.loadYearList();
        this.optionVixMonitorService.updateYearList(yearList);
    }
}
