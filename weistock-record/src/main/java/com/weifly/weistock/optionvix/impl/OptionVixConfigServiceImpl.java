package com.weifly.weistock.optionvix.impl;

import com.weifly.weistock.core.config.AbstractConfigService;
import com.weifly.weistock.core.config.ConfigConstants;
import com.weifly.weistock.optionvix.OptionVixConfigService;
import com.weifly.weistock.optionvix.bo.VixDayBO;
import com.weifly.weistock.optionvix.bo.VixDayExternalBO;
import com.weifly.weistock.optionvix.bo.VixYearBO;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * VIX配置服务实现
 *
 * @author weifly
 * @since 2021/1/21
 */
public class OptionVixConfigServiceImpl extends AbstractConfigService implements OptionVixConfigService {

    public static final String PREFIX_VIX = "vix_";

    @Override
    public List<VixYearBO> loadYearList() {
        List<VixYearBO> yearList = new ArrayList<>();
        for (File xmlFile : this.getConfigFolder().listFiles()) {
            String fileName = xmlFile.getName();
            if (fileName.startsWith(PREFIX_VIX) && fileName.endsWith(".xml")) {
                log.info("加载VIX记录：" + xmlFile.getAbsolutePath());
                yearList.add(this.loadYearXml(xmlFile));
            }
        }
        return yearList;
    }

    private VixYearBO loadYearXml(File xmlFile) {
        VixYearBO yearBO = new VixYearBO();
        try {
            SAXReader reader = new SAXReader();
            Document document = reader.read(xmlFile);
            Element rootEle = document.getRootElement();

            yearBO.setYear(rootEle.attributeValue(ConfigConstants.ATTR_YEAR));

            List<Element> recordEleList = rootEle.elements(ConfigConstants.ELE_RECORD);
            if (recordEleList != null) {
                for (Element recordEle : recordEleList) {
                    VixDayBO dayBO = new VixDayBO();
                    dayBO.setDay(recordEle.attributeValue(ConfigConstants.ATTR_DAY));
                    dayBO.setOpenValue(this.getDoubleAttr(recordEle, ConfigConstants.ATTR_OPEN));
                    dayBO.setHighValue(this.getDoubleAttr(recordEle, ConfigConstants.ATTR_HIGH));
                    dayBO.setLowValue(this.getDoubleAttr(recordEle, ConfigConstants.ATTR_LOW));
                    dayBO.setCloseValue(this.getDoubleAttr(recordEle, ConfigConstants.ATTR_CLOSE));
                    yearBO.getDayList().add(dayBO);
                }
            }
            return yearBO;
        } catch (DocumentException e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void saveYear(VixYearBO yearBO) {
        DocumentFactory factory = DocumentFactory.getInstance();
        Document document = factory.createDocument();
        document.setXMLEncoding("UTF8");

        Element rootEle = factory.createElement(ConfigConstants.ELE_ROOT);
        rootEle.addAttribute(ConfigConstants.ATTR_YEAR, yearBO.getYear());
        document.setRootElement(rootEle);

        for (VixDayBO dayBO : yearBO.getDayList()) {
            Element recordEle = factory.createElement(ConfigConstants.ELE_RECORD);
            recordEle.addAttribute(ConfigConstants.ATTR_DAY, dayBO.getDay());
            recordEle.addAttribute(ConfigConstants.ATTR_OPEN, String.valueOf(dayBO.getOpenValue()));
            recordEle.addAttribute(ConfigConstants.ATTR_HIGH, String.valueOf(dayBO.getHighValue()));
            recordEle.addAttribute(ConfigConstants.ATTR_LOW, String.valueOf(dayBO.getLowValue()));
            recordEle.addAttribute(ConfigConstants.ATTR_CLOSE, String.valueOf(dayBO.getCloseValue()));
            rootEle.add(recordEle);
        }

        File xmlFile = new File(this.getConfigFolder(), PREFIX_VIX + yearBO.getYear() + ".xml");
        log.info("保存VIX记录, file={}", xmlFile.getAbsolutePath());
        this.saveConfig(xmlFile, document);
    }

    @Override
    public void saveDayExternal(Map<String, VixDayExternalBO> dayExternalMap) {
        DocumentFactory factory = DocumentFactory.getInstance();
        Document document = factory.createDocument();
        document.setXMLEncoding("UTF8");

        Element rootEle = factory.createElement(ConfigConstants.ELE_ROOT);
        document.setRootElement(rootEle);

        for (VixDayExternalBO dayExternal : dayExternalMap.values()) {
            Element recordEle = factory.createElement(ConfigConstants.ELE_RECORD);
            recordEle.addAttribute(ConfigConstants.ATTR_DAY, dayExternal.getDay());
            recordEle.addAttribute("diff", String.valueOf(dayExternal.getDiff()));
            recordEle.addAttribute("rate", String.valueOf(dayExternal.getRate()));
            recordEle.addAttribute("recentPercent", String.valueOf(dayExternal.getRecentPercent()));
            recordEle.addAttribute("allPercent", String.valueOf(dayExternal.getAllPercent()));
            rootEle.add(recordEle);
        }

        File xmlFile = new File(this.getConfigFolder(), "day_external.xml");
        log.info("保存VIX附加信息, file={}", xmlFile.getAbsolutePath());
        this.saveConfig(xmlFile, document);
    }
}
