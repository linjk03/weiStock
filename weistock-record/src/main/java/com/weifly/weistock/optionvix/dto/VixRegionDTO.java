package com.weifly.weistock.optionvix.dto;

/**
 * VIX分布
 *
 * @author weifly
 * @since 2021/1/21
 */
public class VixRegionDTO {

    private double highVix; // VIX高值
    private double lowVix; // VIX低值

    private int recentDayNumber; // 近期-天数
    private double recentDayPercent; // 近期-天数百分比
    private int recentTotalDayNumber; // 近期-累积天数
    private double recentTotalDayPercent; // 近期-累积天数百分比

    private int allDayNumber; // all-天数
    private double allDayPercent; // all-天数百分比
    private int allTotalDayNumber; // all-累积天数
    private double allTotalDayPercent; // all-累积天数百分比

    public double getHighVix() {
        return highVix;
    }

    public void setHighVix(double highVix) {
        this.highVix = highVix;
    }

    public double getLowVix() {
        return lowVix;
    }

    public void setLowVix(double lowVix) {
        this.lowVix = lowVix;
    }

    public int getRecentDayNumber() {
        return recentDayNumber;
    }

    public void setRecentDayNumber(int recentDayNumber) {
        this.recentDayNumber = recentDayNumber;
    }

    public double getRecentDayPercent() {
        return recentDayPercent;
    }

    public void setRecentDayPercent(double recentDayPercent) {
        this.recentDayPercent = recentDayPercent;
    }

    public int getRecentTotalDayNumber() {
        return recentTotalDayNumber;
    }

    public void setRecentTotalDayNumber(int recentTotalDayNumber) {
        this.recentTotalDayNumber = recentTotalDayNumber;
    }

    public double getRecentTotalDayPercent() {
        return recentTotalDayPercent;
    }

    public void setRecentTotalDayPercent(double recentTotalDayPercent) {
        this.recentTotalDayPercent = recentTotalDayPercent;
    }

    public int getAllDayNumber() {
        return allDayNumber;
    }

    public void setAllDayNumber(int allDayNumber) {
        this.allDayNumber = allDayNumber;
    }

    public double getAllDayPercent() {
        return allDayPercent;
    }

    public void setAllDayPercent(double allDayPercent) {
        this.allDayPercent = allDayPercent;
    }

    public int getAllTotalDayNumber() {
        return allTotalDayNumber;
    }

    public void setAllTotalDayNumber(int allTotalDayNumber) {
        this.allTotalDayNumber = allTotalDayNumber;
    }

    public double getAllTotalDayPercent() {
        return allTotalDayPercent;
    }

    public void setAllTotalDayPercent(double allTotalDayPercent) {
        this.allTotalDayPercent = allTotalDayPercent;
    }
}
