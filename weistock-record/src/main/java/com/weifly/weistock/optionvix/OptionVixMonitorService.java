package com.weifly.weistock.optionvix;

import com.weifly.weistock.optionvix.bo.VixDayBO;
import com.weifly.weistock.optionvix.bo.VixDayExternalBO;
import com.weifly.weistock.optionvix.bo.VixYearBO;
import com.weifly.weistock.optionvix.dto.GetDayListRequest;
import com.weifly.weistock.optionvix.dto.VixSummaryDTO;

import java.util.List;

/**
 * 期权VIX监控服务
 *
 * @author weifly
 * @since 2021/1/21
 */
public interface OptionVixMonitorService {

    /**
     * 更新所有VIX记录
     */
    void updateYearList(List<VixYearBO> yearList);

    /**
     * 更新某天的VIX
     */
    void updateVix(VixDayBO vixDay);

    /**
     * 计算统计信息
     */
    VixSummaryDTO calcMonitorInfo();

    /**
     * 获取dayList
     */
    List<VixDayBO> getVixDayList(GetDayListRequest dayListRequest);

    /**
     * 获得VIX附加信息
     */
    VixDayExternalBO getVixDayExternal(String day);

    /**
     * 加载VIX记录列表，用于chart显示
     *
     * @param lowDay 日期下限
     * @return lowDay到最新日期之间的所有记录
     */
    List<VixDayBO> loadChartDayList(String lowDay);
}
