package com.weifly.weistock.optionvix.dto;

/**
 * 获取dayList请求
 *
 * @author weifly
 * @since 2021/1/22
 */
public class GetDayListRequest {

    private String day;
    private int limit; // 返回记录数

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
