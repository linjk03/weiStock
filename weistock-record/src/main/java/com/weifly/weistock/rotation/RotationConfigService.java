package com.weifly.weistock.rotation;

import com.weifly.weistock.rotation.bo.RotationConfigBO;

/**
 * 轮动配置服务
 *
 * @author weifly
 * @since 2020/11/30
 */
public interface RotationConfigService {

    /**
     * 设置配置文件路径
     */
    void setConfigPath(String configPath);

    /**
     * 加载配置
     */
    RotationConfigBO loadRotationConfig();

    /**
     * 保存配置
     */
    void saveRotationConfig(RotationConfigBO configBO);
}
