package com.weifly.weistock.rotation;

import com.weifly.weistock.rotation.bo.RotationConfigBO;
import com.weifly.weistock.core.util.ModuleBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * 轮动监控builder
 *
 * @author weifly
 * @since 2020/12/01
 */
public class RotationBuilder implements ModuleBuilder {

    private Logger log = LoggerFactory.getLogger(RotationBuilder.class);

    private RotationConfigService rotationConfigService;

    private RotationMonitorService rotationMonitorService;

    public void setRotationConfigService(RotationConfigService rotationConfigService) {
        this.rotationConfigService = rotationConfigService;
    }

    public void setRotationMonitorService(RotationMonitorService rotationMonitorService) {
        this.rotationMonitorService = rotationMonitorService;
    }

    @Override
    public void build(String configPath) {
        String rotationPath = new File(configPath, "rotation").getAbsolutePath();
        log.info("构建轮动监控模块: " + rotationPath);
        this.rotationConfigService.setConfigPath(rotationPath);
        RotationConfigBO rotationConfig = this.rotationConfigService.loadRotationConfig();
        this.rotationMonitorService.updateRotationConfig(rotationConfig, false);
    }
}
