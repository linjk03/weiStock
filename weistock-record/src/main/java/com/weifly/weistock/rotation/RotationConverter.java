package com.weifly.weistock.rotation;

import com.weifly.weistock.rotation.bo.RotationConfigBO;
import com.weifly.weistock.core.common.Result;
import com.weifly.weistock.rotation.bo.RotationStatusBO;
import com.weifly.weistock.rotation.dto.RotationStatusDTO;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * 轮动converter
 *
 * @author weifly
 * Create at 2020/12/01
 */
public class RotationConverter {

    public static Result convertToRotationConfig(HttpServletRequest request, RotationConfigBO rotationConfig) {
        rotationConfig.setOpen("true".equals(request.getParameter("open")));

        String baseStockCode = request.getParameter("baseStockCode");
        if (StringUtils.isBlank(baseStockCode)) {
            return Result.createErrorResult("缺少配置：基准标的代码");
        }
        rotationConfig.setBaseStockCode(baseStockCode);

        String baseStockName = request.getParameter("baseStockName");
        if (StringUtils.isBlank(baseStockName)) {
            return Result.createErrorResult("缺少配置：基准标的名称");
        }
        rotationConfig.setBaseStockName(baseStockName);

        String baseStockPrice = request.getParameter("baseStockPrice");
        if (StringUtils.isBlank(baseStockPrice)) {
            return Result.createErrorResult("缺少配置：基准标的价格");
        }
        double baseStockPriceDouble = Double.parseDouble(baseStockPrice);
        if (baseStockPriceDouble < 0) {
            return Result.createErrorResult("基准标的价格应大于0");
        }
        rotationConfig.setBaseStockPrice(baseStockPriceDouble);

        String compareStockCode = request.getParameter("compareStockCode");
        if (StringUtils.isBlank(compareStockCode)) {
            return Result.createErrorResult("缺少配置：比较标的代码");
        }
        rotationConfig.setCompareStockCode(compareStockCode);

        String compareStockName = request.getParameter("compareStockName");
        if (StringUtils.isBlank(compareStockName)) {
            return Result.createErrorResult("缺少配置：比较标的名称");
        }
        rotationConfig.setCompareStockName(compareStockName);

        String compareStockPrice = request.getParameter("compareStockPrice");
        if (StringUtils.isBlank(compareStockPrice)) {
            return Result.createErrorResult("缺少配置：比较标的价格");
        }
        double compareStockPriceDouble = Double.parseDouble(compareStockPrice);
        if (compareStockPriceDouble < 0) {
            return Result.createErrorResult("比较标的价格应大于0");
        }
        rotationConfig.setCompareStockPrice(compareStockPriceDouble);

        return null;
    }

    public static void clear(RotationStatusBO rotationStatus) {
        rotationStatus.setNowDiffTime(null);
        rotationStatus.setNowDiff(0);
        rotationStatus.setMinDiffTime(null);
        rotationStatus.setMinDiff(0);
        rotationStatus.setMaxDiffTime(null);
        rotationStatus.setMaxDiff(0);
    }

    public static void calcMonitorInfo(RotationStatusDTO statusDTO, RotationConfigBO configBO, RotationStatusBO statusBO){
        statusDTO.setBaseStockCode(configBO.getBaseStockCode());
        statusDTO.setBaseStockName(configBO.getBaseStockName());
        statusDTO.setCompareStockCode(configBO.getCompareStockCode());
        statusDTO.setCompareStockName(configBO.getCompareStockName());
        statusDTO.setNowDiffTime(statusBO.getNowDiffTime());
        statusDTO.setNowDiff(statusBO.getNowDiff());
        statusDTO.setMinDiffTime(statusBO.getMinDiffTime());
        statusDTO.setMinDiff(statusBO.getMinDiff());
        statusDTO.setMaxDiffTime(statusBO.getMaxDiffTime());
        statusDTO.setMaxDiff(statusBO.getMaxDiff());
    }
}
