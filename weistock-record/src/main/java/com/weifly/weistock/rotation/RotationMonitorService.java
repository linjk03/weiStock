package com.weifly.weistock.rotation;

import com.weifly.weistock.rotation.bo.RotationConfigBO;
import com.weifly.weistock.rotation.bo.RotationPointBO;
import com.weifly.weistock.core.task.UpdateStatus;
import com.weifly.weistock.rotation.dto.RotationStatusDTO;

/**
 * 轮动比值监控服务
 *
 * @author weifly
 * @since 2020/12/01
 */
public interface RotationMonitorService {

    /**
     * 更新轮动配置
     */
    void updateRotationConfig(RotationConfigBO configBO, boolean save);

    /**
     * 获取轮动配置
     *
     * @return
     */
    RotationConfigBO getRotationConfig();

    /**
     * 清空监控信息
     */
    void clearMonitorInfo();

    /**
     * 增加监控点
     */
    void addRotationPoint(RotationPointBO rotationPoint);

    /**
     * 计算统计信息
     */
    RotationStatusDTO calcMonitorInfo();

    /**
     * 更新状态信息
     */
    void updateStatus(UpdateStatus status);

    /**
     * 写入日志消息
     */
    void writeMessage(String message);
}
