package com.weifly.weistock.rotation.impl;

import com.weifly.weistock.core.config.AbstractConfigService;
import com.weifly.weistock.core.config.ConfigConstants;
import com.weifly.weistock.rotation.RotationConfigService;
import com.weifly.weistock.rotation.bo.RotationConfigBO;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;

/**
 * 轮动配置服务实现
 *
 * @author weifly
 * @since 2019/11/18
 */
public class RotationConfigServiceImpl extends AbstractConfigService implements RotationConfigService {

    private static final String ELE_baseStockCode = "baseStockCode";
    private static final String ELE_baseStockName = "baseStockName";
    private static final String ELE_baseStockPrice = "baseStockPrice";
    private static final String ELE_compareStockCode = "compareStockCode";
    private static final String ELE_compareStockName = "compareStockName";
    private static final String ELE_compareStockPrice = "compareStockPrice";
    private static final String CONFIG_FILE_NAME = "rotation.xml";

    @Override
    public RotationConfigBO loadRotationConfig() {
        RotationConfigBO configBO = new RotationConfigBO();
        File configFile = new File(this.getConfigFolder(), CONFIG_FILE_NAME);
        if (!configFile.exists()) {
            return configBO;
        }

        log.info("加载轮动配置：" + configFile.getAbsolutePath());
        try {
            SAXReader reader = new SAXReader();
            Document document = reader.read(configFile);
            Element rootEle = document.getRootElement();

            configBO.setOpen("true".equals(rootEle.elementText(ConfigConstants.ELE_OPEN)));
            configBO.setBaseStockCode(rootEle.elementText(ELE_baseStockCode));
            configBO.setBaseStockName(rootEle.elementText(ELE_baseStockName));
            configBO.setBaseStockPrice(Double.parseDouble(rootEle.elementText(ELE_baseStockPrice)));
            configBO.setCompareStockCode(rootEle.elementText(ELE_compareStockCode));
            configBO.setCompareStockName(rootEle.elementText(ELE_compareStockName));
            configBO.setCompareStockPrice(Double.parseDouble(rootEle.elementText(ELE_compareStockPrice)));
            return configBO;
        } catch (DocumentException e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void saveRotationConfig(RotationConfigBO configBO) {
        DocumentFactory factory = DocumentFactory.getInstance();
        Document document = factory.createDocument();
        document.setXMLEncoding("UTF8");

        Element rootEle = factory.createElement(ConfigConstants.ELE_CONFIG);
        document.setRootElement(rootEle);
        rootEle.add(this.createEle(factory, ConfigConstants.ELE_OPEN, configBO.isOpen()));
        rootEle.add(this.createEle(factory, ELE_baseStockCode, configBO.getBaseStockCode()));
        rootEle.add(this.createEle(factory, ELE_baseStockName, configBO.getBaseStockName()));
        rootEle.add(this.createEle(factory, ELE_baseStockPrice, configBO.getBaseStockPrice()));
        rootEle.add(this.createEle(factory, ELE_compareStockCode, configBO.getCompareStockCode()));
        rootEle.add(this.createEle(factory, ELE_compareStockName, configBO.getCompareStockName()));
        rootEle.add(this.createEle(factory, ELE_compareStockPrice, configBO.getCompareStockPrice()));

        File configFile = new File(this.getConfigFolder(), CONFIG_FILE_NAME);
        this.saveConfig(configFile, document);
    }
}
