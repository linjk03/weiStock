# wei的股票软件

#### 介绍
基于网格策略的自动化股票交易软件，低买高卖，开盘日自动盯盘，无需人工干预。
1. 基于网格策略，低于指定价格时自动买入，高于指定价格时自动卖出。
2. 自动提交订单后，会检测订单的状态，只有当订单成交后，才改变基准价格。
3. 自动检测当日是否开盘（9:15至15:00），并在开盘时实时执行策略。
4. 以windows服务方式运行，全自动交易，无需人工干预。

![首页](https://images.gitee.com/uploads/images/2019/0816/172754_f180e2d0_1136461.jpeg "wei股票-首页.jpg")

![股票网格配置](https://images.gitee.com/uploads/images/2019/0816/172831_2533fcd6_1136461.jpeg "wei股票-网格.jpg")

![股票自动交易](https://images.gitee.com/uploads/images/2019/0816/172855_b115b55e_1136461.png "网格自动交易.png")

#### 软件架构
采用java语言开发的标准的web app应用，可运行在任意的java servlet容器中。

#### 安装教程

1. 下载源代码
2. 安装java8及maven
3. 用maven编译打包生成weiStock.war
4. 安装tomcat
5. 在tomcat中部署weiStock.war

#### 使用说明

1. 账号及股票配置文件路径是D:\TradeInfo.xml，请确保存在D盘
2. 打开浏览器访问：http://hostname:port/weiStock/
3. 通过页面配置股票账号
4. 通过页面添加股票网格配置
5. 在交易日的交易时间段，通过页面监控策略执行情况

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)